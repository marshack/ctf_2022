# Write up "Protect my home"

## Consigne:

```
Pour résoudre ce challenge, "il suffit" de récupérer le flag qui se trouve sur le bureau de l'utilisateur. Malheureusement, ce dernier a chiffré sa partition "home".

Démarrez un environnement virtuel, et tentez de retrouver le flag.

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/14','_blank')" >Accéder au portail</button>
</div>
<p></p>
* Note : Vous avez la possibilité de démarrer sur un "liveCD"
<p></p>

`3136150cce6cbddc3309970ae05bfe4da73c3557` [rockyou.zip](https://download.marshack.fr/cryptography/Protect_my_home/rockyou.zip)
```

## Pièce jointe :

```
rockyou.zip
```

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{~myFilesAreVerySecure~}
```

## Solution : 

Je démarre un environnement virtuel :

![](images/portailvms.png)

Je sélectionne une image ISO, je sélectionne "bootable", je valide et je redémarre l'environnement.

![](images/linuxcdrescue.png)

Une fois le *liveCD* démarré, je choisi la langue, modifie le mot de passe root  et arrête le parefeu iptables (afin de pouvoir se connecter en ssh) :
```bash
[root@sysrescue ~]# loadkeys fr
[root@sysrescue ~]# passwd 
New password: 
Retype new password: 
passwd: password updated successfully
```

Je peux maintenant me connecter en ssh en tant que root.

J'explore :
```bash
root@sysresccd /root % fdisk -l /dev/sda
Disk /dev/sda: 6 GiB, 6372560896 bytes, 12446408 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x000d792e

Device     Boot   Start      End Sectors  Size Id Type
/dev/sda1  *       2048  7094271 7092224  3.4G 83 Linux
/dev/sda2       7094272 12445695 5351424  2.6G  5 Extended
/dev/sda5       7096320 12445695 5349376  2.6G 82 Linux swap / Solaris
root@sysresccd /root % mount /dev/sda1 /mnt 
root@sysresccd /root % cd /mnt/home 
root@sysresccd /mnt/home % ls -la
total 16
drwxr-xr-x  4 root root 4096 Mar 11 19:46 .
drwxr-xr-x 23 root root 4096 Mar 12 16:31 ..
drwxrwxr-x  3 root root 4096 Mar 11 19:46 .ecryptfs
dr-x------  2 1000 1000 4096 Mar 11 19:46 gerard
root@sysresccd /mnt/home % ls -la gerard 
total 8
dr-x------ 2 1000 1000 4096 Mar 11 19:46 .
drwxr-xr-x 4 root root 4096 Mar 11 19:46 ..
lrwxrwxrwx 1 1000 1000   56 Mar 11 19:46 Access-Your-Private-Data.desktop -> /usr/share/ecryptfs-utils/ecryptfs-mount-private.desktop
lrwxrwxrwx 1 1000 1000   32 Mar 11 19:46 .ecryptfs -> /home/.ecryptfs/gerard/.ecryptfs
lrwxrwxrwx 1 1000 1000   31 Mar 11 19:46 .Private -> /home/.ecryptfs/gerard/.Private
lrwxrwxrwx 1 1000 1000   52 Mar 11 19:46 README.txt -> /usr/share/ecryptfs-utils/ecryptfs-mount-private.txt
```

Le "*home*" de l'utilisateur est chiffré à l'aide de *ecryptfs*, qui est un système de chiffrement au niveau fichiers : https://en.wikipedia.org/wiki/ECryptfs

La clé de chiffrement se trouve ici : `home/.ecryptfs/gerard/.ecryptfs/wrapped-passphrase`

Cette clé est protégée par une passphrase. On nous offre un dictionnaire de "mots de passe", il semble évident qu'il va falloir faire tourner les CPU/GPU ;-)

Tout d'abord, il est nécessaire de *parser* ce fichier, pour le rendre lisible au profit de 'john' ou 'hashcat'. Un utilitaire écrit en python permet de réaliser cette tâche : https://github.com/openwall/john/blob/bleeding-jumbo/run/ecryptfs2john.py

Sur ma kali, je récupère le fichier, et le converti :
```bash
[kali@kali ~]$ scp root@172.24.8.201:/mnt/home/.ecryptfs/gerard/.ecryptfs/wrapped-passphrase .
[kali@kali ~]$ python2.7 ecryptfs2john.py wrapped-passphrase > hash_ecryptfs
[kali@kali ~]$ cat hash_ecryptfs
wrapped-passphrase:$ecryptfs$0$4f05fb7775321293
```

Et je donne tout ça à "*John the Ripper*" :
```bash
[kali@kali ~]$ john --format=eCryptfs --wordlist=rockyou.txt hash_ecryptfs
Using default input encoding: UTF-8
Loaded 1 password hash (eCryptfs [SHA512 256/256 AVX2 4x])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
smile1994        (wrapped-passphrase)     
1g 0:00:23:07 DONE (2022-03-13 11:07) 0.000720g/s 420.1p/s 420.1c/s 420.1C/s smiley97..smexy10
```

Avec 4 CPU, en moins de 25 minutes, le mot de passe est retrouvé **smile1994**.

On a l'identifiant (*gerard*) et le mot de passe. On redémarre la machine en ayant pris soins d'enlever le *liveCD*. On s'authentifie, et le fichier *flag.txt* est bien présent sur le bureau de l'utilisateur :-)

![](images/login.png)

![](images/desktop.png)


<!-- Ok F : 20220424 -->
