#!/usr/bin/env python3

import random
import base64
import uuid
import socket, os, sys
import rsa
import binascii

import signal


# configuration du timeout
def alarm_handler(signum, frame):
    print("Oups !")
    exit()
 
signal.signal(signal.SIGALRM, alarm_handler)
signal.alarm(2)
 
### Secret random ###

sec_rand = str(uuid.uuid4())


### Question 1 ###
q1_r1 = random.randint(0,100000000000000)
q1_r2 = random.randint(0,100000000000000)
q1_poss = f'{q1_r1} + {q1_r2}'
q1_rep = (q1_r1 + q1_r2)  
q1_ind = b'petit_challenge_rsa'

### Question 2 ###
q2_r1 = random.randint(0,100000000000000)
q2_r2 = random.randint(0,100000000000000)
q2_poss = f'{q2_r1} - {q2_r2}'
q2_rep = (q2_r1 - q2_r2)

### Question 3 ###
q3_r1 = random.randint(0,100000000000000)
q3_r2 = random.randint(0,100000000000000)
q3_poss = f'{q3_r1} + {q3_r2}'
q3_rep = (q3_r1 + q3_r2)

### Question 4 ###
q4_r1 = random.randint(0,100000000000000)
q4_r2 = random.randint(0,100000000000000)
q4_poss = f'{q4_r1} - {q4_r2}'
q4_rep = (q4_r1 - q4_r2)

### Question 5 ###
q5_r1 = random.randint(0,100000000000000)
q5_r2 = random.randint(0,100000000000000)
q5_poss = f'{q5_r1} + {q5_r2}'
q5_rep = (q5_r1 + q5_r2)

### Question 6 ### 

q6_poss = "\nDonne moi le message dechiffre et en echange tu auras le flag\n"

### Mauvaise reponse ###

bad = '\nRecommence !!!!\n'

#########################

def main():
    # q1
    print('\n########## Question 1 ##########')
    print(q1_poss)
    print('ta reponse -> ')
    data = input()
    #print(data)
    if int(data) != q1_rep :
        print(bad)
        exit(1)
    else:
        print('Voici ton premier indice :')
        print((base64.b64encode(q1_ind)).decode('utf-8'))

    # q2
    #print(q2_rep)
    pubkey, privkey = rsa.newkeys(512)
    n=privkey.n
    print('\n########## Question 2 ##########')
    print(q2_poss)
    print('ta reponse ->')
    data = input()
    if int(data) != q2_rep:
        print(bad)
        exit(1)
    else:
        print('Voici ton deuxieme indice : n = ')
        print(str(n))

    # q3
    #print(q3_rep)
    e=privkey.e
    print('\n########## Question 3 ##########')
    print(q3_poss)
    print('ta reponse ->')
    data = input()
    if int(data) != q3_rep:
        print(bad)
        exit(1)
    else:
        print('Voici ton troisieme indice : e =')
        print(str(e))

    # q4
    #print(q4_rep)
    d=privkey.d
    print('\n########## Question 4 ##########')
    print(q4_poss)
    print('ta reponse ->')
    data = input()
    if int(data) != q4_rep:
        print(bad)
        exit(1)
    else:
        print('Voici ton quatrieme indice : d =')
        print(str(d))

    # q5
    #print(q5_rep)
    print('\n########## Question 5 ##########')
    message=sec_rand.encode('utf8')
    #print(message)
    encrypted=rsa.encrypt(message, pubkey)
    print(q5_poss)
    print('ta reponse ->')
    data = input()
    if int(data) != q5_rep:
        print(bad)
        exit(1)
    else:
        print('Voici ton cinquieme indice : c=')
        #print(encrypted)
        res = (base64.b64encode(encrypted))
        print (res.decode('utf-8'))
    # q6
    print('\n########## Question 6 ##########')
    print(q6_poss)
    data = input()

    #if data != sec_rand:
    if (data.strip()) !=sec_rand:
        #print ((data.encode('utf-8')).strip())
        print(bad)
        exit(1)
    else:
        print('\n########## Congratulation ##########')
        file=open("/home/remote01/flag.txt", "r")
        flag=(file.read())
        #print(flag)
        print('\nVoici le flag :')
        print(flag)
    
    #print ("Flag found by: ", tsap_cli)


main()
