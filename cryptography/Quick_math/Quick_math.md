# Write up Crypto/quick_math

## Consigne : 

```
Un administrateur a besoin de toi pour faire quelques calculs. 5 questions te seront posées. Pour chaque réponse tu gagneras un indice. 

Ceux-ci te permettront de déchiffrer un message. Donne lui ce message et tu gagnera le Flag. Attention tu as 2 secondes pour repondre !!!! 
        
```

## Pièce jointe :

```

```

## Serveur :

```
Docker

game1.marshack.fr:30001
```

## Points attribués :

```
30
```

## Hint : 

```

```

## Flag :

```
fl@g{Rsa_powahhh}
```

## Créateur :

```
f3nris/cez40
```

##  Description général du challenge :

Le but de ce challenge est de répondre au calcul le plus rapidement possible. 

Chaque reponse rapporte un indice. 

Présentation de la méthodologie de résolution du challenge :

Pour commencer, nous avons répondre au question afin de récolter les différents indices.

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# F3 toogle affichage des numero de lignes
# F5 paste/no paste (copier coller sans indentation)
# F6 pour executer le script python 2.7
# F7 pour executer le script python 3
# F8 highlighting on/off, and show current value.



from pwn import *
import binascii

# désactiver l'aslr dans la console
# echo 0 | sudo tee /proc/sys/kernel/randomize_va_space


context.log_level="debug"
#context.log_level="critical"


def bytes2int(raw_bytes: bytes) -> int:
    return int.from_bytes(raw_bytes, "big", signed=False)

def int2bytes(number: int, fill_size: int = 0) -> bytes:
 
     if number < 0:
         raise ValueError("Number must be an unsigned integer: %d" % number)
 
     bytes_required = (number.bit_length()+7) // 8  
     if fill_size > 0:
         return number.to_bytes(fill_size, "big")
 
     return number.to_bytes(bytes_required, "big")

## site distant
hostname="192.168.120.1"
port=30001
p = remote(hostname,port)      


#Marshack
res=(p.recvlines(8,timeout=1))
print (res)


# question 1
#p.sendline('<texte a envoyer>')
res=(p.recvlines(4,timeout=1))
print (res)
#print (res[2])
#
p.sendline((str(eval(res[2]))).encode('utf_8'))
# premier indice - evoque rsa
res=(p.recvlines(2,timeout=1))
print (res)
ind1=(base64.b64decode(res[1]))
print (ind1)

# question 2
res=(p.recvlines(4,timeout=1))
print (res)
p.sendline((str(eval(res[2]))).encode('utf_8'))
# deuxieme indice - n modulus
res=(p.recvlines(2,timeout=1))
print (res)
ind2=res[1]
print (ind2)

# question 3
res=(p.recvlines(4,timeout=1))
print (res)
print (res[2])
p.sendline((str(eval(res[2]))).encode('utf_8'))
#troisième indice - e clef public
res =(p.recvlines(2,timeout=1))
print (res)
ind3=res[1]
print (ind3)

# question 4
res=(p.recvlines(4,timeout=1))
print (res)
print (res[2])
p.sendline((str(eval(res[2]))).encode('utf_8'))
#quatrième indice - d clef privee
res =(p.recvlines(2,timeout=1))
print (res)
ind4=res[1]
print (ind4)

# question 5
res=(p.recvlines(4,timeout=1))
print (res)
print (res[2])
p.sendline((str(eval(res[2]))).encode('utf_8'))
#cinquième indice - cipher
res =(p.recvlines(2,timeout=1))
print (res)
ind5=res[1]


```

Premier indice se trouve être du base64. une fois décodé on obtient : 

```
petit_challenge_rsa

```

Les indices 2 à 4 sont les éléments d'une clé privé RSA.

La clé RSA ainsi que le UUID est généré aléatoirement à chaque connexion.  Ce qui signifie que le challenge doit être fait en une seul fois.

A chaque calcul réussi on obtient le N, E et D

l'indice 5 est le C correspondant un message chiffré (UUID).

Une fois les différents indice obtenu on doit déchiffer le message et le transmettre au serveur. En cas de reponse positive on obtiendra le flag

```python
# evoque rsa
print (ind1)

# n modulus
ind2=int(ind2,10)
print (ind2)

#e clef privée 
print (ind3)

#d clef privée 
ind4=int(ind4,10)
print (ind4)

# ind5
print (ind5)

#ind5 = ind5.decode('utf-8')
ind5 = base64.b64decode(ind5)
ind5=int.from_bytes(ind5, "big", signed=False)

# question 6 - envoie l uuid
res=p.recvlines(5,timeout=1)
print (res)

#clairtext = pow(cipher,d,n)
res = pow(ind5,ind4,ind2)

res=int2bytes(res)
#print ("indice")
indice=res.find(b"\x00",2)
clairtext = (res[indice+1:])
p.sendline(clairtext)
        
res=p.recvlines(5,timeout=1)
print (res)
print (res[1].decode('utf-8'))
print (res[2].decode('utf-8'))
print (res[3].decode('utf-8'))
print (res[4].decode('utf-8'))

#input("CMD> ") 

p.close
```



Résultat de l'execution du script : 

```python
[DEBUG] Received 0x21 bytes:
    b'\n'
    b'########## Question 1 ##########'
[DEBUG] Received 0x30 bytes:
    b'\n'
    b'29012280863157 + 14483915618797\n'
    b'ta reponse -> \n'
[b'', b'########## Question 1 ##########', b'29012280863157 + 14483915618797', b'ta reponse -> ']
[DEBUG] Sent 0xf bytes:
    b'43496196481954\n'
[DEBUG] Received 0x1a bytes:
    b'Voici ton premier indice :'
[DEBUG] Received 0x1e bytes:
    b'\n'
    b'cGV0aXRfY2hhbGxlbmdlX3JzYQ==\n'
[b'Voici ton premier indice :', b'cGV0aXRfY2hhbGxlbmdlX3JzYQ==']
b'petit_challenge_rsa'
[DEBUG] Received 0x21 bytes:
    b'\n'
    b'########## Question 2 ##########'
[DEBUG] Received 0x2f bytes:
    b'\n'
    b'58910314412326 - 15625954246721\n'
    b'ta reponse ->\n'
[b'', b'########## Question 2 ##########', b'58910314412326 - 15625954246721', b'ta reponse ->']
[DEBUG] Sent 0xf bytes:
    b'43284360165605\n'
[DEBUG] Received 0x20 bytes:
    b'Voici ton deuxieme indice : n = '
[DEBUG] Received 0xec bytes:
    b'\n'
    b'8110549893590313907217897255813568453960221484176999467588004802433818933967341560439930638978887565542956933702686387505549309167576145172553581719191633\n'
    b'\n'
    b'########## Question 3 ##########\n'
    b'75534051386375 + 98615243068268\n'
    b'ta reponse ->\n'
[b'Voici ton deuxieme indice : n = ', b'8110549893590313907217897255813568453960221484176999467588004802433818933967341560439930638978887565542956933702686387505549309167576145172553581719191633']
b'8110549893590313907217897255813568453960221484176999467588004802433818933967341560439930638978887565542956933702686387505549309167576145172553581719191633'
[b'', b'########## Question 3 ##########', b'75534051386375 + 98615243068268', b'ta reponse ->']
b'75534051386375 + 98615243068268'
[DEBUG] Sent 0x10 bytes:
    b'174149294454643\n'
[DEBUG] Received 0x20 bytes:
    b'Voici ton troisieme indice : e ='
[DEBUG] Received 0x56 bytes:
    b'\n'
    b'65537\n'
    b'\n'
    b'########## Question 4 ##########\n'
    b'41930595507182 - 9961709164176\n'
    b'ta reponse ->\n'
[b'Voici ton troisieme indice : e =', b'65537']
b'65537'
[b'', b'########## Question 4 ##########', b'41930595507182 - 9961709164176', b'ta reponse ->']
b'41930595507182 - 9961709164176'
[DEBUG] Sent 0xf bytes:
    b'31968886343006\n'
[DEBUG] Received 0x20 bytes:
    b'Voici ton quatrieme indice : d ='
[DEBUG] Received 0xec bytes:
    b'\n'
    b'5584457084521154692207571504166917565420373139958910248789366567127364375747004706329278760683646429384638807440248250447745272699189622497667214965713973\n'
    b'\n'
    b'########## Question 5 ##########\n'
    b'11163158479666 + 47067608109030\n'
    b'ta reponse ->\n'
[b'Voici ton quatrieme indice : d =', b'5584457084521154692207571504166917565420373139958910248789366567127364375747004706329278760683646429384638807440248250447745272699189622497667214965713973']
b'5584457084521154692207571504166917565420373139958910248789366567127364375747004706329278760683646429384638807440248250447745272699189622497667214965713973'
[b'', b'########## Question 5 ##########', b'11163158479666 + 47067608109030', b'ta reponse ->']
b'11163158479666 + 47067608109030'
[DEBUG] Sent 0xf bytes:
    b'58230766588696\n'
[DEBUG] Received 0x1f bytes:
    b'Voici ton cinquieme indice : c='
[DEBUG] Received 0xbc bytes:
    b'\n'
    b'cEYjytX0ro0w5SRFS7BDmWUrgNnBKvDjczmLIc5/rRZ7MJv/K8y+ugA4+/nEoG7Hiy5Ix+O9/Q2Gb33oSoVSRA==\n'
    b'\n'
    b'########## Question 6 ##########\n'
    b'\n'
    b'Donne moi le message dechiffre et en echange tu auras le flag\n'
    b'\n'
[b'Voici ton cinquieme indice : c=', b'cEYjytX0ro0w5SRFS7BDmWUrgNnBKvDjczmLIc5/rRZ7MJv/K8y+ugA4+/nEoG7Hiy5Ix+O9/Q2Gb33oSoVSRA==']
b'petit_challenge_rsa'
8110549893590313907217897255813568453960221484176999467588004802433818933967341560439930638978887565542956933702686387505549309167576145172553581719191633
b'65537'
5584457084521154692207571504166917565420373139958910248789366567127364375747004706329278760683646429384638807440248250447745272699189622497667214965713973
b'cEYjytX0ro0w5SRFS7BDmWUrgNnBKvDjczmLIc5/rRZ7MJv/K8y+ugA4+/nEoG7Hiy5Ix+O9/Q2Gb33oSoVSRA=='
[b'', b'########## Question 6 ##########', b'', b'Donne moi le message dechiffre et en echange tu auras le flag', b'']
[DEBUG] Sent 0x25 bytes:
    b'36106078-029d-46ef-aa27-0054a777431f\n'
[DEBUG] Received 0x25 bytes:
    b'\n'
    b'########## Congratulation ##########'
[DEBUG] Received 0x25 bytes:
    b'\n'
    b'\n'
    b'Voici le flag :\n'
    b'fl@g{Rsa_powahhh}\n'
    b'\n'
[b'', b'########## Congratulation ##########', b'', b'Voici le flag :', b'fl@g{Rsa_powahhh}']
########## Congratulation ##########

Voici le flag :
fl@g{Rsa_powahhh}
[*] Closed connection to 192.168.120.1 port 30001
```

<!-- Ok F : 20220424 -->