#!/usr/bin/env python
# -*- coding: utf-8 -*-

# F3 toogle affichage des numero de lignes
# F5 paste/no paste (copier coller sans indentation)
# F6 pour executer le script python 2.7
# F7 pour executer le script python 3
# F8 highlighting on/off, and show current value.



from pwn import *
import binascii

# désactiver l'aslr dans la console
# echo 0 | sudo tee /proc/sys/kernel/randomize_va_space


context.log_level="debug"
#context.log_level="critical"


def bytes2int(raw_bytes: bytes) -> int:
    return int.from_bytes(raw_bytes, "big", signed=False)

def int2bytes(number: int, fill_size: int = 0) -> bytes:
 
     if number < 0:
         raise ValueError("Number must be an unsigned integer: %d" % number)
 
     bytes_required = (number.bit_length()+7) // 8  
     if fill_size > 0:
         return number.to_bytes(fill_size, "big")
 
     return number.to_bytes(bytes_required, "big")

## site distant
hostname="192.168.120.1"
port=30001
p = remote(hostname,port)      


#Marshack
res=(p.recvlines(8,timeout=1))
print (res)


# question 1
#p.sendline('<texte a envoyer>')
res=(p.recvlines(4,timeout=1))
print (res)
#print (res[2])
#
p.sendline((str(eval(res[2]))).encode('utf_8'))
# premier indice - evoque rsa
res=(p.recvlines(2,timeout=1))
print (res)
ind1=(base64.b64decode(res[1]))
print (ind1)

# question 2
res=(p.recvlines(4,timeout=1))
print (res)
p.sendline((str(eval(res[2]))).encode('utf_8'))
# deuxieme indice - n modulus
res=(p.recvlines(2,timeout=1))
print (res)
ind2=res[1]
print (ind2)

# question 3
res=(p.recvlines(4,timeout=1))
print (res)
print (res[2])
p.sendline((str(eval(res[2]))).encode('utf_8'))
#troisième indice - e clef public
res =(p.recvlines(2,timeout=1))
print (res)
ind3=res[1]
print (ind3)

# question 4
res=(p.recvlines(4,timeout=1))
print (res)
print (res[2])
p.sendline((str(eval(res[2]))).encode('utf_8'))
#quatrième indice - d clef privee
res =(p.recvlines(2,timeout=1))
print (res)
ind4=res[1]
print (ind4)

# question 5
res=(p.recvlines(4,timeout=1))
print (res)
print (res[2])
p.sendline((str(eval(res[2]))).encode('utf_8'))
#cinquième indice - cipher
res =(p.recvlines(2,timeout=1))
print (res)
ind5=res[1]

# evoque rsa
print (ind1)

# n modulus
ind2=int(ind2,10)
print (ind2)

#e clef privée 
print (ind3)

#d clef privée 
ind4=int(ind4,10)
print (ind4)

# ind5
print (ind5)

#ind5 = ind5.decode('utf-8')
ind5 = base64.b64decode(ind5)
ind5=int.from_bytes(ind5, "big", signed=False)

# question 6 - envoie l uuid
res=p.recvlines(5,timeout=1)
print (res)

#clairtext = pow(cipher,d,n)
res = pow(ind5,ind4,ind2)

res=int2bytes(res)
#print ("indice")
indice=res.find(b"\x00",2)
clairtext = (res[indice+1:])
p.sendline(clairtext)
        
res=p.recvlines(5,timeout=1)
print (res)
print (res[1].decode('utf-8'))
print (res[2].decode('utf-8'))
print (res[3].decode('utf-8'))
print (res[4].decode('utf-8'))

#input("CMD> ") 

p.close
