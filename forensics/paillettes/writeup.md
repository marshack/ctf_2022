# Mets des paillettes dans ta vie Kévin!

## Consignes :  

```
Kévin a 14 ans et passe beaucoup trop de temps sur son smartphone. Son père, pour le punir, lui change son code de démarrage. 

Plus tard, il lui donne un fichier sur une clé USB:  "Si tu veux récupérer ton code de téléphone, et bien cherche le!"
Aidez Kévin à analyser le fichier pour retrouver son code.
```

## Pièce jointe :  

```
paillettes.jpg
```

## Serveur :  

```
```

## Points attribués :  

```
50
```

## Flag :  

```
fl@g{I_l0v3_mY_d@D_<3}
```

## Architecture :  

```
Néant
```

## Déroulé :  

Le fichier fourni semble être une photo. La commande "file" à partir d'une machine Linux le confirme:

```
# file paillettes.jpg 
paillettes.jpg: JPEG image data
```

Cependant celle-ci pèse environ 400M, ce qui parait est très élévé.

```
# ll paillettes.jpg 
-rwxrwxrwx 1 root root 410553808 Oct 27 22:39 paillettes.jpg*
```

On vérifie donc s'il n'y a pas des fichiers cachés à l'intérieur et on les extrait si c'est la cas avec binwalk:

```
# binwalk -e paillettes.jpg 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
2             0x2             Zip archive data, at least v2.0 to extract, compressed size: 410553648, uncompressed size: 1610211328, name: hiberfil.sys
410553786     0x18788DBA      End of Zip archive
```

Le fichier **hiberfil.sys** est dézippé automatiquement.

On reconnait alors que c'est un fichier d'hibernation qui est créé dans différentes versions de Windows lors de la mise en veille prolongée.

Il faut ensuite trouver la version de Windows correspondante pour l'analyser et en déduire le profil:

```
# strings hiberfil.sys | grep "NT 6"
 NT 6.1; x
ndows NT 6.1; x
/5.0 (Windows NT 6.1; x
```

La valeur "NT 6.1" indique que le fichier provient probablement d'un poste Windows 7.

```
# strings hiberfil.sys | grep Windows | grep SP1
Windows7SP1-KB9
Windows7SP1-KBk
```

On constate également que le Service Pack 1 semble être installé sur la machine.

En supposant que le poste incriminé comporte un système d'exploitation en 64bits, on suppose que le profil volatility utilisé peut être **Win7SP1x64**.

Un fichier d'hibernation n'étant pas expoitable par défaut par volatility, il faut tout d'abord le décompresser avec le plugin imagecopy:

```
# volatility -f hiberfil.sys imagecopy --profile=Win7SP1x64 -O Win7.raw
Volatility Foundation Volatility Framework 2.6
Writing data (5.00 MB chunks): |......................................................................................................................................................................................................................................................
```

Un fichier "Win7.raw" est alors généré et expoitable par volatility.


Commençons par lister les processus avec le plugin **pslist**:

```
# volatility -f Win7.raw --profile=Win7SP1x64 pslist
Volatility Foundation Volatility Framework 2.6
Offset(V)          Name                    PID   PPID   Thds     Hnds   Sess  Wow64 Start                          Exit                          
------------------ -------------------- ------ ------ ------ -------- ------ ------ ------------------------------ ------------------------------
0xfffffa80018ae990 System                    4      0     93      409 ------      0 2019-10-27 16:47:18 UTC+0000                                 
0xfffffa8002805760 smss.exe                260      4      2       29 ------      0 2019-10-27 16:47:18 UTC+0000                                 
0xfffffa8003501550 csrss.exe               340    332      9      526      0      0 2019-10-27 16:47:24 UTC+0000                                 
0xfffffa800351db30 wininit.exe             388    332      3       76      0      0 2019-10-27 16:47:25 UTC+0000                                 
0xfffffa800351fb30 csrss.exe               396    380      9      340      1      0 2019-10-27 16:47:25 UTC+0000                                 
0xfffffa8003532b30 winlogon.exe            420    380      6      125      1      0 2019-10-27 16:47:25 UTC+0000                                 
0xfffffa8003597910 services.exe            484    388      9      215      0      0 2019-10-27 16:47:27 UTC+0000                                 
0xfffffa80035abb30 lsass.exe               492    388      8      732      0      0 2019-10-27 16:47:27 UTC+0000                                 
0xfffffa80035b19e0 lsm.exe                 500    388     10      161      0      0 2019-10-27 16:47:27 UTC+0000                                 
0xfffffa80035702d0 svchost.exe             612    484     12      356      0      0 2019-10-27 16:47:35 UTC+0000                                 
0xfffffa80035ee340 svchost.exe             688    484     17      297      0      0 2019-10-27 16:47:35 UTC+0000                                 
0xfffffa8003662060 svchost.exe             732    484     25      550      0      0 2019-10-27 16:47:36 UTC+0000                                 
0xfffffa8003580350 svchost.exe             816    484     31      587      0      0 2019-10-27 16:47:36 UTC+0000                                 
0xfffffa80035a3200 svchost.exe             840    484     50     1031      0      0 2019-10-27 16:47:36 UTC+0000                                 
0xfffffa80035c1060 svchost.exe             968    484     24      510      0      0 2019-10-27 16:47:38 UTC+0000                                 
0xfffffa80035b4060 svchost.exe             292    484     18      384      0      0 2019-10-27 16:47:38 UTC+0000                                 
0xfffffa8003740320 spoolsv.exe             560    484     14      270      0      0 2019-10-27 16:47:40 UTC+0000                                 
0xfffffa8002ad7b30 svchost.exe             988    484     20      329      0      0 2019-10-27 16:47:41 UTC+0000                                 
0xfffffa80037d5740 svchost.exe            1120    484     28      317      0      0 2019-10-27 16:47:41 UTC+0000                                 
0xfffffa80038ab390 svchost.exe            1428    484      8       96      0      0 2019-10-27 16:47:46 UTC+0000                                 
0xfffffa8003998b30 taskhost.exe           1716    484     10      174      1      0 2019-10-27 16:47:53 UTC+0000                                 
0xfffffa80039e2b30 dwm.exe                1780    816      4       73      1      0 2019-10-27 16:47:53 UTC+0000                                 
0xfffffa80039ecb30 taskeng.exe            1792    840      6       79      0      0 2019-10-27 16:47:53 UTC+0000                                 
0xfffffa8003a1db30 explorer.exe           1860   1768     30      696      1      0 2019-10-27 16:47:53 UTC+0000                                 
0xfffffa8003b09b30 SearchIndexer.         1736    484     13      583      0      0 2019-10-27 16:48:06 UTC+0000                                 
0xfffffa8003b424e0 wmpnetwk.exe           1696    484     25      544      0      0 2019-10-27 16:48:08 UTC+0000                                 
0xfffffa8003b26b30 SearchProtocol         2032   1736      7      290      0      0 2019-10-27 16:48:13 UTC+0000                                 
0xfffffa8002acf710 SearchFilterHo         1292   1736      5       77      0      0 2019-10-27 16:48:14 UTC+0000                                 
0xfffffa8003c0e390 svchost.exe            2112    484     11      350      0      0 2019-10-27 16:48:15 UTC+0000                                 
0xfffffa8003cf5b30 WmiPrvSE.exe           2784    612     13      293      0      0 2019-10-27 16:48:43 UTC+0000                                 
0xfffffa8002ce6060 chrome.exe             2868   1860     31      820      1      0 2019-10-27 16:48:59 UTC+0000                                 
0xfffffa8003c99290 chrome.exe             2876   2868      9       83      1      0 2019-10-27 16:48:59 UTC+0000                                 
0xfffffa8002a59710 chrome.exe             2908   2868      3       55      1      0 2019-10-27 16:49:00 UTC+0000                                 
0xfffffa8001a06380 chrome.exe             3016   2868     16      310      1      0 2019-10-27 16:49:01 UTC+0000                                 
0xfffffa8001a77b30 chrome.exe             2184   2868     13      266      1      0 2019-10-27 16:49:03 UTC+0000                                 
0xfffffa8001a72350 chrome.exe             1872   2868     12      197      1      0 2019-10-27 16:49:04 UTC+0000                                 
0xfffffa8003d1d2f0 chrome.exe             1772   2868     16      289      1      0 2019-10-27 16:49:04 UTC+0000                                 
0xfffffa8001ac2b30 chrome.exe             1224   2868     10      205      1      0 2019-10-27 16:49:11 UTC+0000                                 
0xfffffa8001b28690 WmiApSrv.exe           2532    484      7      114      0      0 2019-10-27 16:49:16 UTC+0000                                 
0xfffffa8001a04b30 WmiPrvSE.exe           2588    612      9      176      0      0 2019-10-27 16:49:18 UTC+0000                                 
0xfffffa8001bf6b30 chrome.exe             2212   2868     14      214      1      0 2019-10-27 16:49:24 UTC+0000                                 
0xfffffa8001c57060 mscorsvw.exe           3572    484      6       83      0      1 2019-10-27 16:49:46 UTC+0000                                 
0xfffffa8001b2a060 mscorsvw.exe           3680    484      6       76      0      0 2019-10-27 16:49:49 UTC+0000                                 
0xfffffa8001cd5b30 sppsvc.exe             3776    484      5      146      0      0 2019-10-27 16:49:51 UTC+0000                                 
0xfffffa8001cde6c0 svchost.exe            3844    484     18      366      0      0 2019-10-27 16:49:51 UTC+0000
0xfffffa8001d91b30 KeePass.exe            4040   1860     12      322      1      0 2019-10-27 16:50:08 UTC+0000                                 
0xfffffa8001df3580 VeraCrypt.exe          3436   1860      6      214      1      0 2019-10-27 16:50:38 UTC+0000                                 
0xfffffa8001cb0630 LogonUI.exe            3592    420      9      184      1      0 2019-10-27 16:50:56 UTC+0000  
```

Dans un premier temps, les processus "chrome.exe" attirent notre attention, mais également "keypass.exe" et "Veracrypt.exe". 

Il est possible d'extraire l'historique du navigateur Chrome avec le plugin chromehistory :

Pour installer le plugin, copier le fichier chromehistory.py disponible à l'adresse suivante :

[https://github.com/superponible/volatility-plugins/blob/master/chromehistory.py](https://github.com/superponible/volatility-plugins/blob/master/chromehistory.py) puis le placer dans le répertoire "/usr/lib/python2.7/dist-packages/volatility/plugins".

```

# volatility -f Win7.raw --profile=Win7SP1x64 chromehistory
Volatility Foundation Volatility Framework 2.6
Index  URL                                                                              Title                                                                            Visits Typed Last Visit Time            Hidden Favicon ID
------ -------------------------------------------------------------------------------- -------------------------------------------------------------------------------- ------ ----- -------------------------- ------ ----------
    45 https://www.dropbox.com/h                                                        Accueil - Dropbox                                                                     1     0 2019-10-27 16:50:00.875870        N/A       
    40 chrome-extension://hdokiejnpimakedhajhdlcegeplioahd/vault.html                   My LastPass Vault                                                                     2     0 2019-10-27 16:49:23.547609        N/A       
    44 https://www.dropbox.com/                                                         Accueil - Dropbox                                                                     1     0 2019-10-27 16:50:00.670313        N/A       
    41 https://www.dropbox.com/fr/login                                                 Connexion - Dropbox                                                                   2     0 2019-10-27 16:49:28.429314        N/A       
    39 https://www.larousse.fr/dictionnaires/anglais-francais/flag/580963               Traduction : flag - Dictionnaire anglais-français Larousse                           1     0 2019-10-27 15:57:45.987204        N/A       
    38 https://www.google.com/search?q=flag&oq...j0l5.1226j0j7&sourceid=chrome&ie=UTF-8 flag - Recherche Google                                                               3     0 2019-10-27 15:57:27.390538        N/A       
    37 https://www.google.com/search?q=comment...j33.17513j0j7&sourceid=chrome&ie=UTF-8 comment faire du fromage de chêvre avec du lait de vache - Recherche Google          3     0 2019-10-27 15:57:08.960691        N/A       

```

On retrouve dans l'historique des requêtes sur dropbox.com mais on aperçoit également une extension **Lastpass**, un gestionnaire de mots de passe qui permet de stocker les identifiants de connexion et effectuer le remplissage automatique dans le navigateur.

> **_NOTE:_**
Si le plugin **chromehistory** ne fournit aucun résultat, il est également possible de trouver la présence de **Lastpass** avec la commande suivante:

```
# strings Win7.raw | grep pass
pass
semainepass
epass
lasemainepass
Fla semaine pass
epass
astpassschema_registry
astpassuser_gestures
astpassapiDefinitions
astpasssetIcon
astpassactivityLogger
astpass
astpassfile_system_natives
astpassapp_window_natives
er des mots de passe plus forts. Cliquez sur un site de votre coffre-fort pour le lancer et vous connecter instantan
All your passwords, in one safe place.
En savoir plus sur l'importation: https://support.logmeininc.com/lastpass/help/import-passwords-from-other-sources-lp040003
New password generated!
Afficher le mot de passe
Disable master password risk warning for this site?
...
```

Un plugin volatility existe pour extraire les informations de LastPass. Il est récupérable à cette adresse:

[https://github.com/kevthehermit/volatility_plugins/blob/main/vol2/lastpass/lastpass.py](https://github.com/kevthehermit/volatility_plugins/blob/main/vol2/lastpass/lastpass.py)

```
# volatility -f Win7.raw --profile=Win7SP1x64 lastpass
Volatility Foundation Volatility Framework 2.6
Searching for LastPass Signatures
Found pattern in Process: chrome.exe (2868)
Found pattern in Process: chrome.exe (2868)
Found pattern in Process: chrome.exe (2868)
Found pattern in Process: chrome.exe (2868)
Found pattern in Process: chrome.exe (2868)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (1772)
Found pattern in Process: chrome.exe (2212)
Found pattern in Process: chrome.exe (2212)
Found pattern in Process: chrome.exe (2212)
Found pattern in Process: chrome.exe (2212)
Found pattern in Process: chrome.exe (2212)
Found pattern in Process: chrome.exe (2212)

Found LastPass Entry for dropbox.com
UserName: marsathackmarsan@gmail.com
Pasword: R=uZQ/+UpF9egA
```

Les identifiants de connexion à un compte dropbox sont extraits.

On se connecte alors sur le compte dropxbox. Le fichier **papa.vc** comporte une extension qui correspond à un fichier de type Véracrypt.

A partir d'un poste Windows on télécharge et installe "veracrypt": [https://www.veracrypt.fr/en/Downloads.html](https://www.veracrypt.fr/en/Downloads.html), on l'exécute, puis on ouvre le fichier **papa.vc**

Malheureusement, il nous manque un mot de passe pour ouvrir le fichier. On continue donc à chercher dans le fichier d'hibernation.

![veracrypt](assets/image1.png)

Dans la liste des processus, nous avions constaté la présence du processus Keepass.exe. L'utilisation de ce gestionnaire de mot de passe entraine la copie temporaire des identifiants dans le presse papier.

Jetons donc un coup d'oeil dans le presse papier avec volatility et le plugin "clipboard":

```
# volatility -f Win7.raw --profile=Win7SP1x64 clipboard
Volatility Foundation Volatility Framework 2.6
Session    WindowStation Format                         Handle Object             Data                                              
---------- ------------- ------------------ ------------------ ------------------ --------------------------------------------------
         1 WinSta0       CF_UNICODETEXT               0x2d01a1 0xfffff900c019dae0 lor1CTCSJ76Cs2Qq0YDF11                            
         1 WinSta0       CF_TEXT                          0x10 ------------------                                                   
         1 WinSta0       0x300bfL               0x200000000000 ------------------                                                   
         1 WinSta0       CF_TEXT                           0x1 ------------------                                                   
         1 ------------- ------------------            0x300bf 0xfffff900c2078370 
```

Une suite de caractère est présente, peut-être un mot de passe. On essaie de l'utiliser pour ouvrir le fichier veracrypt.

BINGO! Un fichier **code.txt** est présent dans le conteneur Veracrypt.

![code.txt](assets/image2.png)

Nous trouvons alors le code de Kévin qui se trouve être le flag.

<!-- Ok F : 20220424 -->