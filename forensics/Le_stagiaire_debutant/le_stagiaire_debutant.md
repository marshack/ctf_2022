# Write up Forensics/Le stagiaire débutant

## Consigne : 

```
Vous venez de recevoir une lettre vous menaçant vous et votre entreprise. La personne indique qu'elle vient de récupérer des données, la menace est-elle sérieuse ? Le stagiaire venait de se servir de ce poste.

Servez-vous des logs du serveur Splunk qui venait d’être installé.
```

## Pièce jointe :

```
splunk_log.txt
```

## Serveur :

```
CTFD
```

## Points attribués :

```
10
```

## Hint : 

```
```

## Flag :
```
ESD{TropSécuriséPourMoi}
```

## Soluce :
On va commencer par regarder le contenu des logs splunk :
```
Jun 1 10:03:55 192.168.2.41 2021-06-01 10:04:26 reason=Allowed event_id=6968733069742899202 protocol=HTTP_PROXY action=Allowed transactionsize=156 responsesize=65 requestsize=91 urlcategory=URL_Office365 serverip=152.109.28.33 clienttranstime=0 requestmethod=CONNECT refererURL=None useragent=Unknown product=NSS ClientIP=10.52.0.17 status=200 user=stag1@home.com url=roaming.officeapps.live.com:443 vendor=Zscaler clientpublicIP=87.36.16.4 threatcategory=None threatname=None filetype=None appname=Common Office 365 Applications pagerisk=0 department=Default Department urlsupercategory=User-defined appclass=Business dlpengine=None urlclass=Security Risk threatclass=None dlpdictionaries=None fileclass=None bwthrottle=NO servertranstime=0 contenttype=Other unscannabletype=None devicehostname=NA deviceowner=NA
Jun 1 10:03:57 192.168.2.41 2021-06-01 10:04:26 reason=Allowed event_id=6968733069742899203 protocol=HTTP_PROXY action=Allowed transactionsize=156 responsesize=65 requestsize=91 urlcategory=URL_Office365 serverip=152.109.28.33 clienttranstime=0 requestmethod=CONNECT refererURL=None useragent=Unknown product=NSS ClientIP=10.52.0.17 status=200 user=stag1@home.com url=roaming.officeapps.live.com:443 vendor=Zscaler clientpublicIP=87.36.16.4 threatcategory=None threatname=None filetype=None appname=Common Office 365 Applications pagerisk=0 department=Default Department urlsupercategory=User-defined appclass=Business dlpengine=None urlclass=Security Risk threatclass=None dlpdictionaries=None fileclass=None bwthrottle=NO servertranstime=0 contenttype=Other unscannabletype=None devicehostname=NA deviceowner=NA
Jun 1 10:03:59 192.168.2.41 2021-06-01 10:04:26 reason=Allowed event_id=6968733069742899204 protocol=HTTP_PROXY action=Allowed transactionsize=156 responsesize=65 requestsize=91 urlcategory=URL_web serverip=252.209.18.163 clienttranstime=0 requestmethod=CONNECT refererURL=None useragent=Unknown product=NSS ClientIP=10.52.0.17 status=200 user=stag1@home.com url=https://github.com/DontDoThat79/report vendor=Zscaler clientpublicIP=87.36.16.4 threatcategory=None threatname=None filetype=None appname=GitHub pagerisk=0 department=Default Department urlsupercategory=User-defined appclass=Business dlpengine=None urlclass=Security Risk threatclass=None dlpdictionaries=None fileclass=None bwthrottle=NO servertranstime=0 contenttype=Other unscannabletype=None devicehostname=NA deviceowner=NA
Jun 1 10:04:00 192.168.2.41 2021-06-01 10:04:26 reason=Allowed event_id=6968733069742899205 protocol=HTTP_PROXY action=Allowed transactionsize=156 responsesize=65 requestsize=91 urlcategory=URL_google serverip=54.19.238.34 clienttranstime=0 requestmethod=CONNECT refererURL=None useragent=Unknown product=NSS ClientIP=10.52.0.17 status=200 user=stag1@home.com url=https://www.youtube.com/watch?v=iPGgnzc34tY vendor=Zscaler clientpublicIP=87.36.16.4 threatcategory=None threatname=None filetype=None appname=Youtube pagerisk=0 department=Default Department urlsupercategory=User-defined appclass=Business dlpengine=None urlclass=Security Risk threatclass=None dlpdictionaries=None fileclass=None bwthrottle=NO servertranstime=0 contenttype=Other unscannabletype=None devicehostname=NA deviceowner=NA
```

Sur la ligne 3 on peut remarquer l'adresse suivante :

```
https://github.com/DontDoThat79/report
```

Le lien contient un fichier dump.txt

```bash
$ wget https://raw.githubusercontent.com/DontDoThat79/report/main/dump.txt

$ cat dump.txt

$ RllYQzRMUk9GWVhDNkxaUEY0WFM2TFJPRllYQzRMUk9HUTJTNExST0ZZWEM0TFJQRjRYUzZMWlBGWVhDNExST0ZZWEFVTkpURjRYUzZMWlBGNFhDNExST0ZZWEM0TFJPRjRYUzZMWlBGNFhTNkxaUEY0WFM0TFJPRllYQzRMUk9GWVhDNExST0JJWFM2TFpPRllYQzRMUk9GWVhTNExST0ZZWEM0TFJPRjRYUzZMWlBGWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWTJESUNST0ZZWEM0TFJPRllYQzRMUk9GNFhTNkxaUEc1UkM0TFJPRllYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWUZDNExST0ZZWFM2TFpWR1FYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMWlBGNFhTNkxaUEY0WFM2TFpQRjRYUzZMWUtGNFhTNkxaUEY0WFM2TFpQRjRYQzRMUk9GWVhDNExST0ZZWEM0TFJPRjRYUzZMWk9GWVhDNE5aU0ZZWEM0TFJPRllYQVVMWlBGNFhDNkxaT0ZZWEM0TFJPRjRYUzZMUk9GWVhDNExST0ZZM0dNTFpQRllYQzRMUk9GWVhDNkxST0ZZWEM0TFJPQklYQzRMWlBGNFhTNkxaT0ZZWERPTUJPRllYQzRMUk9GWVhDNExSUEY0WFM2TFpQRjRYUzZMWlBGNFhTNkxaUEY0WFM2Q1JQRllYVEtNWlBGWVhTNkxST0Y0WFM2TFpQRjRYUzZMWlBGWVhDNExST0ZZWFM2TFpPRllYQzRMUk9GWVhDNExST0ZZRkM0TFJPRllYQzZMM0ZIRVhTNkxaUEY0WFM2TFpQRjRYUzZMWlBGNFhTNkxaUEY0WFM2TFpQRjRYUzZMWlBGNFhTNkxZS0Y0WFM2TFpQRjRYUzZMWlBGNFhTNkxaT0ZZWEM0TFJXR01YQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWVhBVUxST0ZZWEM0TFJPRllYQzRMUk9GWTNUS0xaUEY0WFM2TFpQRjRYUzZMWlBGNFhTNkxaUEY0WFM0TFJPRllYQzRMUk9CSVhDNExST0ZZWEM0TFJPRzRaQzZMWlBGNFhTNkxaUEY0WFM2TFJPRllYQzRMUk9GWVhDNkxaT0ZZWEM0TFJPRllYQzRDUlBGNFhTNkxaUEY0WFM2TFpQRjRYUzZMWlBGNFhDNExST0ZZWEM0TFJPRllYRE1PSlBGNFhDNExST0ZZWEM0TFJPRllGQzRMUk9GWVhTNkxaUEY0WFM0TFJPRllYQzRMUk9GWVhDNkxaUEY0WFM2TFJPRllYQzRMUk9GWVhET01aUEY0WFM2TFlLRllYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWVhTNlpKWkY0WFM2TFpQRjRYUzZMWlBGNFhTNkxaUEY0WFFVTFpQRjRYUzZMWlBGNFhTNkxaUEY0WFM2TFJPRllYQzRMUk9GWVhDNExST0ZZWEM0TFpQRjRYUzZMWlBHVVlDNExST0JJWEM0TFJPRllYUzZMWlBGNFhTNkxaUEY0M0dNTFJPRllYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWVhDNENSUEY0WFM2TFpPRllYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMUlBGNFhTNExSWEdVWEM0TFJPRllYQzRMUk9GWUZDNkxaUEZZWEM0TFJPRllYQzRMUlhHSVhTNkxaUEY0WFM2TFpQRjRYUzZMWlBGWVhDNExST0ZZWEM0TFJPRllYQzRMUUtGWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWVhDNkxaVU1RWEM0TFJPRllYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQVVMWlBGNFhTNkxaT0ZZWEM0TFJPRllYQzRMUk9GWVhDNExST0ZZWEM0TlRHRjRYUzZMWlBGNFhTNkxaUEY0WFM2TFpQQklYUzZMWlBGNFhTNkxaUEZZWERNT0pPRllYQzRMUk9GWVhDNExST0ZZWEM0TFJPRllYQzRMUk9GWVhDNExST0ZZWEM0Q1JQRjRYUzZMWlBGNFhTNkxaUEY0WFM2TFpQRjRYUzZMWlBGNFhTNkxaUEY0WFM2TFpQRjQzV0lMWk9GNFhTNkxaUEY0PT09PT09
```

Cela ressemble à du base64.

On va décoder le fichier.

```bash
$ cat dump.txt |base64 -d 

$ FYXC4LROFYXC6LZPF4XS6LROFYXC4LROGQ2S4LROFYXC4LRPF4XS6LZPFYXC4LROFYXAUNJTF4XS6LZPF4XC4LROFYXC4LROF4XS6LZPF4XS6LZPF4XS4LROFYXC4LROFYXC4LROBIXS6LZOFYXC4LROFYXS4LROFYXC4LROF4XS6LZPFYXC4LROFYXC4LROFYXC4LROFY2DICROFYXC4LROFYXC4LROF4XS6LZPG5RC4LROFYXC4LROFYXC4LROFYXC4LROFYXC4LROFYFC4LROFYXS6LZVGQXC4LROFYXC4LROFYXC4LROFYXC4LZPF4XS6LZPF4XS6LZPF4XS6LYKF4XS6LZPF4XS6LZPF4XC4LROFYXC4LROFYXC4LROF4XS6LZOFYXC4NZSFYXC4LROFYXAULZPF4XC6LZOFYXC4LROF4XS6LROFYXC4LROFY3GMLZPFYXC4LROFYXC6LROFYXC4LROBIXC4LZPF4XS6LZOFYXDOMBOFYXC4LROFYXC4LRPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6CRPFYXTKMZPFYXS6LROF4XS6LZPF4XS6LZPFYXC4LROFYXS6LZOFYXC4LROFYXC4LROFYFC4LROFYXC6L3FHEXS6LZPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6LYKF4XS6LZPF4XS6LZPF4XS6LZOFYXC4LRWGMXC4LROFYXC4LROFYXC4LROFYXC4LROFYXAULROFYXC4LROFYXC4LROFY3TKLZPF4XS6LZPF4XS6LZPF4XS6LZPF4XS4LROFYXC4LROBIXC4LROFYXC4LROG4ZC6LZPF4XS6LZPF4XS6LROFYXC4LROFYXC6LZOFYXC4LROFYXC4CRPF4XS6LZPF4XS6LZPF4XS6LZPF4XC4LROFYXC4LROFYXDMOJPF4XC4LROFYXC4LROFYFC4LROFYXS6LZPF4XS4LROFYXC4LROFYXC6LZPF4XS6LROFYXC4LROFYXDOMZPF4XS6LYKFYXC4LROFYXC4LROFYXC4LROFYXC4LROFYXS6ZJZF4XS6LZPF4XS6LZPF4XS6LZPF4XQULZPF4XS6LZPF4XS6LZPF4XS6LROFYXC4LROFYXC4LROFYXC4LZPF4XS6LZPGUYC4LROBIXC4LROFYXS6LZPF4XS6LZPF43GMLROFYXC4LROFYXC4LROFYXC4LROFYXC4LROFYXC4CRPF4XS6LZOFYXC4LROFYXC4LROFYXC4LROFYXC4LRPF4XS4LRXGUXC4LROFYXC4LROFYFC6LZPFYXC4LROFYXC4LRXGIXS6LZPF4XS6LZPF4XS6LZPFYXC4LROFYXC4LROFYXC4LQKFYXC4LROFYXC4LROFYXC4LROFYXC6LZUMQXC4LROFYXC4LROFYXC4LROFYXC4LROFYXAULZPF4XS6LZOFYXC4LROFYXC4LROFYXC4LROFYXC4NTGF4XS6LZPF4XS6LZPF4XS6LZPBIXS6LZPF4XS6LZPFYXDMOJOFYXC4LROFYXC4LROFYXC4LROFYXC4LROFYXC4LROFYXC4CRPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6LZPF4XS6LZPF43WILZOF4XS6LZPF4======
```

On obtient ce qui ressemble à du base32.

```bash
$ cat dump.txt |base64 -d |base32 -d

$.......//////.......45.......//////.......
53//////.........////////////.............
///......./......../////................44
.........../////7b........................
....///54.................////////////////
///////////..............////....72.......
///.//......///........6f//......./.......
..//////...70..........///////////////////
/./53/.//..//////////......///............
.....//e9/////////////////////////////////
//////////////.....63.....................
.............75///////////////////........
.........72///////////.........//.........
/////////////////...........69//..........
....//////..........//////.........73/////
.....................//e9/////////////////
///////////////...............///////50...
.....//////////6f.........................
/////....................///..75..........
///.........72//////////////..............
.................//4d.....................
//////...................6f///////////////
/////////..69.............................
////////////////////////////////7d/.//////
```

On peut voir des caractères ascii en hex.

On va supprimer les caractères "." et "/" ainsi que les retours chariots


```bash
$ cat dump.txt |base64 -d |base32 -d |sed -e 's/\.//g' -e 's/\///g' |tr -d "\n"

$4553447b54726f7053e96375726973e9506f75724d6f697d

```

On va transformer l'hexadécimal en ascii :

```bash
cat dump.txt |base64 -d |base32 -d |sed -e 's/\.//g' -e 's/\///g' |tr -d "\n" |xxd -r -p

$ ESD{TropSécuriséPourMoi}
```

```bash
Le flag est : ESD{TropSécuriséPourMoi}
```

<!-- Ok F : 20220424 -->
