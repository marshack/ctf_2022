# Write up Forensic/Dockersick

## Consigne: 

```
Retrouver le flag dans l'image docker suivante : docker pull blackknigth/dockersick
```

## Pièce jointe :

```
Néant
```

## Serveur :

```
CTFD
```

## Points attribués :

```
20
```

## Hint : 

```
openssl
```

## Flag :

```
fl@g{L0v3_D0cker_For3nsic}
```

## Soluce :

Récupérons l'image en local:

```bash
$ docker pull blackknigth/dockersick
Using default tag: latest
latest: Pulling from blackknigth/dockersick
Digest: sha256:2f68a825b5e50495649a735894a65527a61b2cdfb337dc1edac7e7e2e5411811
Status: Downloaded newer image for blackknigth/dockersick:latest
docker.io/blackknigth/dockersick:latest
```

On peut voir que l'on a bien récupéré l'image en local

```bash
$ docker image ls
REPOSITORY               TAG       IMAGE ID       CREATED        SIZE
blackknigth/dockersick   latest    3236fbc596e6   12 hours ago   122MB
```

On peut commençer à analyser l'historique de construction de l'image

```bash
$ docker history blackknigth/dockersick:latest --no-trunc
IMAGE                                                                     CREATED        CREATED BY                                                                                                                                                                        SIZE      COMMENT
sha256:3236fbc596e6d70c8cf9dc22d7d5add69b46343c88bf32ca438e28efd25cb523   13 hours ago   |1 DEBIAN_FRONTEND=noninteractive /bin/sh -c rm /pass.txt                                                                                                                         0B        
sha256:6ed89f7beeb3d366a2eba35ee56392a9ab5fa043c7280134ab0baff5292671cb   13 hours ago   |1 DEBIAN_FRONTEND=noninteractive /bin/sh -c echo -n $(curl -s https://pastebin.com/raw/Xafq96YT) | openssl enc -aes-256-cbc -iter 10 -pass pass:$(cat /pass.txt) -out flag.enc   48B       
sha256:e0d83aea7dd80735ba7c5ca3cad2a1642a9cbb912e8a4eed2c06912a22c9efe3   13 hours ago   /bin/sh -c #(nop) COPY file:a06a80b41440786de1851a146492f35672238db4f8266209ff2d3064054d9ab9 in /pass.txt                                                                         36B       
sha256:16efe053ab905473ea137c44aafa1a86d6e089bd7f0435d6a00b6729852648f5   13 hours ago   |1 DEBIAN_FRONTEND=noninteractive /bin/sh -c apt install -y openssl curl                                                                                                          16.2MB    
sha256:266118a0e00643b78a4611411b2dadfe17f6b298861e29e21b00b25b4e98291d   13 hours ago   |1 DEBIAN_FRONTEND=noninteractive /bin/sh -c apt update                                                                                                                           32.8MB    
sha256:500d1326b93c22806002ad742ef10af1695637f48523af07f72e27b3e7ac0992   13 hours ago   /bin/sh -c #(nop)  ARG DEBIAN_FRONTEND=noninteractive                                                                                                                             0B        
sha256:d13c942271d66cb0954c3ba93e143cd253421fe0772b8bed32c4c0077a546d4d   10 days ago    /bin/sh -c #(nop)  CMD ["bash"]                                                                                                                                                   0B        
<missing>
```

On peut constater la copie puis la suppression d'un fichier appelé pass.txt qui sert de mot de passe de chiffrement avec openssl du contenu d'un pastebin qui n'est plus accessible.
Le fichier chiffré flag.enc devrait se trouver à la racine du système de fichier .

- COPY file:a06a80b41440786de1851a146492f35672238db4f8266209ff2d3064054d9ab9 in /pass.txt
- echo -n $(curl -s https://pastebin.com/raw/Xafq96YT) | openssl enc -aes-256-cbc -iter 10 -pass pass:$(cat /pass.txt) -out flag.enc
- rm /pass.txt

Connectons nous au conteneur afin de voir ce qui est accessible à l'intérieur.

```bash
$ docker run -it blackknigth/dockersick bash
root@464475c82b78:/# ls -al
total 60
drwxr-xr-x   1 root root 4096 Jan 17 20:41 .
drwxr-xr-x   1 root root 4096 Jan 17 20:41 ..
-rwxr-xr-x   1 root root    0 Jan 17 20:41 .dockerenv
lrwxrwxrwx   1 root root    7 Jan  5 16:47 bin -> usr/bin
drwxr-xr-x   2 root root 4096 Apr 15  2020 boot
drwxr-xr-x   5 root root  360 Jan 17 20:41 dev
drwxr-xr-x   1 root root 4096 Jan 17 20:41 etc
-rw-r--r--   1 root root   48 Jan 17 08:16 flag.enc
drwxr-xr-x   2 root root 4096 Apr 15  2020 home
lrwxrwxrwx   1 root root    7 Jan  5 16:47 lib -> usr/lib
lrwxrwxrwx   1 root root    9 Jan  5 16:47 lib32 -> usr/lib32
lrwxrwxrwx   1 root root    9 Jan  5 16:47 lib64 -> usr/lib64
lrwxrwxrwx   1 root root   10 Jan  5 16:47 libx32 -> usr/libx32
drwxr-xr-x   2 root root 4096 Jan  5 16:47 media
drwxr-xr-x   2 root root 4096 Jan  5 16:47 mnt
drwxr-xr-x   2 root root 4096 Jan  5 16:47 opt
dr-xr-xr-x 314 root root    0 Jan 17 20:41 proc
drwx------   2 root root 4096 Jan  5 16:50 root
drwxr-xr-x   5 root root 4096 Jan  5 16:50 run
lrwxrwxrwx   1 root root    8 Jan  5 16:47 sbin -> usr/sbin
drwxr-xr-x   2 root root 4096 Jan  5 16:47 srv
dr-xr-xr-x  13 root root    0 Jan 17 20:41 sys
drwxrwxrwt   1 root root 4096 Jan 17 08:16 tmp
drwxr-xr-x   1 root root 4096 Jan  5 16:47 usr
drwxr-xr-x   1 root root 4096 Jan  5 16:50 var
```

Notre fichier flag.enc est bien présent à la racine .

```bash
root@464475c82b78:/# cat flag.enc 
Salted__�b�}d���<��l�:/yb���rk��ans8l�N Ťk��
```

Il faut maintenant que l'on arrive à trouver le contenu du fichier pass.txt afin de pouvoir déchiffrer notre fichier flag.enc.

On peut sauvegarder le contenu de l'image en local et chercher dans les layers le fichier pass.txt.

```bash
$ docker save blackknigth/dockersick > dockersick.tar
```

Il faut extraire l'archive tar pour voir son contenu .

```bash
$ tar -xvf dockersick.tar 
3236fbc596e6d70c8cf9dc22d7d5add69b46343c88bf32ca438e28efd25cb523.json
4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69/
4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69/VERSION
4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69/json
4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69/layer.tar
4f0bec3d024ed1868b8f77eaa3678a4ce38c17577baa5a0552dfb375d0850e92/
4f0bec3d024ed1868b8f77eaa3678a4ce38c17577baa5a0552dfb375d0850e92/VERSION
4f0bec3d024ed1868b8f77eaa3678a4ce38c17577baa5a0552dfb375d0850e92/json
4f0bec3d024ed1868b8f77eaa3678a4ce38c17577baa5a0552dfb375d0850e92/layer.tar
9af5e4767bb70fba0d28455965e20ac1066ea93d931148d8742fdb9ad84dbba7/
9af5e4767bb70fba0d28455965e20ac1066ea93d931148d8742fdb9ad84dbba7/VERSION
9af5e4767bb70fba0d28455965e20ac1066ea93d931148d8742fdb9ad84dbba7/json
9af5e4767bb70fba0d28455965e20ac1066ea93d931148d8742fdb9ad84dbba7/layer.tar
a1c96971b0983b59b51e57259d71aee4084de196310c876ca50f21e1e88a973c/
a1c96971b0983b59b51e57259d71aee4084de196310c876ca50f21e1e88a973c/VERSION
a1c96971b0983b59b51e57259d71aee4084de196310c876ca50f21e1e88a973c/json
a1c96971b0983b59b51e57259d71aee4084de196310c876ca50f21e1e88a973c/layer.tar
b48f88eeadad085ebbab713ed73c70d859adaacd4f92dc116c709d6b5c74ce1b/
b48f88eeadad085ebbab713ed73c70d859adaacd4f92dc116c709d6b5c74ce1b/VERSION
b48f88eeadad085ebbab713ed73c70d859adaacd4f92dc116c709d6b5c74ce1b/json
b48f88eeadad085ebbab713ed73c70d859adaacd4f92dc116c709d6b5c74ce1b/layer.tar
c6405be4a3d586eed3c5040eb21964e0eb0d47ed9bd848100de21f51db3a551a/
c6405be4a3d586eed3c5040eb21964e0eb0d47ed9bd848100de21f51db3a551a/VERSION
c6405be4a3d586eed3c5040eb21964e0eb0d47ed9bd848100de21f51db3a551a/json
c6405be4a3d586eed3c5040eb21964e0eb0d47ed9bd848100de21f51db3a551a/layer.tar
manifest.json
repositories
```

Script bash permettant de lister le contenu des layers .

```bash
$ vi liste_archive.sh
#!/usr/bin/env bash

for file in "$@"
do
    printf "\n-----\nArchive '%s'\n-----\n" "$file"
    ## Get the file's extension
    ext=${file##*.}
    ## Special case for compressed tar files. They sometimes
    ## have extensions like tar.bz2 or tar.gz etc.
    [[ "$(basename "$file" ."$ext")" =~ \.tar$ ]] && ext="tgz"

    case $ext in
        7z)
            type 7z >/dev/null 2>&1 && 7z l "$file" || 
            echo "ERROR: no 7z program installed"
            ;;
        tar|tbz|tgz)
            type tar >/dev/null 2>&1 && tar tf "$file"|| 
            echo "ERROR: no tar program installed"
            ;;
        rar)
            type rar >/dev/null 2>&1 && rar v "$file"|| 
            echo "ERROR: no rar program installed"
            ;;
        zip)
            type zip >/dev/null 2>&1 && zip -sf "$file"|| 
            echo "ERROR: no zip program installed"
            ;;
        *)
            echo "Unknown extension: '$ext', skipping..."
            ;;
    esac
done

$ chmod +x liste_archive.sh
```

Il ne nous reste plus qu'à ressortir les archive tars des layers et exécuter notre script de listing.

```bash
$ find . -name "layer*.tar" | xargs -I % sh -c './liste_archive.sh %;'

.... SNIP ....
Archive './c6405be4a3d586eed3c5040eb21964e0eb0d47ed9bd848100de21f51db3a551a/layer.tar'
-----
.wh.pass.txt

-----
Archive './4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69/layer.tar'
-----
pass.txt
```

On peut voir que notre fichier pass.txt se trouve dans le layer dont le checksum sha256 est 4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69.

Allons regarder son contenu .

```bash
 tar xvf ./4a261d73137f8c60008c1cc3a4d029273838970c15973a4be85cb9374b00df69/layer.tar
pass.txt

$ cat pass.txt 
V3ry_Str0ng_Passw0rd_f0r_3ncrypti0n
```

On a maintenant le password qui a servi à chiffer le contenu du pastebin.

On peut ainsi déchiffrer le fichier flag.enc.

```bash
$ docker run -it blackknigth/dockersick bash
```

Se connecter au conteneur et exécuter la commande de déchiffrement que le fichier flag.enc.

```bash
root@464475c82b78:/# openssl enc -d -aes-256-cbc -salt -pass pass:V3ry_Str0ng_Passw0rd_f0r_3ncrypti0n -iter 10 -in flag.enc -out original.txt

root@464475c82b78:/# cat original.txt 
fl@g{L0v3_D0cker_For3nsic}
```

Le flag est : fl@g{L0v3_D0cker_For3nsic}

<!-- Ok F : 20220424 -->
