# Write up

## Consigne:

```
Les renseignements nous informent que le site web d’un groupe de hacker classé à haut risque a été identifié. D'après nos sources, ils détiennent une information essentielle. 
Votre mission, si toutefois vous l’acceptez, est de vous introduire dans leur serveur pour récupérer cette information.


Démarrer un environnement virtuel. Attention, la durée de vie de l'environnement est limitée.
<div>
<button class="btn btn-info" onclick="window.open('/portailvms/11','_blank')" >Accéder au portail</button>
</div>
<p></p>
Bonne chance !
```

## Pièce jointe :

```
Néant
```

## Serveur :

```
Portailvms
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{H4st4_l4_V1st4_B4by!}
```

## Solution : 

Si on essaye d'afficher une page web à l'adresse de la machine, on arrive sur une page d'aceuil avec plusieurs catégories (Accueil, Team 404, H4CK’Tu, Docs et Login)
Les deux qui ont un intérêt dans le cadre de la résolution sont en premier lieu «Docs» et «Login».

Sur la page Docs:
Plusieurs outils sont présentés, et un indice majeur est présent. Sous chacun des articles, nous pouvons remarquer le sigle «@author: terminator».
«terminator» est donc un potentiel utilisateur de la cible.
«FEROXBUSTER» est aussi un indice, cet outil nous permettra de retrouver une page lors de la phase d’énumération.

Allons maintenant sur la page Login:
Une page de connexion classique développée en PHP. Il est bien notifié qu’il ne faut pas faire d’attaque de type brute force, les identifiants sont ailleurs sur le site.

Énumération :
Nous allons maintenant lister les points finaux d’url de la cible (endpoint enumeration). Pour cela, nous pouvons utiliser l’outil Feroxbuster présenté sur la page Docs (avec le lien vers le github associé). 
Cet outil nous permet d’effectuer de la recherche récursive, ce qui sera nécessaire pour ce challenge:

![image01](images/feroxbuster.jpg)

```
Détail de la commande:
	-u 	= URL cible (ici http://ip)
	-w	= Wordlist (ici la wordlist générique de dirbuster)
	-t	= Thread 
```

Résultat de la commande:

![image02](images/enum.jpg)

La recherche récursive nous à permis de repérer des pages non indexées présentes sur le site.
`/team/dev` semble prometteuse.

Exploitation - page en développement

Allons sur la page /team/dev/:
Nous avons un tableau listant des données. L’ensemble est encodé en base64. Voici le décodage des titres:

```
aWQ		= id
dXNlcm5hbWU	= username
```

L'utilisateur interessant est celui ci: dGVybWluYXRvcg== (terminator que nous avions vu dans la page docs)
Cela confirme la présence de cet utilisateur.
Maintenant que nous avons les noms d’utilisateurs, il faut en extraire les mots de passe. Le tableau étant généré par un code php qui récupère les données sur une base de donnée sql, nous ne trouverons rien dans le code source de la page web.
Jetons un œil si des cookies sont présents en allant dans l’inspecteur d’éléments du navigateur (ici Firefox):

![image03](images/firefox.jpg)

Nous avons un cookie nommé «Query» («requête» en français). Sa valeur est encodée en base64, il faut la décoder pour en comprendre son contenu:

	Encodé	= YVdRLCBkWE5sY201aGJXVQ%3D%3D
	Décodé	= aWQ, dXNlcm5hbWU

Nous retrouvons les noms des colonnes du tableau de la page en question. Il s’agit d’une partie de la requête SQL effectuée par le code PHP pour récupérer les informations contenues dans la base de donnée. La requête complète s’apparente à celle-ci:

	SELECT aWQ, dXNlcm5hbWU FROM table_name;

Il faut remplacer «aWQ, dXNlcm5hbWU» par «*» pour afficher l’ensemble des données (et potentiellement les mots de passe). Pour ce faire, il faut encoder le caractère «*» en base64:

	Décodé	= *
	Encodé	= Kg==

Il faut maintenant modifier le cookie en remplacent sa valeur par ce que nous venons d’encoder.
Il faut maintenant recharger la page pour que la modification du cookie soit prise en compte. Voici le résultat:

![image04](images/password.jpg)

Une nouvelle colonne est apparue. Elle est aussi encodée en base64, il faut donc la décoder:

	Encodé	= cGFzc3dvcmQ
	Décodé	= password

Il s’agit des mots de passe des utilisateurs listés. Nous savons que l’ID 3 correspond à l’utilisateur «terminator», décodons son mot de passe:

	Encodé	= QnJ1dDNmMHJjZTFzbid0bjNjM3NzNHJ5dGgzcjFnaHRzMGx1dDEwbgo=
	Décodé	= Brut3f0rce1sn'tn3c3ss4ryth3r1ghts0lut10n

Allons maintenant sur la page «Login» pour nous connecter avec l'identifiant/mot de passe de "terminator"
Nous sommes maintenant sur la page «admin» en tant qu’utilisateur «terminator».

Exploitation - page admin
Une fois sur la page d’admin, nous remarquons la présence d’un outil permettant d’effectuer des tests de ping:

![image05](images/ping.jpg)

La «box» est protégée par un filtre de valeur, seule une IP peut être entrée.
Regardons ce qui se passe dans l’URL lorsqu’il y a une erreur avec la commande «127.0.0.1; ls»:

![image06](images/ls.jpg)

Les options suivantes sont ajoutées en fin d’URL:

	?ip=8.8.8.8%3B+ls&state=error

Nous retrouvons l’adresse IP testée dans la commande avec l’injection «; ls» encodée en «URL encode». La partie «state=error» indique une erreur, ce qui bloque notre injection.

Il faut échanger «error» par «valid». Regardons le résultat:

![image07](images/result.jpg)

Les fichiers sont bien listés et nous pouvons remarquer la présence d’une clé («terminator.key»).
L’objectif maintenant est de lire le contenu du fichier «terminator.key».
Changeons notre commande «ls» par «cat terminator.key»;
Nous arrivons bien à afficher le fichier voulu.
Il s’agit d’une clé privée ssh, elle nous permettra de nous connecter au serveur.
Il faut donc la copier pour créer un fichier de clé ssh, ce qui nous permettra de progresser.

Scope n°2 - SYSTEME
Identification du point d’entrée
Comme nous l’avons vu dans le point précédent, notre point d’entrée sur le serveur sera une connexion ssh via une clé privée.

Regardons les notes présentes sur la page «admin»:

```
Hello terminator, little reminder to connect to the server, your username is also 'terminator'.
Don't forget, we hate those who try to enter without knocking ;)
```

Il nous est rappelé que le nom d’utilisateur à utiliser pour le ssh est «terminator». Cependant, une sécurité supplémentaire est présente, il s’agit de «port knocking». Ceci est insinué dans la phrase suivante:
«N’oublie pas, nous détestons ceux qui essaient d’entrer sans toquer»
Si nous ne «toquons» pas la série de port définie sur la cible, le ssh sera bloqué. Regardons les autres notes présentent:

```
31337 - Back orifice, long live the rootkits
https://en.wikipedia.org/wiki/Back_Orifice
1969 - The Hero was born
https://en.wikipedia.org/wiki/Linus_Torvalds
2006 - We don't like secrets
https://en.wikipedia.org/wiki/WikiLeaks
```

Nous avons quelques notes historiques
Ceci est un indice, le port et les dates listées ici sont les ports à utiliser pour le port knocking, qui sont les suivants:

	31337
	1969
	2006

Connexion sur la cible

Il nous faut créer la clé ssh sur notre système:

1- Créer et éditer le fichier qui servira de clé ssh

2- Coller la clé ssh dans le fichier:

```
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBGC869VnvvrBZq/Zr2w4pK59S66MPtwJDiBTo6PumGSgAAAJjRaMMD0WjD
AwAAAAtzc2gtZWQyNTUxOQAAACBGC869VnvvrBZq/Zr2w4pK59S66MPtwJDiBTo6PumGSg
AAAECwUb67YdLG/3n4XRJTYj3heDibvNs5UYOJsfXuxT2/AkYLzr1We++sFmr9mvbDikrn
1Lrow+3AkOIFOjo+6YZKAAAAD3N0cmFuZ2V1c2VyQHBvYwECAwQFBg==
-----END OPENSSH PRIVATE KEY-----
```

3- Définir les bons droits sur le fichier

Maintenant que la clé est prête, nous pouvons initier le port knocking. Pour ce faire, il faut utiliser l’outil «knock»:

![image08](images/knock.jpg)

Une fois le port knocking fait, nous n’avons que 15 secondes pour nous connecter en ssh avec la clé SSH récupéré
Bienvenue, vous êtes connectés sur le serveur.

Élévation de privilège

Être connecté en tant que «terminator» sur le serveur, c’est bien, mais être en tant que «root», c’est mieux.

La «sudo» peut être exploitable si des commandes y sont autorisées. Nous pouvons les lister via la commande «sudo -l».
Le mot de passe a renseigner est celui recuperer sur le site web dans la partie /team/dev
Nous remarquons que le programme «find» est autorisé à être exécuté avec les droits «root». Il s’agit d’une vulnérabilité car «find» peut permettre le lancement de sous-process, ce qui est exploitable.
L’exploitation de la vulnérabilité se fait de la manière suivante:

```
sudo -u root /usr/bin/find /home -exec /bin/sh \;
```

Voila, on est root, on a donc accès a toute l'arborescence, il ne nous reste plus qu'à trouver le flag ( `/flag.txt`)

<!-- Ok F : 20220423 -->