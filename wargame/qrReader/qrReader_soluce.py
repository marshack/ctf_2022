#!/usr/bin/env python3

"""
   Mars@Hack 2022 - qrReader - Taz

   Pré-requis : pip3 install pyzbar opencv-python requests pillow

   Ce fichier permet de résoudre la totalité des challenges proposés

"""

from pyzbar import pyzbar
import sys
import cv2
import requests
import re
import base64
import os
from io import BytesIO
from PIL import Image, ImageDraw
import PIL.ImageOps
import numpy as np

bDebug = False # Affiche ou non les messages de débuggage
iTailleCarre = 4 # Taille d'un carré affiché dans un qrCode
iTailleBordure = 4 * iTailleCarre # Taille de la bordure d'un pattern
iTaillePattern = ( 7 ) * iTailleCarre # Taille d'un pattern
iZoneADecouper =  iTailleBordure + iTaillePattern
sFlagPattern = 'fl@g{(.*)}' # regex d'un flag à trouver

oSession = requests.Session() # objet de request
aCookies = {} # cookie de la session du challenge

def drawPattern( pIMG, pX, pY ):
    '''
       Dans certaines images, le pattern a été supprimé, on le redessine
       en superposant 3 carrés (noir, blanc, noir) en partant de la position pX, pY
    '''
    if bDebug: print("  => Dessin du pattern (carré de controle)")
    oIMG = ImageDraw.Draw(pIMG)
    iDimension = iTaillePattern - 1
    oIMG.rectangle((pX, pY, pX + iDimension, pY + iDimension ), fill ="#000000")

    pX += iTailleCarre
    pY += iTailleCarre
    iDimension -= iTailleCarre * 2
    oIMG.rectangle((pX, pY, pX + iDimension, pY + iDimension ), fill ="#ffffff")

    pX += iTailleCarre
    pY += iTailleCarre
    iDimension -= iTailleCarre * 2
    oIMG.rectangle((pX, pY, pX + iDimension, pY + iDimension ), fill ="#000000")

    return pIMG

def revertIfBlack( pIMG ):
    '''
       Certaines images sont en couleurs inversées
       On controle les 4 coins pour savoir si au moins 3 sont noirs
       - Si oui alors l'image est en reverse, on la remet dans la bonne couleur
       - Si non, alors on ne fait rien

       Pour cela, on additionne les valeurs (r,g,b) de chaque pixel.
       pixel noir : (0, 0, 0)
       pixel blanc : (255, 255, 255)

       Si la somme est inférieur à 766 alors au moins 3 carrés sont noirs
    '''
    if bDebug: print("  => Controle si le carré est inversé")
    iHeight, iWidth = pIMG.size
    iHeight -= 1
    iWidth -= 1

    aGrid = pIMG.load() # L'image est tranformée en array(x,y) contenant le RGB du pixel

    iTopLeftR, iTopLeftG, iTopLeftB = aGrid[0,0]
    iTopRightR, iTopRightG, iTopRightB = aGrid[0,iHeight]
    iBottomLeftR, iBottomLeftG, iBottomLeftB = aGrid[iWidth,0]
    iBottomRightR, iBottomRightG, iBottomRightB = aGrid[iWidth,iHeight]
    iRefR = iTopLeftR + iTopRightR + iBottomLeftR + iBottomRightR
    iRefG = iTopLeftG + iTopRightG + iBottomLeftG + iBottomRightG
    iRefB = iTopLeftB + iTopRightB + iBottomLeftB + iBottomRightB
    # On additionne tous les RGB des 4 coins
    iRefTotal = iRefR + iRefG + iRefB

    if iRefTotal < 766 :
       if bDebug: print("     => Le carré est blanc sur noir, on inverse")
       pIMG = PIL.ImageOps.invert(pIMG)
    else:
       if bDebug: print("     => Le carré est dans la bonne couleur")

    return pIMG

def quelEmplacement( pIMG ):
    """
       On recherche quel est l'emplacement de l'image en cours
       Selon les challenges l'image n'est pas au même endroit
       et le pattern (carré de controle) est présent ou pas.
       Sur les challenges n°4 & 5, la position des images est aléatoire

       Pour trouver où se place l'image, on cherche :
            - soit un grand carré blanc dans un coin de la taille des pattern
            - soit le pattern lui-même dans un coin s'il n'a pas été effacé

       Pour cela, on additionne le poids de chaque pixel (r,g,b) sur un carré de côté ( bordure blanche + taille du pattern ) :
            - le carré blanc est égal à : 1224000 => ( 40 * 40 * ( 255 + 255 + 255 ) )
            - un pattern non effacé : 979200
    """
    iHeight, iWidth = pIMG.size
    iNombreDeCarresDansLImage = iHeight / 4; # 1 carre de base ( 1 "pixel qrCode" ) de qrCode = 4 pixels
    tZoneADecouper = iZoneADecouper - iTailleCarre
    iTotalDesPixelsVides = 1224000
    iTotalDuPattern = 979200

    if bDebug: print("  => Controle de la position de l'image")

    # On découpe une zone de la taille du pattern
    # puis on additionne tous les RGB des pixels de cette zone
    # Si la somme est 1224000 alors c'est un pattern effacé
    # Si la somme est 979200 c'est un pattern non effacé
    # Dans ces deux cas, on sait quelle est la position de cette image

    # On teste haut-gauche
    oCrop = pIMG.crop((0, 0, tZoneADecouper, tZoneADecouper))
    iSommeDesPixels = np.asarray(oCrop, dtype="int32").sum()
    sReturn = ""
    if ( iSommeDesPixels == iTotalDesPixelsVides or iSommeDesPixels == iTotalDuPattern):
      if bDebug: print("     => C'est le carré en haut à gauche")
      sReturn =  "TopLeft"
    else:
      # On teste haut-droit
      oCrop = pIMG.crop((iWidth - tZoneADecouper, 0, iWidth, tZoneADecouper))
      iSommeDesPixels = np.asarray(oCrop, dtype="int32").sum()
      if ( iSommeDesPixels == iTotalDesPixelsVides or iSommeDesPixels == iTotalDuPattern):
         if bDebug: print("     => C'est le carré en haut à droite")
         sReturn = "TopRight"
      else:
        # On teste bas-gauche
        oCrop = pIMG.crop((0, iHeight - tZoneADecouper, tZoneADecouper, iHeight))
        iSommeDesPixels = np.asarray(oCrop, dtype="int32").sum()
        if ( iSommeDesPixels == iTotalDesPixelsVides or iSommeDesPixels == iTotalDuPattern):
           if bDebug: print("     => C'est le carré en base à gauche")
           sReturn = "BottomLeft"
        else:
           # On a rien trouvé, c'est le carré bas droit qui ne contient pas de pattern
           if bDebug: print("     => On a rien trouvé, par défaut c'est le carré en bas à droite")
           sReturn = "BottomRight"
    return sReturn

def getCode ( pIMG ):
    '''
       On récupère une image complète et on essaye de lire le qrCode
    '''
    if bDebug: print("=> Lecture du qrCode final")
    oDecode = pyzbar.decode(pIMG)
    if ( oDecode != [] ):
        sCode = oDecode[0].data.decode("utf-8")
        if bDebug:
            print("   ", end="")
        print ("=> Le code est :",sCode)
    else:
        print( "" )
        print( "#" * 80 )
        print( "Erreur de lecture, relancez le script" )
        print( "#" * 80 )
        print( "" )
        exit(9)
    return sCode

def getFlag( pHTML ):
    '''
       Le code lu a été soumis, on recherche le flag
    '''
    # On cherche si la chaine flag existe
    aFlag = re.findall(sFlagPattern, pHTML)
    if ( aFlag == []) :
        aTooLate = re.findall("Le temps est dépassé", pHTML)
        if ( aTooLate != [] ):
            sFlag = "Le temps est dépassé"
        else:
            print( "" )
            print( "#" * 80 )
            print( "Erreur de lecture, relancez le script" )
            print( "#" * 80 )
            print( "" )
            exit(9)
    else:
        sFlag = sFlagPattern.replace('(.*)',aFlag[0])
    if bDebug: print('')
    print ("=> Le flag est : " +  sFlag)
    if bDebug: print('')
    return sFlag


def goChallenge( pIndex ):
   # Changez pour l'url du serveur
   sTmpURL = sURL + '?iLevel=' + str(pIndex)

   oHTML = oSession.get(sTmpURL, cookies = aCookies)

   # Si c'est le premier challenge, on sauvegarde le cookies
   if ( aCookies == {} ):
        aCookies['PHPSESSID'] = oSession.cookies['PHPSESSID']

   # Pour le fun, on cherche le titre du challenge
   aTITLE = re.findall(r'<span class="badge badge-primary">(.*)</span>', oHTML.text)


   # on cherche les images PNG présentes dans la page
   aIMG = re.findall(r'base64,(.*)" ', oHTML.text)

   print('='*60)
   print(aTITLE[0])
   print('-'*10)
   iIndex = 0

   iIMGSize = 0

   if len(aIMG) > 1:
     for eachIMG in aIMG:
         if bDebug: print("=> Image " + str(iIndex))
         sIMG = base64.b64decode( eachIMG )
         oMemFile = BytesIO(sIMG)  # convert image to file-like object
         oIMG = Image.open(oMemFile)   # img is now PIL Image object
         if bDebug: oIMG.save(str(iIndex) + "_1_originale.png")

         if iIMGSize == 0:
            # On crée l'image qui va reconstituer le qrCode
            iIMGSize = oIMG.size[0]
            oIMGQRCode = Image.new("RGB", (iIMGSize* 2, iIMGSize* 2), (255, 255, 255))

         oIMG = revertIfBlack(oIMG)
         if bDebug: oIMG.save(str(iIndex) + "_2_revert.png")
         # On détermine quel est la position de cette image et du coup l'emplacement du pattern
         sEmplacement = quelEmplacement(oIMG)

         # Ajout du pattern
         if sEmplacement == "TopLeft":
                    iX,iY = (0, 0)
                    oIMG = drawPattern(oIMG, iTailleBordure, iTailleBordure)
         elif sEmplacement == "TopRight":
                    iX,iY = (iIMGSize, 0)
                    oIMG = drawPattern(oIMG, iIMGSize - iTailleBordure - iTaillePattern, iTailleBordure)
         elif sEmplacement == "BottomLeft":
                    iX,iY = (0, iIMGSize)
                    oIMG = drawPattern(oIMG, iTailleBordure, iIMGSize - iTailleBordure - iTaillePattern)
         else:
                    iX,iY = (iIMGSize, iIMGSize)
         if bDebug: oIMG.save(str(iIndex) + "_3_pattern.png")

         oIMGQRCode.paste( oIMG, (iX, iY))
         if bDebug: oIMGQRCode.save(str(iIndex) + "_4_qrcode_final.png")

         iIndex += 1
   else:
     sIMG = base64.b64decode( aIMG[0] )
     oMemFile = BytesIO(sIMG)  # convert image to file-like object
     oIMGQRCode = Image.open(oMemFile)   # img is now PIL Image object

   if bDebug: oIMGQRCode.save("5_qrcode_final.png")

   sCode = getCode(oIMGQRCode)

   # On envoit le code trouvé dans le qrCodes
   if bDebug: print("=> Envoi du code : " + sCode )
   aPostParams = {'sPlayerResponse': sCode}
   oHTML = oSession.post(sTmpURL, data = aPostParams, cookies=aCookies)
   getFlag(oHTML.text)


if __name__ == "__main__":
    bDebug = False
    sURL = "http://127.0.0.1"

    for iIndex in range(0,6):
        goChallenge(iIndex)
