# Write up Mysterio - Partie 2

## Consigne :

```
Vous êtes maintenant connecté sur l'interface web avec un compte privilégié, mais il reste encore un flag à trouver ...

Pour réaliser ce challenge, vous devez avoir réussi "*Mysterio - part 1*", et utiliser le même environnement virtuel.
```

## Pièce jointe :

*Néant*

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

`fl@g{Nice_view_bro}`

## Solution :

ESD

### Partie 2

On nous parle de mot de passe SSH caché dans une image (*"Au cas où j'oublierai mon mot de passe ssh, mon image est là pour moi krkr"*).

Le lien hypertexte nous amène vers l’URL http://xxx.xxx.xxx.xxx/reminderofbruce.jpg, qui contient une image.

Le mot de passe est contenu dans les metadonnées de l’image, on peut l’obtenir par exemple avec l’outil "exiftool" sur Linux ou un clic droit -> Propriétés -> Détails sur Windows. La section Comment XP contient le mot de passe :
```bash
exif reminderofbruce.jpg 
Marqueurs EXIF dans'reminderofbruce.jpg' (ordre des octets 'Motorola') :
--------------------+----------------------------------------------------------
Marqueur            |Valeur
--------------------+----------------------------------------------------------
Commentaire XP      |Rappel password : I'm_a_fool
Bourrage            |2060 octets de données inconnues
Résolution X        |72
Résolution Y        |72
Unité de la résoluti|pouces
Bourrage            |2060 octets de données inconnues
Version d’Exif      |Version d'exif 2.1
FlashPixVersion     |FlashPix version 1.0
Espace des couleurs |Erreur interne (valeur 65535 inconnue)
--------------------+----------------------------------------------------------
```

On se connecte en SSH avec l’utilisateur Bruce et le mot de passe "I'm_a_fool" et on obtient une session SSH.
On voit un fichier Flag2.txt qui nous indique que le flag n'est pas loin, si je regarde les fichiers cachés avec « ls -la », je remarque un fichier caché .Flag2.txt qui contient le deuxième flag :
```bash
ssh -p 222 bruce@172.102.100.100

bruce@172.102.100.100's password: 

mysterio:~$ ls -l
total 4
-rw-r-----    1 root     bruce           33 Sep  9  2020 Flag2.txt
mysterio:~$ cat Flag2.txt 
Tu y es presque, il est pas loin
mysterio:~$ ls -la
total 12
drwxr-sr-x    1 bruce    bruce           62 Mar  9 10:05 .
drwxr-xr-x    1 root     root            10 Mar  9 09:58 ..
-rw-r-----    1 root     bruce           20 Mar  9 10:04 .Flag2.txt
-rw-------    1 bruce    bruce           35 Mar  9 10:05 .ash_history
-rw-r-----    1 root     bruce           33 Sep  9  2020 Flag2.txt
mysterio:~$ cat .Flag2.txt 
fl@g{Nice_view_bro}
```

<!-- Ok F : 20220423 -->