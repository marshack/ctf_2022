# Write up Mysterio - Partie 1

## Consigne :

```
"*Mysterio*" est en deux parties, un flag est à récupérer/valider pour chaque partie. 
Démarrer un environnement virtuel et utilisez-le pour ces 2 challenges. 
Attention, la durée de vie de l'environnement est limitée :
<div>
<button class="btn btn-info" onclick="window.open('/portailvms/12','_blank')" >Accéder au portail</button>
</div>
```

## Pièce jointe :

*Néant*

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

`fl@g{easy_peasy_lemon_squeezy}`

## Solution :

ESD

## Partie 1

On exécute un scan de ports avec NMAP sur l'adresse IP qui nous a été attribuée, on obtient :
```bash
nmap 172.102.100.100

Starting Nmap 7.60 ( https://nmap.org ) at 2022-03-09 08:53 CET
Nmap scan report for 172.102.100.100
Host is up (0.00011s latency).
Not shown: 996 closed ports
PORT    STATE SERVICE
21/tcp  open  ftp
80/tcp  open  http
222/tcp open  rsh-spx
443/tcp open  https
```

Le ftp, ne sert à rien et est là pour nous faire perdre du temps :
```bash
ftp 172.102.100.100
Connected to 172.102.100.100.
220 ProFTPD Server (ProFTPD Default Installation) [172.102.100.100]
Name (172.102.100.100:kali): anonymous
331 Anonymous login ok, send your complete email address as your password
Password:
230 Anonymous access granted, restrictions apply
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> dir
200 PORT command successful
150 Opening ASCII mode data connection for file list
-rw-r--r--   1 0        0              57 Sep  8  2020 Bravo.txt
226 Transfer complete
ftp> get Bravo.txt -
remote: Bravo.txt
200 PORT command successful
150 Opening BINARY mode data connection for Bravo.txt (57 bytes)
Bravo t'as rien gagné, retourne à ton scan de ports =)
```

Dans le fichier *Bravo.txt* : `Bravo t'as rien gagné, retourne à ton scan de ports =)`

On affiche la page web sur notre navigateur http://xxx.xxx.xxx.xxx. Dans la partie description de l’utilisateur haxor nous avons un indice :
```
Description = Je dois réussir à trouver un moyen de devenir admin, mais comment ... C'est quelque chose en rapport avec le formulaire d'inscription 
```

Un autre indice dans le profil d'admin :
```
Description = Tu peux m'appeler Bruce, retient bien ce prénom ! Je suis tout puissant et j'ai accès à tout ce que je veux ! 
```

La vulnérabilité proviendrait lors de l’inscription, un rapport avec SQL ?
Dans la partie inscription, si nous inspectons le code source de la page nous voyons un commentaire :
```html
<br/><br/> <!-- To-do : mettre un nb max de chars-->
<input type="password" pattern="{4-100}" name="mdp" placeholder="Mot de passe" required/>
<br/><br/>
```

Une recherche rapide sur google sur SQL et un nombre limité de caractères nous amène à plusieurs articles comme https://www.dailysecurity.fr/les-sql-truncations/.
Il s’agit de l’attaque SQL Truncation.
On insère les identifiants suivants pour une nouvelle inscription :

- Pseudo : `admin        xxxx`
- Mot de passe : `mypassword`
- Mail : `test@test.fr`
- Description : `ceci est un test`

Comme la longueur du pseudo n’est pas vérifiée et qu’il y a une limite dans la base donnée, l’attaque truncation va nous permettre de créer un autre utilisateur avec le pseudo admin.
Nous pouvons nous connecter avec, et avoir le panel admin avec le premier flag et la suite de l’épreuve :

Flag part. 1 : `fl@g{easy_peasy_lemon_squeezy}`

<!-- Ok F 20220423 -->