#define FDA 0x00
#define FDB 0x01
#define HDA 0x80
#define HDB 0x81

// Affiche une chaîne de caratère
void print(char * string) {
    while(*string!=0) {
        __asm__ (
            "int $0x10" : : "a" ((0x0e << 8) | *string)
        );
	string++;
    }
}

// Affiche un caractère imprimable
void print_char(char c) {
    if (c < 32 || c > 127) return; 
    __asm__ (
	    "int $0x10" :: "a" ((0x0e << 8) | c)
    );
    if (c == '\n') print_char('\r');
}

void main(void) {
    unsigned int cylinder, head, sector;
    char erreur;
    unsigned int i;
    // Placement du buffer dans la pile (base 0xC700+512+0x4000)
    char buffer[512];

    for(cylinder=0;cylinder<4;cylinder++) {
        for(head=0;head<16;head++) {
            for(sector=1;sector<64;sector++) {
                // Lecture d'un secteur positionné à cylinder, head, sector et mise en mémoire dans buffer
                // Lecture de l'erreur éventuelle en recupérant la retenu dans al (sbb %al, %al)
                __asm__ (
                    "int $0x13\n"
                    "sbb  %%al, %%al\n"
                    "mov %%al, %0"  : "=r" (erreur):  "a" ((0x02 << 8) | 0x01), "d" ((head << 8) | HDA), "c" ((cylinder << 8) | ((cylinder >> 2) & 0xC0) | sector), "b" (buffer)
                );
                if (erreur) {
                    print("$");
                    goto end;
                }
                for (i=0;i<512;i++) print_char(*(buffer+i));
            }
        }
    }
    end:
    print("\n\rFin.");
    while (1) {
    };
}
