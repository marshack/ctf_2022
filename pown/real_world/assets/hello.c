char hello[] = "NOOB_HELLO";

void print(char * string) {
    while(*string!=0) {
        __asm__ (
            "int $0x10" : : "a" ((0x0e << 8) | *string)
        );
	string++;
    }
}

void main(void) {
    print(hello);
    print("\n\rFin.");
    while (1) {
    };
}

