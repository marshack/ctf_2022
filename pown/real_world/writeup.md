# Bienvenue dans le mode réel (ludo_du_rzo)

## Consignes :

```
Vous avez fait le choix de prendre la pilule rouge et vous voilà maintenant dans le mode réel.
Vous devez envoyer un shell code au format MBR d'une taille de 512 octets pour démarrer un vieux système afin de récupérer des informations sur son disque dur.

- premier niveau: le shellcode doit afficher "NOOB_HELLO" dans la console pour que le flag soit envoyé en retour;
- deuxième niveau: rechercher le flag dans le disque principal au format "*fl@{...}*". La géométrie de ce disque est 63 secteurs, 16 têtes et 4 cylindres.

TIPS:
* pas d'OS (ah ah!);
* mode réel 16 bits à l'ancienne;
* envoi du shellcode via netcat (ex: `cat shellcode | nc game1.marshack.fr 31004`);
* les données affichées à l'écran sont renvoyées à netcat;
* au bout de 30 secondes la machine virtuelle est tuée;
* attendre 30 secondes pour l'affichage du flag du premier niveau;
* les interruptions du BIOS sont disponibles.

Information sur les interruptions, lecture et écriture :

```
## Pièces jointes :  

- ![](assets/doc1.jpg)
- ![](assets/doc2.jpg)

## Serveur :  

```
Docker
```

## Points attribués :

```
Niveau 1: 15 points
Niveau 2: 50 points
```

## Flag :  

```
Niveau 1: fl@g{902888E7-3344-4D76-91FA-0F69BB76CA42}
Niveau 2: fl@g{8BA9BFEC-2513-477D-83F0-F7731C2BBFA8}
```

## Architecture :  

```
x86 mode réel 16 bits
```

## Déroulé :  

- **Le mode réel**:

  le joueur doit créer un MBR d'une taille de 512 octets qui permettra de démarrer le système. La taille du code bootable est volontairement petite afin d'empêcher l'utilisation d'outils ou d'OS minimalistes. Une première version du programme doit afficher un "hello world" et un second doit parcourir le petit disque de 2,4 Mo à la recherche du flag caché dans un fichier texte.

  Dans le mode réel, seul 1 Mo de RAM sont accessibles en mode paginé (pas de mémoire virtuelle, pas de MMU). Le processeur et ses registres fonctionnent en 16bits (ex: AX, BX, CX, BP etc ...). C'est le mode utilisé par MS DOS.

  Malheureusement, avec 512 octets, il est impossible d'utiliser d'OS, seul les fonctions du BIOS sont donc accessibles.

- **Démarrer en mode réel**

  Pour être reconnu par le BIOS, le code de démarrage en mode MBR (non UEFI) doit contenir un flag 0x55 et 0xAA aux adresses 0x1FE et 01xFF (en fin de MBR).

  Une fois reconnu, le bios place le MBR à l'adresse 0x7c00 et exécute le code machine présent à cette adresse.

  Il existe de nombreux exemples sur Internet de codes assembleurs en mode bare-metal ([https://github.com/cirosantilli/x86-bare-metal-examples]()).

- **Premier niveau**: afficher une chaîne déterminée à l'écran

  Il existe de nombreux tutoriels à ce sujet, un simple copier/coller peut faire l'affaire (https://github.com/cirosantilli/x86-bare-metal-examples/blob/master/nasm/bios_hello_world.asm).

  L'affichage de caractère à l'écran s'opère via l'interruption 0x10 du BIOS:

  - fonction AH = 0x0E
  - Caractère à écrire: AL
  - Interruption: 0x10

  Pour afficher une chaîne, il suffit donc de parcourir chaque caractère puis l'afficher jusqu'au caractère nul.

  Exemple:

  ```asm
      org 0x7c00
      bits 16
      xor ax, ax
      mov ds, ax
  start:
      cli
      mov si, msg
      mov ah, 0x0e
  .loop:
      lodsb
      or al, al
      jz halt
      int 0x10
      jmp .loop
  halt:
      hlt
  msg:
      db "NOOB_HELLO", 0
      times 510 - ($-$$) db 0
      dw 0xaa55
  ```

  > **INFORMATION**: *lodsb* charge dans AL le caractère présent à l'adresse contenu dans SI puis incrémente ce dernier.

- **MBR en C**:

     Pour préparer la suite, j'ai décidé de créer un code en C. Cela nécessite de créer un bootstrap en assembleur afin de préparer la pile, de lancer la fonction main et de configurer correctement le linker:

    *entry.S*

    ```asm
    .code16
    .section .text
    .global mystart
    mystart:
        mov $__stack_top, %esp
    	cld
        call main
    ```

    *linker.ld*

    ```ld
    ENTRY(mystart)
    SECTIONS
    {
      . = 0x7c00;
      .text : {
        entry.o(.text)
        *(.text)
        *(.data)
        *(.rodata)
        __bss_start = .;
        /* COMMON vs BSS: https://stackoverflow.com/questions/16835716/bss-vs-common-what-goes-where */
        *(.bss)
        *(COMMON)
        __bss_end = .;
      }
      /* https://stackoverflow.com/questions/53584666/why-does-gnu-ld-include-a-section-that-does-not-appear-in-the-linker-script */
      .sig : AT(ADDR(.text) + 512 - 2)
      {
          SHORT(0xaa55);
      }
      /DISCARD/ : {
        *(.eh_frame)
      }
      __stack_bottom = .;
      . = . + 0x4000;
      __stack_top = .;
    }
    ```

    *Makefile*

    ```makefile
    all: clean  hello
    
    clean:
    	rm -f *.o *.elf
    
    entry:
    	as -ggdb3 --32 -o entry.o entry.S
    		
    hello: entry
    	gcc -c -ggdb3 -m16 -ffreestanding -fno-PIE -nostartfiles -nostdlib -o hello.o -std=c99 hello.c
    	ld -m elf_i386 -o hello.elf -T linker.ld entry.o hello.o
    	objcopy -O binary hello.elf hello.img
    ```

    *hello.c*

    ```c
    char hello[] = "NOOB_HELLO";
    
    void print(char * string) {
        while(*string!=0) {
            __asm__ (
                "int $0x10" : : "a" ((0x0e << 8) | *string)
            );
            string++;
        }
    }
    
    void main(void) {
        print(hello);
        print("\n\rFin.");
        while (1) {
        };
    }
    ```

- **Deuxième niveau: accès au disque**

  Les disques IDE et disquettes sont accessibles via l'interruption 0x13 du BIOS. Les paramètre de lecture sont les suivants [(ref)](https://wiki.osdev.org/Disk_access_using_the_BIOS_(INT_13h)):

  | Registre | Fonction                                                    | Valeur      |
  | -------- | ----------------------------------------------------------- | ----------- |
  | AH       | Fonction de lecture                                         | 0x02        |
  | AL       | Nombre de secteur à lire                                    | 1< AL < 128 |
  | BX       | Adresse du buffer                                           | &buffer     |
  | CH       | Numéro de cylindre (8 bits de poids faibles)                | 0 à 3       |
  | CL       | Numéro de secteur + 2 bits de poids fort numéro de cylindre | 1 à 63      |
  | DH       | Numéro de tête de lecture                                   | 0 à 15      |
  | DL       | Numéro de disque (ici, premier disque IDE)                  | 0x80        |

  Les valeurs concernant la géométrie du disque (cylindre, tête et secteur) sont données dans l'énoncé.

  La solution mise en œuvre dans le code C ci-dessous revient donc à parcourir dans des boucles imbriquées l'ensemble des secteurs, têtes et cylindres du disque et à afficher les caractères imprimables présents dans le buffer. Pour simplifier le code, le tri des chaînes peut être effectué sur le poste du joueur à l'aide d'un *grep* sur le mot clé *_FLAG*.

  

  *flag.c*:

  ```c
  #define FDA 0x00
  #define FDB 0x01
  #define HDA 0x80
  #define HDB 0x81
  
  // Affiche une chaîne de caratère
  void print(char * string) {
      while(*string!=0) {
          __asm__ (
              "int $0x10" : : "a" ((0x0e << 8) | *string)
          );
  	string++;
      }
  }
  
  // Affiche un caractère imprimable
  void print_char(char c) {
      if (c < 32 || c > 127) return; 
      __asm__ (
  	    "int $0x10" :: "a" ((0x0e << 8) | c)
      );
      if (c == '\n') print_char('\r');
  }
  
  void main(void) {
      unsigned int cylinder, head, sector;
      char erreur;
      unsigned int i;
      // Placement du buffer dans la pile (base 0xC700+512+0x4000)
      char buffer[512];
  
      for(cylinder=0;cylinder<4;cylinder++) {
          for(head=0;head<16;head++) {
              for(sector=1;sector<64;sector++) {
                  // Lecture d'un secteur positionné à cylinder, head, sector et mise en mémoire dans buffer
                  // Lecture de l'erreur éventuelle en recupérant la retenu dans al (sbb %al, %al)
                  __asm__ (
                      "int $0x13\n"
                      "sbb  %%al, %%al\n"
                      "mov %%al, %0"  : "=r" (erreur):  "a" ((0x02 << 8) | 0x01), "d" ((head << 8) | HDA), "c" ((cylinder << 8) | ((cylinder >> 2) & 0xC0) | sector), "b" (buffer)
                  );
                  if (erreur) {
                      print("$");
                      goto end;
                  }
                  for (i=0;i<512;i++) print_char(*(buffer+i));
              }
          }
      }
      end:
      print("\n\rFin.");
      while (1) {
      };
  }
  ```
  
  > **INFORMATION**: Le code assembleur est intégré au code C au format AT&T (extended ASM). Il contient un modèle (template) d'assembleur pouvant inclure des paramètres (%0, %1, %2 etc...). Ces paramètres sont remplacés par des valeurs entrées et/ou de sorties (template ASM :sorties: entrées) définies en C. Les registres peuvent aussi être implicitement manipulés en entrée et en sortie (::"a" XXXX, "b" XXXX ...).
  >
  > https://dmalcolm.fedorapeople.org/gcc/2015-08-31/rst-experiment/how-to-use-inline-assembly-language-in-c-code.html
  >
  > https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html#Extended-Asm
  
  Nouveau *Makefile*:
  
  ```makefile
  all: clean hello flag
  
  clean:
          rm -f *.o *.elf hello.img flag.img
  
  entry:
          as -ggdb3 --32 -o entry.o entry.S
  
  hello: entry
          gcc -c -ggdb3 -m16 -ffreestanding -fno-PIE -nostartfiles -nostdlib -o hello.o -std=c99 hello.c
          ld -m elf_i386 -o hello.elf -T linker.ld entry.o hello.o
          objcopy -O binary hello.elf hello.img
  
  flag: entry
          gcc -c -ggdb3 -m16 -ffreestanding -fno-PIE -nostartfiles -nostdlib -o flag.o -std=c99 flag.c
          ld -m elf_i386 -o flag.elf -T linker.ld entry.o flag.o
          objcopy -O binary flag.elf flag.img
  ```
  
  

Test 1er flag

```
cat hello.img | nc <ip> <port>

hello.img : mbr de 512 octets
```

Test 2ieme flag

```
cat flag.img | nc <ip> <port>

flag.img: mbr de 512 octets
```

<!-- Ok F : 20220423 -->

