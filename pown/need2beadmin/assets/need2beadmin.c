// gcc -o needtobeadmin needtobeadmin.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
// TAG:cez40
#include <linux/limits.h>

#define LEN_BUFF_AUTH 64
#define MAX_ROLES 128
#define MAX_USERS 128
#define MIN_ROLES 4
#define MIN_USERS 9
#define memberSize(type, member) sizeof(((type *)0)->member)



struct Role {
  unsigned int id;
  char name[8];
  char alias[8];
};

struct User {
  char name[16];
  unsigned int role_id;
};

struct Role *roles[MAX_ROLES];
struct User *users[MAX_USERS];
struct User *currentUser = NULL;
unsigned int numberOfRoles = 0;
unsigned int numberOfUsers = 0;

void deleteNewLine(char *str) {
  for (;*str != '\n'; str++);
  *str = '\x00';
}

int convertNumber(char *str) {
  if (isdigit(str[0]) == 0) {
    printf("Erreur, un nombre est necessaire.\n");
    // fflush(stdout);
    exit(-5);
  }
  return atoi(str);
}

char *getNameRole(char *dst, unsigned int roleId, unsigned int max) {
  for (size_t i = 0; i < MAX_ROLES; i++) {
    if (roles[i]->id == roleId) {
      strncpy(dst, roles[i]->name, max);
      break;
    }
  }
  return dst;
}

unsigned short userExist(char *userName) {
  for (size_t i = 0; i < numberOfUsers; i++) {
    if (strcmp(users[i]->name, userName) == 0) {
      return 1;
    }
  }
  return 0;
}

unsigned short isAdmin(struct User *user) {
  char nameRoleCurrent[memberSize(struct Role, name)];
  getNameRole(nameRoleCurrent, user->role_id, sizeof(nameRoleCurrent));
  if (strcmp(nameRoleCurrent, "Admin") == 0) {
    return 1;
  }
  return 0;
}

unsigned short roleExist(unsigned int roleId) {
  for (size_t i = 0; i < numberOfRoles; i++) {
    if (roles[i]->id == roleId) {
      return 1;
    }
  }
  return 0;
}

struct User *getUser(char *name) {
  for (size_t i = 0; i < numberOfUsers; i++) {
    if (strcmp(users[i]->name, name) == 0) {
      return users[i];
    }
  }
  printf("Erreur dans la récupération de l'utilisateur.\n");
  // fflush(stdout);
  exit(-11);
}

void getUsers() {
  char userRole[memberSize(struct Role, name)];
  write(1,"\n",1);
  for (size_t i = 0; i < numberOfUsers; i++) {
    getNameRole(userRole, users[i]->role_id, sizeof(userRole));
    printf("Utilisateur: %s\nRôle:\t     %s\n\n", users[i]->name, userRole);
    //fflush(stdout);
  }
  write(1,"\n",1);
}

void getRoles() {
  write(1,"\n",1);
  for (size_t i = 0; i < numberOfRoles; i++) {
    printf("Rôle:\t %i\nNom:\t %s\nAlias:\t %s\n\n", roles[i]->id, roles[i]->name, roles[i]->alias);
    //fflush(stdout);
  }
  write(1,"\n",1);
}

struct User *newUser(char *name, unsigned int roleId, unsigned short first) {

  char nameRoleNext[memberSize(struct Role, name)];
  struct User *user = malloc(sizeof(struct User));

  if (numberOfUsers >= MAX_USERS) {
    printf("Nombre maximal d'uitlisateurs atteint (max:%d)", MAX_USERS);
    //fflush(stdout);
    exit(-3);
  }

  if (first) {
    user->role_id = roleId;
    memcpy(user->name, name, memberSize(struct User, name));
    currentUser = user;
    numberOfUsers++;

    return user;
  }

  memcpy(user->name, name, sizeof(user->name));

  if (userExist(name)) {
    printf("L'utilisateur existe déjà.\n");
    //fflush(stdout);
    exit(-7);
  }

  if (!roleExist(roleId)) {
    printf("Le rôle selectionné n'existe pas.\n");
    //fflush(stdout);
    exit(-8);
  }

  getNameRole(nameRoleNext, roleId, sizeof(nameRoleNext));

  if (isAdmin(currentUser)) {
    user->role_id = roleId;
  } else if (strcmp(nameRoleNext, "Admin") != 0) {
    user->role_id = roleId;
  } else {
    printf("Tu dois être admin pour définir ce rôle.\n");
    //fflush(stdout);
    exit(-9);
  }

  currentUser = user;
  numberOfUsers++;

  return user;

}

struct Role *newRole(char *name, char *alias, unsigned short privilege, unsigned short first) {
  struct Role *role = malloc(sizeof(struct Role));

  if (numberOfRoles >= MAX_ROLES) {
    printf("Nombre maximal d'uitlisateurs atteint (max:%d)", MAX_ROLES);
    //fflush(stdout);
    exit(-4);
  }

  if (first) {
    role->id = 1;
  } else {
    role->id = roles[numberOfRoles-1]->id+1;
  }

  if (privilege) {
    goto priv;
  }

  if (!isAdmin(currentUser) & strcmp(name, "admin") == 0) {
    printf("\nTu as besoin d'être admin pour créer ce rôle.\n");
    //fflush(stdout);
    exit(-10);
  }

  printf("\nRôle '%s' créé.\n\n", name);
  //fflush(stdout);

  priv:
  memcpy(role->name, name, sizeof(role->name));
  memcpy(role->alias, alias, sizeof(role->alias));

  numberOfRoles++;
  return role;
}

void delUser() {
  char name[memberSize(struct User, name)];

  memcpy(name, currentUser->name, sizeof(name));

  if (numberOfUsers <= MIN_USERS) {
    printf("Besoin d'un minimum de %i utilisateurs.\n", MIN_USERS);
    //fflush(stdout);
    exit(-5);
  }

  free(currentUser);
  users[numberOfUsers-1] = NULL;
  printf("Utilisateur '%s' supprimé\n\n", name);
  //fflush(stdout);

  numberOfUsers--;
}

void delRole() {
  char name[memberSize(struct Role, name)];

  memcpy(name, roles[numberOfRoles-1]->name, sizeof(name));

  if (numberOfRoles <= MIN_ROLES) {
    printf("Besoin d'un minimum de %i rôles.\n", MIN_ROLES);
    //fflush(stdout);
    exit(-6);
  }

  free(roles[numberOfRoles-1]);
  roles[numberOfRoles-1] = NULL;
  printf("\nRôle '%s' supprimé\n\n", name);
  //fflush(stdout);

  numberOfRoles--;
}

void switchAdmin() {
  
  //TAG:cez40	
  char cwd[PATH_MAX]="\x00";
  const char *path = getenv("MY_APP_HOME"); 
  if (path == NULL)
     {
     strcat(cwd,"./SecretMessage");
     }
  else
     {
     strncpy(cwd,path,PATH_MAX);
     strcat(cwd,"/SecretMessage");
     }
	

  char secretMessage[64];
  FILE *secretMessageFile = fopen(cwd,"r");

  if (!isAdmin(currentUser) | strcmp(currentUser->name, "Reine rouge") != 0) {
    printf("\nSeule la 'Reine rouge' peut accéder à cette interface.\n\n");
    
    //fflush(stdout);
    return;
  }

  if(secretMessageFile == NULL) {
    puts("Erreur lors de l'ouverture du fichier.\n");
    exit(-12);
  }

  fgets(secretMessage, sizeof(secretMessage), secretMessageFile);
  fclose(secretMessageFile);
  deleteNewLine(secretMessage);

  printf(".===== Console de la Reine rouge =====.\n");
  //fflush(stdout);
  printf("|Loading...\n|\n");
  //fflush(stdout);
  printf("|.------..------..------..------..------.\n||A.--. ||D.--. ||M.--. ||I.--. ||N.--. |\n|| (\\/) || :/\\: || (\\/) || (\\/) || :(): |\n|| :\\/: || (__) || :\\/: || :\\/: || ()() |\n|| '--'A|| '--'D|| '--'M|| '--'I|| '--'N|\n|`------'`------'`------'`------'`------'\n"
  "|\n|Message secret:\n|%s\n\\______________________________________\n\n", secretMessage);
  //fflush(stdout);
}

void getUserInfo() {
  char userRole[memberSize(struct Role, name)];

  write(1,"\n",1);
  getNameRole(userRole, currentUser->role_id, sizeof(userRole));
  printf("Connecté avec: %s\nRôle:\t       %s\n", currentUser->name, userRole);
  write(1,"\n",1);
  //fflush(stdout);
}

void switchUser(char *userName) {
  if (!isAdmin(currentUser)) {
    printf("Seul les utilisateurs ayant le rôle 'Admin' peuvent accéder à cette fonctionnalité.\n");
    //fflush(stdout);
    return;
  }

  if (!userExist(userName)) {
    printf("L'utilisateur n'existe pas.\n");
    //fflush(stdout);
    return;
  }

  currentUser=getUser(userName);
}

char getCommande() {
  char choix;
  char newLine;
  char role[memberSize(struct Role, name)];

  getNameRole(role, currentUser->role_id, sizeof(role));

  printf("\
|   [1] Liste les utilisateurs\n\
|   [2] Liste les rôles\n\
|   [3] Créer un utilisateur\n\
|   [4] Créer un rôle\n\
|   [5] Supprimer un utilisateur\n\
|   [6] Supprimer un rôle\n\
|   [7] Passer en mode Rouge\n\
|   [8] Info utilisateur\n\
|   [9] Changer d'utilisateur\n\
|   [0] Quitter\n\
\\____________________________________\n\
--- %s@%s: \
", currentUser->name, role);
  //fflush(stdout);
  choix = fgetc(stdin);
  newLine = fgetc(stdin);
  if (newLine != '\n') {
    exit(-1);
  }
  return choix;
}

void gestionUser() {
  printf("Connexion...\n"
          ".=====================================.\n"
          "|                                     |\n"
          "|        Umbrella Corporation         |\n"
          "|                                     |\n"
          "`====================================='\n\n");
  //fflush(stdout);
  while (1) {
    char buff1[128];
    char buff2[128];

    printf(".======== Liste des commandes ========.\n");
    //fflush(stdout);
    switch (getCommande()) {
      case '1':
        getUsers();
        break;
      case '2':
        getRoles();
        break;
      case '3': // struct User *newUser(char *name, unsigned int roleId, unsigned short privilege)
        printf("Nom du nouvel utilisateur: ");
        //fflush(stdout);
        fgets(buff1, sizeof(buff1), stdin);

        getRoles();
        printf("Identifiant du role: ");
        //fflush(stdout);
        fgets(buff2, sizeof(buff2), stdin);

        deleteNewLine(buff1);
        deleteNewLine(buff2);

        users[numberOfUsers] = newUser(buff1, convertNumber(buff2), 0);
        break;
      case '4': // struct Role *newRole(char *name, char *alias, unsigned short privilege, unsigned short first)
        printf("Nom du nouveau rôle: ");
        //fflush(stdout);
        fgets(buff1, sizeof(buff1), stdin);

        printf("Alias du nouveau rôle: ");
        //fflush(stdout);
        fgets(buff2, sizeof(buff2), stdin);

        deleteNewLine(buff1);
        deleteNewLine(buff2);

        roles[numberOfRoles] = newRole(buff1, buff2, 0, 0);
        break;
      case '5':
        delUser();
        break;
      case '6':
        delRole();
        break;
      case '7':
        switchAdmin();
        break;
      case '8':
        getUserInfo();
        break;
      case '9':
        printf("Se connecter avec: ");
        //fflush(stdout);
        fgets(buff1, sizeof(buff1), stdin);
        deleteNewLine(buff1);
        switchUser(buff1);
        break;
      case '0':
        printf("Déconnexion...\n");
        //fflush(stdout);
        exit(-1);
        break;
      default: {
        printf("\n<! [-] Command Not Found !>\n\n");
        //fflush(stdout);
      }
    }
  }
}

int main(int argc, char **argv, char **envp) {
  
  // TAG:cez40	
  setvbuf(stdout, NULL, _IONBF, 0);   
  setvbuf(stderr, NULL, _IONBF, 0);   

  roles[0] = newRole("Admin", "admin", 1, 1); // struct Role *newRole(char *name, char *alias, unsigned short privilege, unsigned short first)
  roles[1] = newRole("Cyber-S", "CS", 1, 0);
  roles[2] = newRole("Comando", "comando", 1, 0);
  roles[3] = newRole("default", "default", 1, 0);
  users[0] = newUser("Reine rouge", 1, 1); //struct User *newUser(char *name, unsigned int roleId, unsigned short privilege)
  users[1] = newUser("Alice Abernathy", 2, 0);
  users[2] = newUser("James One Shade", 3, 0);
  users[3] = newUser("Olga Danilova", 3, 0);
  users[4] = newUser("Vance Drew", 3, 0);
  users[5] = newUser("Alfonso Warner", 3, 0);
  users[6] = newUser("Rain Ocampo", 3, 0);
  users[7] = newUser("J.D. Salinas", 3, 0);
  users[8] = newUser("Anonymous", 4, 0);
  currentUser = users[8];

  gestionUser();
}

