#include <stdio.h>
//#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define LEN_BUFF_AUTH 64

//gcc -w -no-pie -m32 -fstack-protector-all cquoideja.c -o cquoideja

char validPassword[LEN_BUFF_AUTH] = "TODO...\x00";

void deleteNewLine(char *str) {
  for (;*str != '\n'; str++);
  *str = '\x00';
}

char *upper(char *str) {
  unsigned int lenStr = strlen(str);
  for (size_t i = 0; i < lenStr; i++) {
    switch (str[i]) {
      case 'y':
        str[i] = 'Y';
        break;
      case 'n':
        str[i] = 'N';
    }
  }
  return str;
}

int authentification() {
  char password[LEN_BUFF_AUTH];
  char response[LEN_BUFF_AUTH];
  printf("Tu veux être admin [Y/N]: ");
  fflush(stdout);
  fgets(response, LEN_BUFF_AUTH, stdin);
  upper(response);
  deleteNewLine(response);

  if (response[0] == 'Y') {
    printf("Tu pensais vraiment être admin.. :D\n");
    fflush(stdout);
    exit(-2);
  } else {
    printf("Tu as choisi '");
    printf(response);
    printf("', c'était le bon choix.\n");
    fflush(stdout);
  }

  printf("Entre à présent le mot de passe: ");
  fflush(stdout);
  fgets(password, LEN_BUFF_AUTH, stdin);
  deleteNewLine(password);

  if (strcmp(validPassword, password) == 0) {
    return 1;
  }
  return 0;
}

int main(int argc, char **argv, char **envp) {
  if (authentification() != 1) {
    printf("NOP !\n");
    fflush(stdout);
    exit(-1);
  }
  printf("Connecté ! Valide le challenge avec ce mot de passe :)\n");
  fflush(stdout);
}
