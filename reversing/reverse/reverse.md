# Write up Reversing/Reverse

## Consigne : 

```
Trouver le mot de passe de l’application (le mot de passe est le flag et il n’est composé que de lettres minuscules et majuscules).
fl@g{...}
```

## Pièce jointe :

```
crackme
```

## Serveur :

```
CTFD
```

## Points attribués :

```
20
```

## Hint : 

```

```

## Flag :

```
fl@g{ImAnIcELiTTleFlAg}
```

## Créateur :

```
ESD
```

## Soluce 

Exécution sans argument

```
./crack_me

This program is very secure !
usage =./crack_me PASSWORD

```


Exécution avec un mauvais mot de passe


```
./crack_me abcdefgh

This program is very secure !
Nope You are wrong !!!!!
```

Exécution avec le bon mot de passe

```
./crack_me fl@g{ImAnIcELiTTleFlAg}

This program is very secure !
GG WP ! The flag is the password

```

Résolution

On ouvre l’application avec un debugger, j’utilise IDA. On peut voir qu’il y a une fonction nommée « *check_pass* ».

![liste_fonctions](images/liste_fonctions.png)

On va se concentrer sur cette fonction

Décompilation de la fonction check_pass sur ida (F5)

```c
_int64 __fastcall check_pass(__int64 a1)                                   <== pointeur a1 sur le password passé en argument
{
  int v2; // [rsp+1Ch] [rbp-74h]
  __int64 v3[4]; // [rsp+20h] [rbp-70h] BYREF
  __int64 v4[4]; // [rsp+40h] [rbp-50h] BYREF
  __int64 v5[6]; // [rsp+60h] [rbp-30h] BYREF

  v5[5] = __readfsqword(0x28u);
  memset(v5, 0, 32);                                                       <== initialisation du buffer V5
  qmemcpy(v3, "//@1.F:F=N?TWILQLPcS`UQ", 23);                              <== initialisation de la chaine v3 
  qmemcpy(v4, "87906001415926535897006", 23);                              <== initialisation de la chvine v4 
  v2 = 0;                                                                  <== indice initialisé à 0
  do                                                                       <== boucle traitement de chaque caractère   
  {
    *((_BYTE *)v5 + v2) = *(_BYTE *)(v2 + a1) ^ *((_BYTE *)v4 + v2);       <== v5[v2]=a1[v2] ^ v4[v2]   xor
    *((char *)v5 + v2) >>= 1;                                              <== v5[v2]=v5[v2] >> 1       décalage de 1 bit à droite
    *((_BYTE *)v5 + v2) += 2 * v2;                                         <== v5[v2]=v5[v2] + 2 * [v2]
    if ( *((_BYTE *)v5 + v2) != *((_BYTE *)v3 + v2) )                      <== if v5[v2] != v3[v2] 
      return 0LL;                                                             <== retourne 0      
    ++v2;                                                                  <== incrémente l'indice (prochain caractère) 
  }                                                                        
  while ( *(_BYTE *)(v2 + a1) );                                           <== boucle
  return 1LL;
}
```
  
Chaque caractère du password est xoré avec chaque caractère de la chaine  "87906001415926535897006"

Puis on décale de 1 bit (division par 2) chaque résultat

Pour finir on ajoute la valeur de l'indice * 2 (0,2,4,6,8 ...) à chaque résultat.

Chaque résultat obtenu est ensuite comparé à chaque caractère de la chaine "//@1.F:F=N?TWILQLPcS`UQ"
Si les valeurs comparées ne sont pas égales, alors le mot de passe est incorrect.

Prenons l'exemple du premier et deuxième caractères:    

```
           f                 l
         0x66 ("f")        0x6c ("l") 
xor      0x38 ("8")        0x37 ("7")
         ----------        ----------
  =      0x5e              0x5b  

```

```
         0x5e >> 1         0x5b >> 1  
  =      0x2f              0x2d 
```

​        

```
         0x2f + (2 * 0)    0x2d + (2 * 1) 
  =      0x2f ("/")        0x2f ("/") 
         
```

Décoder le mot de passe

Le décodage sera assez simple, on va développer un petit programme en **c** qui va faire chaque étape en sens inverse.

Remarque sur l' opération de décalage

on voit que 0x5e (pair) et 0x5f (impair) donne le même résultat lors du décalage.  
cela signifie que l'on aura à la fin des opération en sens inverse deux solutions pour chaque caractère  

```
 0x5e  >> 1 = 0x2f ;     0x2f  << 1 = 0x5e
 0x5f  >> 1 = 0x2f ;     0x2f  << 1 = 0x5e  <==  


```



```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

# define CORRECT_LEN 23


void decoder(){
        printf("\n------------------  Super decoder ---------------\n");
        char *encoded="//@1.F:F=N?TWILQLPcS`UQ";
        char *mask="87906001415926535897006";
        char password1[CORRECT_LEN+1]={0};
        char password2[CORRECT_LEN+1]={0};

        int i=0;
        do {
           password1[i] = encoded[i]-i*2;
           password1[i] = password1[i] << 1;
           password1[i] = password1[i] ^ mask[i];
           password2[i] = encoded[i]-i*2;
           password2[i] = password2[i] << 1;
           password2[i] = password2[i] +1;
           password2[i] = password2[i] ^ mask[i];
           i++;
           }
        while (encoded[i] !=0);
        printf("%s",password1);
        printf("\n");
        printf("%s",password2);
        printf("\n");
}

int main(int argc,char**arg) {
        decoder();
        return 0;
}

```



Résultat: 

```
fmAfzHlAnIcELhUUmdGm@f|
gl@g{Im@oHbDMiTTleFlAg}

```

Par tatonnement et en respectant la consigne (constitué de minuscules/majuscules) on arrive à reconstituer le flag

```
fmAfzHlAnIcELhUUmdGm@f|
^      ^^^^^^ 

gl@g{Im@oHbDMiTTleFlAg}
 ^^^^^^      ^^^^^^^^^^  

```

on obtient:

```
fl@g{ImAnIcELiTTleFlAg}
```

<!-- Ok F : 20220423 -->
