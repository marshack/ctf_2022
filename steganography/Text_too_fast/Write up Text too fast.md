# Write up Forensic/Text too fast

## Consigne: 

```
Vous êtes un espion russe infiltré à la Rochelle dans l’objectif de récupérer les codes de lancements nucléaires d’Umbrella Corporation. 
Vous éliminez les gardes et obtenez un fichier top secret nommé « 3310.jpg »
Etonnamment vous le trouvez suspect.
Le flag devra être inséré en minuscule dans fl@g{}. 
```

## Pièce jointe :

```
3310.jpg
```

## Serveur :

```
CTFD
```

## Points attribués :

```
10
```

## Hint : 

```
sms
```

## Flag :

```
flag{smsisnotdead}
```

## Solution : 

Présentation de la méthodologie de résolution du challenge :

### Étape 1 : 

Il faut utiliser steghide sur le fichier « 3310.jpg » avec la commande ci-dessous pour extraire le fichier mp3 de l’image :

```bash
$steghide --extract -sf 3310.jpg -xf hack
```

recherche d'informations sur le fichier : 

```bash
$file hack
hack: Audio file with ID3 version 2.4.0, extended header, contains:MPEG ADTS, layer III, v1, 128 kbps, 44.1 kHz, JntStereo
```

### Étape 2 : 

Il faut ouvrir le fichier MP3 avec audacity.

![](../Text_too_fast/image_writeup/audacity.png)


Ensuite il faut se rendre dans le menu ‘effet’ puis choisir ‘changer la vitesse’.
Dans ce menu il faut diminuer la vitesse de lecture du fichier à -50 % et valider.

![](../Text_too_fast/image_writeup/audacity_reduct_speed.png)

![](../Text_too_fast/image_writeup/audacity_speed.png)

Ensuite il faut écouter et noter ce qui est dit dans le fichier audio.
**Les codes correspondent à du multitap-abc.**
Récupérer une image de clavier d’ancien téléphone et déchiffrer le message, ou aller sur https://www.dcode.fr/code-multitap-abc et décoder le message :

```bash
7777 6 7777 444 7777 66 666 8 3 33 2 3
```

```
SMSISNOTDEAD
```

<!-- Ok F : 20220423 -->