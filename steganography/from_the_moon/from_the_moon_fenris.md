# Write up Stegano/from_the_moon

## Consigne : 

```
Retrouver le flag dans cette image
```

## Pièce jointe :

```
moon.png
```

## Serveur :

```
CTFD
```

## Points attribués :

```
40
```

## Hint : 

```
SSTV
```

## Flag :

```
fl@g{Sstv_Copthorne_MacDonald}
```

## Soluce

```bash
$ file moon.png 
moon.png: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 3840x2160, components 3
```

Contrôle avec binwalk (file signatures)

```bash
$ binwalk moon.png 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
286110        0x45D9E         Zip archive data, at least v2.0 to extract, compressed size: 2225570, uncompressed size: 3051535, name: moon.txt
2511824       0x2653D0        End of Zip archive, footer length: 22
```

On peut voir qu'il contient un fichier zip 

```bash
$ binwalk -e moon.png 
```

Binwalk a déjà décompressé l'archive zip. On obtient un fichier `moon.txt`

On va regarder ce qui se trouve dans le fichier `moon.txt`

```bash
$ cat moon.txt
```

résultat : 

```
//uURAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWGluZwAAAA8AABayACJ38AABAwQH
CgwPERMWGBsdHyIlKCstMDM1ODs+QUNGSUtOUVNXWlxfYWRnaWxvcXR3en1/goWHio2PkpSXmp2g
oqWoqq2wsra5u77Aw8bIy87Q09bY297g4+Xo6uzv8fT2+Pr7/P4AAABQTEFNRTMuMTAwBLkAAAAA
AAAAADUgJARljQAB4AAid/C1P9FOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//u0RAAO8AAAf4AAAAgA
AA/wAAABA9QDImAAAACatSJAAI/hFgH///////ywSvNBIsCwuldMz3LLv////2egUOXLX//X3//3
MqMvPFWWpkquX///L+7ksygGh3N1DwbHcxI9HAkcGOEo1QAALQP///////peWLiW9bw6dSbawgMj
```

Cela ressemble à du base64.

On va décoder le fichier.

```bash
$ cat moon.txt |base64 -d > moon.b64
```

On va contrôler le type de fichier.

```
$ file moon.b64
moon.b64: MPEG ADTS, layer III, v1, 128 kbps, 48 kHz, JntStereo
```

On peut voir que le fichier est un mp3
On va renommer le fichier.

```
$ mv moon.b64 moon.mp3
```

On va l'analyser avec audacity.

![audacity](../from_the_moon/image_writeup/audacity.png)


Après quelques recherches sur internet "first transmission moon" on comprend que cela est de la SSTV.

On va installer un logiciel permettant de transformer les fichiers son en image.

```bash
$ sudo apt install qsstv
```

![sstv_error](../from_the_moon/image_writeup/sstv_error.png)


On peut voir que le header n'est pas bon. 

En cherchant dans la doc technique, le logiciel ne prend en change que les fichiers .wav

On va transformer le fichier avec audacity car avec ffmpeg on perd en qualité.

![audacity_convert](../from_the_moon/image_writeup/audacity_convert.png)

- ouvrir le fichier moon.mp3
- aller dans fichier / Exporter / Exporter en wav

Il est très important de garder la qualité avec un taux du projet à 48000 HZ et en wav (microsoft 16bit PCM signé)

Ouvrir qsstv et aller dans options / configuration / sound

![conf_qsstv](../from_the_moon/image_writeup/conf_qsstv.png)

- Dans Sound Input selectionner "From file"
- Conserver le Input clock-frequency et l'output à 48000 HZ
- Cliquer sur "OK"

Ensuite aller dans receive et ouvrir votre fichier moon.mp3

 ![play_sstv](../from_the_moon/image_writeup/play_sstv.png)



![qrcode_sstv](../from_the_moon/image_writeup/qrcode_sstv.png)



Sauvegarder ensuite l'image 

lire le Qrcode à l'aide d'un téléphone ou à partir de "zbarimg"

```
$ apt install zbar-tools
$ zbarimg save_qsstv.png 
QR-Code:fl@g{Sstv_Copthorne_MacDonald}
scanned 1 barcode symbols from 1 images in 0.06 seconds

```

Le flag est : fl@g{Sstv_Copthorne_MacDonald}

<!-- Ok F : 20220423 -->
