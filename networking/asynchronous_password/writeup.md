# Asynchronous password (ludo_du_rzo)


## Consignes :  

```
Nous avons branché un boitier espion sur le port console d'un équipement actif qui a capturé la trame ci-jointe.

Décodez manuellement les trames pour trouver le mot de passe "enable" du commutateur au format ASCII.
```
## Pièces jointes :  
- **Capture:**

![](assets/capture.png)

## Serveur :  

```
néant
```

## Points attribués :
```
15 ou 20
```

## Flag :  

```
Gros_FLAG!
```

## Architecture :  

```
néant
```

## Déroulé :  

Le port console d'un commutateur est généralement une liaison série asynchrone (UART ou RS232). Il faut donc décoder à l'ancienne à la règle et au stylo chaque trame.

- **Vitesse de transfert:**

Comme indiqué sur la capture, le plus petit écart entre deux fronts est de 102µs. Cela correspond à 9800 bauds (1/T), soit en arrondissant, 9600 bauds et une période de 104µs.

Chaque bit a donc une longueur d'environ 104µs.

- **Décodage de la première trame**:

  ![atmega328pin](assets/correction.png)

  Une fois obtenu la largeur des bits, il faut en déduire la configuration de la liaison série (nombre de bits de données, bit de parité, nombre de bit de stop).

  Généralement, les équipements actifs sont configurés avec 7 ou 8 bits de données. Partant de ce principe, on a donc:

  - bit 0: **bit de start**;
  - bits 1 à 8: **8 bits de données**. 7 bits est impossible car les deux bits suivants ne peuvent pas être des bits de stop;
  - bit 9: **bit de parité**. Ce bit permet de contrôler les données. en comprenant ce bit, le compte des bits à 1 doit être pair. Ici c'est le cas, le bit de parité est à 0;
  - bits 10 et 11: longue période à 1 qui doit correspondre à **2 bits de stop**.
  
  On a donc au niveau des données 1110 0010 à lire à l'envers, les bits de poids fort étant envoyés en premier, soit: **0100 0111, 0x47 et "G**".
  
- **Décodage du message**: 

  ![atmega328pin](assets/décodage.png)

  On connaissant la taille minimale d'une trame et en démarrant le décodage d'un caractère après chaque bit de start, on a :

  | Bits de données (dans l'ordre) | Valeur | Caractère ASCII |
  | ------------------------------ | ------ | --------------- |
  | 0100 0111                      | 0x47   | G               |
  | 0111 0010                      | 0x72   | r               |
  | 0110 1111                      | 0x6f   | o               |
  | 0111 0011                      | 0x73   | s               |
  | 0101 1111                      | 0x5F   | _               |
  | 0100 0110                      | 0x46   | F               |
  | 0100 1100                      | 0x4c   | L               |
  | 0100 0001                      | 0x41   | A               |
  | 0100 0111                      | 0x47   | G               |
  | 0010 0001                      | 0x21   | !               |

<!-- Ok F : 20220423 -->