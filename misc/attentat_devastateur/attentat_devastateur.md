# Write up attentat_devastateur

## Consigne : 

```
Vous êtes envoyé le 11 Septembre 2001 pour empêcher un attentat qui a bouleversé l’histoire des Etats-Unis. Votre mission, si vous l’acceptez, est de trouver le code permettant aux autorités locales d’arrêter les malfaiteurs avant le drame. Pour cela, nous vous fournirons un exécutable que nous avons récupéré chez les attaquants ainsi qu’un rapport d’enquête mais celui-ci est protégé par un mot de passe. 

Bon courage, ce message s’autodétruira dans 12 heures        
```

## Pièce jointe :

```
crackme2
archive .rar : RapportEnquete2001.rar
```

## Serveur :

```
CTFD
```

## Points attribués :

```
20
```

## Hint : 

```

```

## Flag :

```
fl@g{StopTheAttacK2001}
```

## Créateur :

```
ESD
```



## Résolution du challenge : 

1- Résoudre le crackme 

Le code source est le suivant 

```
// gcc -m32 crackme2.c -o crackme2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc,char**arg) {
        int i;
        char pass[]="EleVenAttack";
        char flag[]={
            0x0E, 0x09, 0x0D, 0x0B, 0x14, 0x1A, 0x0C, 0x1F, 0x17, 0x55, 0x1F, 0x04
            };


        char output[13];

        if (argc != 2) {
        printf("un seul argument est demandé.\n");
        return -1;
        }

        if (strncmp(arg[1], pass, strlen(pass))) {
           printf("Nop, %s ne permet pas d'arréter l'attaque.\n",arg[1]);
           return -1;
           }

         else {
         printf("Yaaaaaaaa, %s a arrété l'attaque!\n", arg[1]);
         printf("Voici le mdp pour extraire le contenu de l'archive\n");
         for (i=0; i<13;i++){
            char temp = flag[i] ^  arg[1][i];
            output[i] = temp;
            }

         output[i]='\0';
         printf("%s",output);
         printf("\n");
         return 0;
         } // fin else
} //fin main




```



Nous avons en main un crackme, un code développé en C qui demande en argument de saisir un mot de passe. 

```
./crackme2
un seul argument est demandé.

```

```
./crackme2 test
Nop, test ne permet pas d'arréter l'attaque.
```

Afin de découvrir le mot de passe utilisé et le flag, on utilise un débugger comme GDB. 

On désassemble le **crackme2**. 

Pour cela nous allons utiliser la commande **disas**, en précisant la fonction que nous souhaitons désassembler : la fonction **main**. 



```
gdb ./crackme2
break main
run AAAAAAAA
```

```asm
Disas main
Dump of assembler code for function main:
  
   (extrait...)
  
   0x56556300 <+147>:	mov    edx,DWORD PTR [ebp-0x4c]
   0x56556303 <+150>:	add    edx,0x4
   0x56556306 <+153>:	mov    edx,DWORD PTR [edx]
   0x56556308 <+155>:	sub    esp,0x4
   0x5655630b <+158>:	push   eax
   0x5655630c <+159>:	lea    eax,[ebp-0x26]
   0x5655630f <+162>:	push   eax
   0x56556310 <+163>:	push   edx
   0x56556311 <+164>:	call   0x56556120 <strncmp@plt>      <==   fonction de comparaison de chaine
   0x56556316 <+169>:	add    esp,0x10
   
   (extrait...)
End of assembler dump.

```

On place un breakpoint sur la fonction strncmp (main+164) et on continue l'exécution

```
break main+164
continue
```

```asm
 ► 0x56556311 <main+164>    call   strncmp@plt                    <strncmp@plt>
        s1: 0xffffd24f ◂— 'aaaaaaaa'                              <==   chaine saisi
        s2: 0xffffcf82 ◂— 'EleVenAttack'                          <==   chaine attendu 
        n: 0xc
 
   0x56556316 <main+169>    add    esp, 0x10
   0x56556319 <main+172>    test   eax, eax
   0x5655631b <main+174>    je     main+213                   

```

Le password attendu est donc : EleVenAttack

On exécute le programme avec le bon password, et on obtient ceci

```
./crackme2 EleVenAttack

Yaaaaaaaa, EleVenAttack a arrété l'attaque!
Voici le mdp pour extraire le contenu de l'archive
Keh]qtMkc4|o
```

Ceci nous donne le mot de passe pour extraite l'archive :  Keh]qtMkc4|o 



2 - Analyse du fichier

L’archive contient un fichier RapportEnquete2001.pdf. 
L’analyse de celui ci ne mène à rien. Il faut analyser le flux ADS (alternate data stream) qui nous mènera au flag.

Attention:
Il faut des versions récentes des logiciels d'archivage pour obtenir les ads

Exemple avec 7zip 21.0.7

On remarque que le fichier RapportEnquete2001.pdf:Zone.identifier est un ads (collonne Flux alternatif) 

![7z](images/7z.png)

Il suffit de double-cliquer sur **RapportEnquete2001.pdf:Zone.identifier**  et de saisir le mot de passe **Keh]qtMkc4|o** pour obtenir le flag.

**fl@g{StopTheAttacK2001}**

<!-- Ok F : 20220423 -->
