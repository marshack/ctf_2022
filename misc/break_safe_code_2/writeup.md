# Break safe's code 2easy (ludo_du_rzo)


## Consignes :  

```
Vous vous êtes introduit dans un datacenter et essayez de récupérer les mots de passe présents dans deux coffres.
Vous avez réussi à ouvrir le premier au chalumeau, mais le deuxième vous résiste. Impossible de percer sa porte en naquadah !!!!

Seule solution possible : retrouver le code à partir du *firmware* prélevé sur le premier coffre, en espérant que ce soit le même (firmware/code).
Pour vous aider, vous trouverez ci-joint un schéma électronique simplifié, et une page du manuel utilisateur.

Le flag est le code du coffre.

`3d6c9ba24f24697a7449a6925d60fc53580b3ffb`  [coffrefort.bin](https://download.marshack.fr/5f2559adae769ff4ad77d54ccbf03153e067bffb/misc/break_safe_code_2/coffrefort.bin)
`1faaf9a1004dc2891589126edbb71d6012c7bb51`  [coffrefort.eeprom](https://download.marshack.fr/5f2559adae769ff4ad77d54ccbf03153e067bffb/misc/break_safe_code_2/coffrefort.eeprom)
```
## Pièces jointes :  
- **Schéma électronique simplifié:**

![](assets/schema_electronique.png)

- **Diagramme de fonctionnement de la porte:**

    ![](assets/flowchart.png)

- **Mémoire flash du premier coffre fort** : coffrefort.bin et coffrefort.eeprom

    Extraction du binaire de l'ELF via:

    ````bash
  avr-objcopy -O binary coffrefort.elf coffrefort.bin
  ````

  > **IMPORTANT**: Ne pas donner le format ELF !!!

## Serveur :  

```
néant
```

## Points attribués :
```
90
```

## Flag :  

```
8*0361*1239
```

## Architecture :  

```
AVR 8bits sur ATMega328p
```

## Déroulé :  

- **Spécifications des AVRs**:
  
  - Architecture Harvard: mémoire programme séparée de la RAM
  - Processeur RISC 8bits
  - Bus d'adressage 16bits
  - RAM séparée en deux parties: de 0x0000 à 0X0FF, registres de travail, d'états ou d'entrées/sorties. Au dessus (0x0100), mémoire RAM utilisable pour les variables et la pile.
  
- **Reconnaissance des ports d'entrés/sorties utilisés**: la première étape consiste à déterminer la fonctionnalité de chaque broche du µcontrôleur utilisée par le circuit électronique à l'aide du <u>**datasheet**</u>:
  
  ![atmega328pin](assets/atmega328pin.png)
  
  On en déduit les éléments suivants:
  
  - Le port D, des PINs 0 à 6, est exclusivement utilisé pour la lecture du clavier.
  
  - Le clavier forme une matrice où chaque bouton est relié directement à deux broches du µcontrôleur: une pour la colonne et l'autre pour la ligne.
  
  - Le buzzer et la diode électroluminescente sont connectés au port B PIN 5.
  
  - Le servomoteur, patte 16, est branché sur le port B PIN 2. Cette patte est aussi partagée avec OC1B piloté par le TIMER1 servant à générer des signaux en PWM. Il semblerait donc que ce composant soit contrôlé de façon matériel et que sa rotation se fasse via une reconfiguration du TIMER1.
  
    > **Pour information:** les servomoteurs se pilotent par une impulsion dont la largeur est proportionnelle à l'angle du bras.
    >
    > ![](assets/diss_servo.png)
  
- **Contenu du l'EEPROM:**
  
  L'EEPROM du micro-contrôler est une mémoire non volatile servant à stocker des informations que l'on pourra retrouver après une réinitialisation.
  
  Sur ce coffre, le code est modifiable. On peut en déduire que celui-ci doit être inscrit dans cette mémoire.
  
  Contenu de l'EEPROM:
  
  | Offset          | Contenu |
  | --------------- | ------- |
  | 0x0000          | 0x0B    |
  | 0x0001          | 0xFE    |
  | 0x0002          | 0X12    |
  | 0x0003          | 0x1A    |
  | 0x0004          | 0X03    |
  | 0x0005          | 0X05    |
  | 0x0006          | 0X07    |
  | 0x0007          | 0x1B    |
  | 0x0008          | 0X1B    |
  | 0x0009          | 0X03    |
  | 0x000A          | 0X01    |
  | 0x000B          | 0X0A    |
  | 0x000C          | 0X74    |
  | 0x000D          | 0X75    |
  | 0x000E          | 0X74    |
  | 0x000F          | 0x75    |
  | 0x0010          | 0X74    |
  | 0x0011          | 0X75    |
  | 0x0012          | 0x00    |
  | 0x0013 - 0x03FF | 0XFF    |
  
  A première vue, pas ou peu de code ASCII, les données doivent être encodées autrement.
  
- **Désassemblage**: Ouvrir le binaire avec **"cutter"**. Le firmware étant un binaire pur, il faut indiquer au décompilateur certains paramètres:

  -  Niveau d'analyse: aaaa
  - Architecture:  AVR
  - CPU:  ATmega328p
  - Bits: 8

- **Vecteurs d'interruptions:** hormis la première ligne correspondant au reset, toutes les autres renvoient vers la ligne 0x0049 qui elle-même redirige les interruptions vers le début du programme. **On peut en conclure qu'il n'y a pas de routine d'interruption**.

- **EntryPoint/variables globales initialisées**: le point d'entrée du programme permet d'initialiser les registres et les variables globales.

  ![](assets/diss_entrypoint.png)  

  - R1 à 0x00

  - R17 à 0x01

  - Z est initialisé à 0x03FE (via R30 et R31)

  - X est initialisé à 0x0100 (via R26 et R27)

  - Via la boucle de droite, on effectue une copie d'un octet de la mémoire flash vers la mémoire RAM avec auto-incrémentation jusqu'à ce que X soit égal à 0x011A 

  - On recopie donc 25 octets (0x011A-0x0100) de la mémoire flash à partir de l'adresse de base 0x03FE vers l'adresse RAM 0x0100.

    ![](assets/diss_varglobales.png)
    
    > **Le code 2898-47*@2 est un leurre, il n'y a pas de '-' ou '@' sur le clavier**.

- **Variables globales non initialisées:**

    Une deuxième boucle au niveau de l'entry point réserve l'emplacement mémoire pour les variables non initialisées. Ici la boucle démarre à 0x011A et termine à 0x011B soir un 1 octet.

    ![](assets/diss_entrypoint_bss.png)

- **Résumer variables globales:**

    | Adresse         | Contenu                        |
    | --------------- | ------------------------------ |
    | 0x0100 - 0x0x10 | Le code est 2898-47*02\0\0\0\0 |
    | 0x011A          | 0x00                           |

 Une fois la mémoire initialisée, la fonction main() est lancée (0x03FA).

- **Fonction main():** le graphique de la fonction main ressemble beaucoup au diagramme de fonctionnement du coffre,  en comparant, il est possible de nommer les fonctions.

  - Première fonction: *bip()*;

  - Deuxième fonction: *read_key(\*)*;

  - Si la fonction read_key retourne 0x2A (R24), le coffre lance la fonction *update_code()*;

    ![](assets/main_loop.png)

  - La boucle principale commence par verrouiller la porte avec *lock_door()*;

  - La deuxième boucle vérifie que le code soit correct *read_code()* (retour != 0). Dans le cas contraire, la fonction bip est lancée;

  - En sortie de la boucle de test, la porte est ouverte, *open_door()*;

  - Enfin, nouvelle attente tant que la touche "#" n'est pas appuyée via *read_key()*" puis retour au début de la boucle principale.

- **La fonction *read_key()***: cette fonction, ne prenant pas de paramètre, est présente à deux reprises dans la fonction *main()*. Le retour de cette fonction est comparé à 0x2A au début de *main()* et à 0x23 dans la boucle principale. En s'appuyant sur le diagramme, ces valeurs correspondent respectivement à la touche '*' et '#'. **Or ces valeurs sont tout simplement les codes ASCII des touches**.

- **La fonction save_code()**: deux méthodes pour décoder l'EEPROM. La première consiste à étudier la vérification du code, la deuxième plus simple, reviens à comprendre l'enregistrement du code dans l'EEPROM.

  - La documentation confirme le lancement de la fonction bip avertissant le début de mise à jour du code;
  - Une deuxième fonction lancée pendant le changement de code est la suivante:

![](assets/write_eeprom.png)

Cette fonction effectue des actions d'entrées/sorties sur les ports 0x1f, 0x20, 0x21 et 0x22. La documentation Atmel indique que ces ports correspondent aux ports d'accès à l'EEPROM (EECR, EEDR, EEARL et EEARH).

Si on regarde encore la documentation de l'ATmega, le code assembleur pour écrire un octet dans l'EEPROM, donné en exemple, ressemble beaucoup à cette fonction. On peut donc en conclure que cette fonction est *eeprom_write_byte(offset, value)*.

![](assets/eeprom_write_bit.png)

  - *save_code()* commence donc par biper puis initialise ses variables locales (R16, R17, R29, R28)
  - La boucle principale lit en boucle le clavier jusqu'à ce que la touche "#" (0x23) soit pressée:

  ![](assets/save_code_main_loop.png)


Le code clavier R22 (R24->R15->R22) est ensuite enregistré dans l'EEPROM à l'adresse R16-R17 (movw r24, r16) après un XOR avec une clé enregistré dans R29 (initialement égale à 0xC6).

Un fois R28 et R16-R17 incrémentés,  le code clavier est sauvegardé dans R29.

Une dernière condition de sortie de boucle est présent à la fin dans le cas où R28 est égal à 0x19.

En peut en déduire de cela:

    - R16-R17: offset EEPROM initialisé à 0x0001;
    - R29: clé XOR initialisée à 0xC6;
    - R28: compteur de boucle ou du nombre de caractères;
    - 0x19 (25) est la taille maximale d'un code.

**On en conclue que le code clavier est inscrit à partir du deuxième octet (offet 0x0001). Chaque touche du clavier est codée avec un XOR de valeur initiale 0XC6 puis avec le code ASCII du caractère précédent.**

  - Fin de la fonction *save_code()*:

  ![](assets/end_save_code.png)

​	En fin de fonction, R28, le compteur de boucle, c'est à dire le nombre de touches du code, est écrit à l'offset 0x0000 de l'EEPROM.



- **Décodage du code:**

  | Offset          | Contenu | Valeur               |
  | --------------- | ------- | -------------------- |
  | 0x0000          | 0x0B    | 11 touches           |
  | 0x0001          | 0xFE    | 0xFE^0xC6=0x38  "8" |
  | 0x0002          | 0X12    | 0x12^0x38=0x2A  "*" |
  | 0x0003          | 0x1A    | 0x1A^0x2A=0x30  "0"  |
  | 0x0004          | 0X03    | 0x03^0x30=0x33  "3"  |
  | 0x0005          | 0X05    | 0x05^0x33=0x36  "6"  |
  | 0x0006          | 0X07    | 0x07^0x36=0x31  "1"  |
  | 0x0007          | 0x1B    | 0x1B^0x31=0x2A  "*"  |
  | 0x0008          | 0X1B    | 0x1B^0x2A=0x31  "1"  |
  | 0x0009          | 0X03    | 0x03^0x31=0x32  "2"  |
  | 0x000A          | 0X01    | 0x01^0x32=0x33  "3"  |
  | 0x000B          | 0X0A    | 0x0A^0x33=0x39  "9"  |
  | 0x000C          | 0X74    | *INUTILE*            |
  | 0x000D          | 0X75    | *INUTILE*            |
  | 0x000E          | 0X74    | *INUTILE*            |
  | 0x000F          | 0x75    | *INUTILE*            |
  | 0x0010          | 0X74    | *INUTILE*            |
  | 0x0011          | 0X75    | *INUTILE*            |
  | 0x0012          | 0x00    | *INUTILE*            |
  | 0x0013 - 0x03FF | 0XFF    | *INUTILE*            |

Le code du coffre est donc **8\*0361\*1239**.

<!-- Ok F : 20220423 -->