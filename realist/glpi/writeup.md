# Write up GLPI

## Consigne :

```
Vous êtes simple utilisateur d'une application de gestion de parc et de tickets (GLPI). Trouvez un moyen d’accéder au compte super admin.

Démarrer un environnement virtuel. Attention, la durée de vie de l'environnement est limitée.
<div>
<button class="btn btn-info" onclick="window.open('/portailvms/10','_blank')" >Accéder au portail</button>
</div>
<p></p>

Vos identifiants de connexion :
 - login : tech
 - password : tech

Une fois que vous aurez élevé vos privilèges, le paramétrage des signatures emails est votre ami
```

## Pièce jointe :

*Néant*

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{~update_GLPI_Please~otherwise...}
```

## Auteur :

```
ESD
```

## Solution :


L’outil de gestion de parc et de ticket n’est pas mis à jour par l'administrateur et une vulnérabilité a été trouvée sur la version installée. (Glpi 9.4.3).

Un POC est disponible pour exploiter la CVE-2019-14666 : https://www.tarlogic.com/advisories/Tarlogic-2019-GPLI-Account-Takeover.txt ou https://github.com/SamSepiolProxy/GLPI-9.4.3-Account-Takeover/blob/main/reset.py

Ce exploit est écrit pour python2, mais est facilement portable pour python3 (remplacer `print ...` par `print(...)`).

Nous avons besoin pour réussir cet exploit, d'un compte à faible privilège sur l'interface de gestion (ce qui est le cas), et de l'adresse email du compte super utilisateur, visible sur la page /front/user.php :  glpi@marshack.fr

On l'exécute (il faut fournir l'url, nos identifiants de connexion de "tech", l'adresse email du super utilisateur glpi, et enfin le nouveau mot de passe) :
```bash
python3 CVE-2019-14666.py --url http://172.102.100.108 --user tech --password tech --email glpi@marshack.fr --newpass mypassword
[+] Password changed! ;)
```

On se connecte en tant que glpi, avec notre nouveau mot de passe (modifié par l'exploit) :
 - login : glpi
 - password : mypassword

Un indice nous est donné dans la consigne : "Une fois que vous aurez élevé vos privilèges, le paramétrage des signatures emails est votre ami ..."

Ce paramètre est accessible dans "Configuration/Notifications/Configuration des notifications par courriels" (`front/notificationmailingsetting.form.php`)

Le flag est visible dans le champ "Signature des courriels" : `fl@g{~update_GLPI_Please~otherwise...}`

<!-- Ok F : 20220423 -->
